# SrSolve

Provides implementations of numerical methods to compute solutions to the
time-independent radial Schrödinger equation, as well as simple gradient
descent. Originally formulated to deal with the strontium molecular potential,
but is easily generalized to other 1D potentials.

## Package Contents

The elements of the `libSr` Python package are divided among three main
sub-modules:

* `solve`: This module implements the naive Numerov method [[1](#references)],
  matrix Numerov method [[2](#references)], Johnson renormalized Numerov method
  [[3](#references)], and energy-wavefunction coupling method
  [[4](#references)], as well as a simpler, brute-force shooting method based on
  the Johnson renormalized Numerov method. The `Solver` and `MPSolver` classes
  are intended to be the main interfaces to these methods (see
  [Examples](#examples)).
* `optim`: This module implements simple gradient descent for user-defined cost
  functions with optionally variable step sizes using the formula from
  [[5](#references)]. The main interface is the `optim` function.
* `potentials`: This module implements different (spherically symmetric)
  molecular potentials and provides the `MolecularPotential` class for users to
  define their own. Pre-defined potentials are taken from [[6](#references)] and
  [[7](#references)].

## Package Behavior

The main interfaces listed above, as well as physical constants and functions in
`phys` and a couple other miscellaneous classes are imported at the top level
for convenience.

Also included are various definitions (in `misc` and `misc.plotdefs`) to
assist with type-checking, table formatting, and plotting via `matplotlib`. By
default, functions implemented here in Python are strongly typed. This can be
deactivated by setting `libSr.misc.misc.do_typecheck = False`.


## Installation

To use this module, the best-practice recommendation is to first create a Python
virtual environment before cloning the repository:
```BASH
python -m venv srsolve_venv
cd srsolve_venv
source ./bin/activate
git clone https://gitlab.com/whooie/srsolve.git
```
The only true dependencies needed for the package's operation are `numpy` and
`periodictable`, but users may also wish to install `matplotlib` for plotting or
`cython` to compile the C extensions. These are available from PyPI, and can be
installed with `pip`:
```BASH
python -m pip install numpy periodictable matplotlib cython
```

### C Extensions

If `cython` is installed, the C extensions can be compiled by running
`setup.py`,
```BASH
python ./srsolve/setup.py build_ext --inplace
```
Using compiled C extensions greatly improves the runtime efficiency of the
solving and optimization methods.

If (for whatever reason) users prefer not to use Cython, the included C files
generated from the source `*.pyx` files can be compiled with `make`
```BASH
cd srsolve
make
```
with the caveats that `gcc` must be installed (it must be sourced from outside
PyPI) and that changes made to the source `*.pyx` files will have no effect upon
recompilation. Once this is completed, the sub-modules of `libSr` can be
imported from Python programs as normal.


## Examples

A few simple use cases are provided for each of the main modules listed above in
[Package Contents](#package-contents):

### `solve` and `potentials`

The main interfaces to the `libSr.solve` module are through the `Solver` and
`MPSolver` classes. While `Solver` acts simply as a container to a spatial grid
`x` and potential function `V` sampled over the grid, `MPSolver` contains
additional methods to work with defined `MolecularPotential` instances for
things such as generating optimal spatial grids and automatic re-scaling between
naturalized units. The following listing uses both classes to solve for levels
in a simple Lennard-Jones potential.
```python
import numpy as np
import libSr

# define the potential function, plus first and second derivatives
# should contain both attractive and repulsive terms
PARAMS = {
    "sigma": 6.133e9,
    "C6": 3164
}
def V(r, sigma=None, C6=None) -> np.ndarray:
    _sigma = PARAMS["sigma"] if sigma is None else sigma
    _C6 = PARAMS["C6"] if C6 is None else C6
    return _sigma/r**12 - _C6/r**6

def dV(r, sigma=None, C6=None) -> np.ndarray:
    _sigma = PARAMS["sigma"] if sigma is None else sigma
    _C6 = PARAMS["C6"] if C6 is None else C6
    return -12*_sigma/r**13 + 6*_C6/r**7

def d2V(r, sigma=None, C6=None) -> np.ndarray:
    _sigma = PARAMS["sigma"] if sigma is None else sigma
    _C6 = PARAMS["C6"] if C6 is None else C6
    return 156*_sigma/r**14 - 42*_C6/r**8

# assume r, sigma, and C6 are given in atomic units
LJPot = libSr.MolecularPotential(V, dV, d2V, # functions
    a0=libSr.phys.a0, # length unit
    E0=libSr.phys.Eh, # energy unit
    units_tag="Bohr -> Hartree",
    **PARAMS
)

# using Solver first - get all states in the well for two strontium atoms
mu = libSr.reduced_mass("Sr", "Sr") # reduced mass of the dimer
Enat = libSr.phys.hbar**2/(2*mu*LJPot.a0**2) # natural energy unit
r = np.linspace(2, 50, 8000) # spatial grid - may need to adjust size to
                             # accurately calculate the weakly bound states
v = LJPot(r) * LJPot.E0 / Enat # rescale to natural units
X = libSr.Solver(r, v)
sols = X.solve(method="matrix-numerov",
    E_filter=lambda e: e > v.min() and e < 0) # filter out non-physical solutions

# using MPSolver - solve specifically for the state bound by the least energy
# from the list above
n = len(sols) - 1 # vibrational level of the state - assume node counts in the
                  # computed wavefunctions increase from zero
Emax = sols[-1].E * Enat # used to generate the spatial grid; give it in Joules
Y = libSr.MPSolver(mu, LJPot, Emax)
sol = Y.solve(method="johnson", nu=n, bounds=(X.Vmin, 0))[0]
```

### `optim`

The `optim` module can be used for any user-defined cost function following the
signature `costf(params: optim.HashVector, **costf_args) -> float` where
`params` holds any variables with respect to which gradients will be calculated.

```python
import libSr.optim as optim

# cost function
def costf(params, **kwargs) -> float:
    _x = params["x"]
    _y = params["y"]
    _z = params["z"]
    return 5000*(_x - 5.249)**2 + 2500*(_y + 8.585)**2 + 0.5*(_z + 100)**2

params = optim.HashVector({"x": -100, "y": 200, "z": 40}) # initial guess
vlist = [("x", None), ("y", None), ("z", None)] # optimize wrt these variables;
                                                # replace None with any existing
                                                # correlations
costf_args = dict()
stepsize = 1e-6 # for gradient calculations
L0 = 1e-6 # initial descent stepsize
L1 = 1e-6 # fallback stepsize in case dynamic stepsize formula fails
dynamic_until = 0.5 # use dynamic descent stepsizes for only half the descent
EPSILON = 1e-6 # detect convergence when |gradient| < EPSILON
maxiters = 1000
filename = "optim_test.txt"
optim.optim(costf, params, vlist, costf_args, stepsize, L0, L1, dynamic_until,
    EPSILON, maxiters, filename)
```

## References

[1] B. Numerov, "Note on the numerical integration of d2x/dt2 = f(x,t)."
Astronomische Nachrichten 230 (19) (1927).

[2] M. Pillai, J. Goglio, and T. Walker, "Matrix Numerov method for solving
Schrödinger's equation." American Journal of Physics 80 (11):1017-1019 (2012).

[3] B. R. Johnson, "New numerical methods applied to solving the one-dimensional
eigenvalue problem." J. Chem. Phys. 67:4086 (1977).

[4] A. Udal, R. Reeder, E. Velmre, and P.Harrison, "Comparison of methods for
solving the Schrödinger equation for multiquantum well heterostructure
applications." Proc. Estonian Acad. Sci. Eng. 12 (2006).

[5] J. Barzilai and J. M. Borwein, “Two-Point Step Size Gradient Methods.” IMA
Journal of Numerical Analysis 8:141-148 (1988).

[6] A. Stein, H. Knöckel, and E. Tiemann, "The 1S+1S asymptote of Sr2 studied by
Fourier-transform spectroscopy." Eur. Phys. J. D 57 (171) (2010).

[7] M. Kitagawa, K. Enomoto, K. Kasa, Y. Takahashi, R. Ciurylo, P. Naidon, and
P. Julienne, "Two-color photoassociation spectroscopy of ytterbium atoms and the
precise determinations of s-wave scattering length." Phys. Rev. A 77:012719
(2008).

