"""
Provides methods to perform gradient descent for a general cost function.
"""

import copy

import numpy as np

from libSr.solve import SrSolver
import libSr.phys as phys
import libSr.misc as misc
from libSr.misc import typecheck, Real, Int, Function

DTYPE = np.float64

class HashVector:
    """
    Class to implement simple arithmetic operations between vectors represented
    as hash maps (dicts). Binary operations (excluding `==` and `!=`) are
    computed over only the intersection of the keys of the two operants, meaning
    resultant vectors may have fewer keys than one or both operants. The `*`
    operator implements element-wise multiplication, while the `@` operator
    implements the vector inner product.

    Attributes
    ----------
    data : dict[str : float]
        Elements of the vector.
    """

    @typecheck({
        "data": (dict, type(None))
    })
    def __init__(self, data=None):
        """
        Constructor.

        Parameters
        ----------
        data : dict[str : float]
            Elements of the vector.
        """
        self.data = copy.deepcopy(data) if data is not None else dict()

    def _get_shared_keys(self, other) -> set:
        assert isinstance(other, HashVector)
        return set(self.data.keys()).intersection(set(other.data.keys()))

    def _get_unshared_keys(self, other) -> set:
        assert isinstance(other, HashVector)
        return set(self.data.keys()).symmetric_difference(set(other.data.keys()))

    def __add__(self, other):
        assert isinstance(other, HashVector)
        new = copy.deepcopy(self)
        new.data.update({k: self.data[k] + other.data[k]
            for k in self.data.keys() if k in other.data.keys()})
        return new

    def __sub__(self, other):
        assert isinstance(other, HashVector)
        new = copy.deepcopy(self)
        new.data.update(-other)
        new.data.update({k: self.data[k] - other.data[k]
            for k in self.data.keys() if k in other.data.keys()})
        return new

    def __mul__(self, other):
        assert isinstance(other, (HashVector, *Real))
        if isinstance(other, HashVector):
            shared_keys = self._get_shared_keys(other)
            unshared_keys = self._get_unshared_keys(other)
            new = HashVector({k: self.data[k] * other.data[k] for k in shared_keys})
            new.update({k: 0 for k in unshared_keys})
            return new
        else:
            return HashVector({k: self.data[k] * other for k in self.data.keys()})

    def __rmul__(self, other):
        return self.__mul__(other)

    def __matmul__(self, other) -> float:
        assert isinstance(other, (HashVector, *Real))
        keys = self._get_shared_keys(other)
        res = 0
        for k in keys:
            res += self.data[k] * other.data[k]
        return res

    def __neg__(self):
        return HashVector({k: -self.data[k] for k in self.data.keys()})

    def __neq__(self, other) -> bool:
        assert isinstance(other, HashVector)
        return not self.__eq__(other)

    def __eq__(self, other) -> bool:
        assert isinstance(other, HashVector)
        keys = self._get_shared_keys(other)
        return not bool(sum([self.data[k] != other.data[k] for k in keys]) \
                + (self.data.keys() != other.data.keys()))

    def __getitem__(self, key) -> float:
        assert isinstance(key, str)
        return self.data[key]

    def __setitem__(self, key, val) -> None:
        assert isinstance(key, str)
        assert isinstance(val, Real)
        self.data[key] = val

    def __delitem__(self, key) -> None:
        assert isinstance(key, str)
        del self.data[key]

    #def __iter__(self):
    #    return iter(self.data)

    #def __reversed__(self):
    #    return reversed(self.data)

    def __contains__(self, key) -> bool:
        assert isinstance(key, str)
        return key in self.data

    def __repr__(self) -> str:
        return "HashVector("+repr(self.data)+")"

    def __str__(self) -> str:
        return "HashVector("+str(self.data)+")"

    def __len__(self) -> int:
        return len(self.data)

    def get(self, key, default=None) -> float:
        assert isinstance(key, str)
        return self.data.get(key, default)

    def insert(self, key, val) -> None:
        assert isinstance(key, str)
        assert isinstance(val, float)
        self.data[key] = val

    def shape(self) -> tuple:
        return (len(self.data),)

    def norm(self) -> float:
        return np.sqrt(sum([self.data[k]**2 for k in self.data.keys()]))

    def keys(self) -> list:
        return list(self.data.keys())

    def values(self) -> list:
        return list(self.data.values())

    def items(self) -> list:
        return list(self.data.items())

    def update(self, data) -> None:
        assert isinstance(data, dict)
        self.data.update(data)

    def update_hv(self, other) -> None:
        assert isinstance(other, HashVector)
        self.data.update(other.data)

@typecheck({
    "costf": Function,
    "params": HashVector,
    "v": tuple,
    "costf_args": dict,
    "stepsize": Real
})
def partial(costf, params, v, costf_args=dict(), stepsize=0.001) -> float:
    """
    Compute the partial derivative of the cost function about the point in
    parameter space defined by `params` with respect to the parameter `v` using
    the central finite difference:

        d(costf)   costf(v + |stepsize*v|) - costf(v - |stepsize*v|)
        -------- = -------------------------------------------------
           dx                        2*|stepsize*v|

    Parameters
    ----------
    costf : python function
        Cost function. Expected to follow the signature
            costf(params, **costf_args) -> float
        In other words, `costf` will be passed `params` as the first positional
        argument, followed by the unpacked kwargs in `costf_args`.
    params : HashVector
        HashVector of parameter values to pass to `costf`.
    v : (str, callable or None)
        Tuple containing the name of the parameter with respect to which the
        partial derivative is calculated, as well as a callable defining any
        relationships between parameters. The callable is expected to take as
        input the unpacked `params` dict and return another dict with updated
        values of the relevant parameters. If no callable should be called, then
        `callable` should be `None`.
    costf_args : dict (optional)
        Additional keyword arguments to provide `costf` after `params`.
    stepsize : float > 0 (optional)
        Fractional stepsize taken (forward and backward) to compute the partial
        derivative.

    Returns
    -------
    pd : float
        Partial derivative of the cost function with respect to `v`.
    """
    assert stepsize > 0

    val0 = params[v[0]]
    val_p = val0 + abs(stepsize*val0)
    val_m = val0 - abs(stepsize*val0)
    params_p = copy.deepcopy(params)
    params_m = copy.deepcopy(params)
    params_p[v[0]] = val_p
    params_m[v[0]] = val_m
    if v[1] is not None:
        params_p.update(v[1](**params_p))
        params_m.update(v[1](**params_m))

    cost_p = costf(params_p, **costf_args)
    cost_m = costf(params_m, **costf_args)
    return (cost_p - cost_m)/abs(2*stepsize*val0)

@typecheck({
    "costf": Function,
    "params": HashVector,
    "vlist": list,
    "costf_args": dict,
    "stepsize": Real
})
def gradient(costf, params, vlist, costf_args=dict(), stepsize=0.001) \
        -> HashVector:
    """
    Compute the gradient of the cost function about the point in parameter space
    defined by `params` with respect to the variables named in `vlist`.

    Parameters
    ----------
    costf : python function
        Cost function. Expected to follow the signature
            costf(params, **costf_args) -> float
        In other words, `costf` will be passed `params` as the first positional
        argument, followed by the unpacked kwargs in `costf_args`.
    params : HashVector
        HashVector of parameter values to pass to `costf`.
    vlist : list[(str, callable or None)]
        List of parameters with respect to which to calculate partial
        derivatives. See `help(libSr.optim.partial)` for more information on the
        format of each element in `vlist`.
    costf_args : dict (optional)
        Additional keyword arguments to provide `costf` after `params`.
    stepsize : float > 0 (optional)
        Fractional stepsize taken (forward and backward) to compute the partial
        derivative.

    Returns
    -------
    g : HashVector
        Gradient of the cost function.
    """
    g = HashVector()
    for v in vlist:
        g[v[0]] = partial(costf, params, v, costf_args, stepsize)
    return g

@typecheck({
    "val1": Real,
    "val2": Real
})
def absmin(val1, val2) -> float:
    """
    Return `val1` if |val1| < |val2| else `val2`.
    """
    return val1 if (abs(val1) < abs(val2)) else val2

@typecheck({
    "p": HashVector,
    "plast": HashVector,
    "g": HashVector,
    "glast": HashVector,
    "L0": Real
})
def learning_stepsize(p, plast, g, glast, L0=0.001) -> float:
    """
    Determine the appropriate stepsize from a given last step.

    Parameters
    ----------
    p, plast : dict
        Current and last point in parameter space.
    g, glast : dict
        Gradient at the current and last point.
    L0 : float (optional)
        Default stepsize if an invalid value is encountered.

    Returns
    -------
    Ln : float
        Gradient descent stepsize.
    """
    pdiff = p - plast
    gdiff = g - glast
    pgdiffdot = pdiff@gdiff
    pdiffsize = pdiff.norm()**2
    gdiffsize = gdiff.norm()**2

    if pgdiffdot == 0 or gdiffsize == 0:
        print("optim.learning_stepsize: WARNING: fallback stepsize used")
        return L0
    else:
        return pgdiffdot/gdiffsize

    #if pdiffsize == 0 or pgdiffdot == 0:
    #    print("optim.learning_stepsize: WARNING: fallback stepsize used")
    #    return L0
    #else:
    #    return pdiffsize/pgdiffdot

    #if pgdiffdot == 0 or (pdiffsize == 0 and gdiffsize == 0):
    #    return L0
    #elif pdiffsize == 0:
    #    return pgdiffdot/gdiffsize
    #elif gdiffsize == 0:
    #    return pdiffsize/pgdiffdot
    #else:
    #    return absmin(pgdiffdot/gdiffsize, pdiffsize/pgdiffdot)

@typecheck({
    "costf": Function,
    "params": HashVector,
    "vlist": list,
    "costf_args": dict,
    "stepsize": Real,
    "L0": Real,
    "L1": Real,
    "dynamic_until": Real,
    "EPSILON": Real,
    "maxiters": Int,
    "filename": str
})
def optim(costf, params, vlist, costf_args=dict(), stepsize=0.001, L0=0.001,
        L1=0.001, dynamic_until=1, EPSILON=1e-6, maxiters=1000,
        filename="libSr_optim.txt") -> None:
    """
    Optimize parameters in the strontium potential using data from `libSr.data`
    in a reduced-chi-squared analysis as the cost function.

    Parameters
    ----------
    costf : python function
        Cost function. Expected to follow the signature
            costf(params, **costf_args) -> float
        In other words, `costf` will be passed `params` as the first positional
        argument, followed by the unpacked kwargs in `costf_args`.
    params : HashVector
        HashVector of parameter values to pass to `costf`.
    vlist : list[(str, callable or None)]
        List of parameters with respect to which to calculate partial
        derivatives. See `help(libSr.optim.partial)` for more information on the
        format of each element in `vlist`.
    costf_args : dict (optional)
        Additional keyword arguments to provide `costf` after `params`.
    stepsize : float > 0 (optional)
        Fractional stepsize taken (forward and backward) to compute the partial
        derivative.
    L0 : float (optional)
        Initial learning rate for the first step. All steps after the first one
        will use an adjustable learning rate.
    L1 : float (optional)
        Fallback learning rate in the event that invalid values are encountered
        or the dynamic_until condition is passed.
    dynamic_until : float >= 0 (optional)
        Use a dynamically adjusted learning rate until the step number given by
            floor(`maxiters` * `dynamic_until`)
        for values between 0 and 1 (inclusive) and
            floot(`dynamic_until`)
        for values greater than 1, at which point switch to constant learning
        rate `L1`. Setting equal to zero disables the dynamic learning rate.
    EPSILON : float > 0 (optional)
        Detect convergence when the magnitude of the gradient is less than
        `EPSILON`.
    maxiters : int > 0 (optional)
        Upper limit on the number of steps taken.
    filename : str (optional)
        Write information to this file to track progress.

    Returns
    -------
    None
    """
    assert stepsize > 0
    assert EPSILON > 0
    assert maxiters > 0
    assert dynamic_until >= 0

    D = int(dynamic_until*maxiters) if dynamic_until <= 1 else dynamic_until
    Nvars = len(vlist)
    vnames = [vlist[j][0] for j in range(Nvars)]
    head, table_fmt = misc.gen_table_fmt(
        *[
            ("STEP", "i", dict(l=max(4, len(str(maxiters))-1))),
            ("COSTF", "e"),
            ("GRAD. MAG.", "e"),
            *[(vname, "e") for vname in vnames]
        ], L=24, P=17
    )

    outfile = open(filename, 'w')
    misc.print_write(outfile, "Using cost function `{}`".format(costf.__name__))
    misc.print_write(outfile, "With initial parameters: {")
    for v in params:
        misc.print_write(outfile, "    {} = {}".format(v, params[v]))
    misc.print_write(outfile, "}")
    misc.print_write(outfile, "With optimization parameters:")
    misc.print_write(outfile, "    stepsize = {}".format(stepsize))
    misc.print_write(outfile, "    L0 = {}".format(L0))
    misc.print_write(outfile, "    L1 = {}".format(L1))
    misc.print_write(outfile, "    dynamic_until = {}".format(dynamic_until))
    misc.print_write(outfile, "    EPSILON = {}".format(EPSILON))
    misc.print_write(outfile, "    maxiters = {}".format(maxiters))
    misc.print_write(outfile, "")
    misc.print_write(outfile, head)

    # do one initial step to set glast, paramslast so that the proper step size
    # can be calculated
    p = copy.deepcopy(params)
    for v,f in vlist:
        if f is not None:
            p.update(f(**p))
    plast = copy.deepcopy(p)
    g = gradient(costf, p, vlist, costf_args, stepsize)
    glast = copy.deepcopy(g)
    gsize = g.norm()
    cost = costf(p, **costf_args)
    misc.print_write(outfile, table_fmt.format(0, cost, gsize, *[p[v] for v in vnames]))

    p = p - L0*g
    for v,f in vlist:
        if f is not None:
            p.update(f(**p))
    g = gradient(costf, p, vlist, costf_args, stepsize)
    gsize = g.norm()
    l = learning_stepsize(p, plast, g, glast, L1) if 1 < D else L1
    cost = costf(p, **costf_args)
    misc.print_write(outfile, table_fmt.format(1, cost, gsize, *[p[v] for v in vnames]))
    
    for j in range(2, maxiters+1):
        plast = copy.deepcopy(p)
        glast = copy.deepcopy(g)
        p = p - l*g
        for v,f in vlist:
            if f is not None:
                p.update(f(**p))
        g = gradient(costf, p, vlist, costf_args, stepsize)
        gsize = g.norm()
        l = learning_stepsize(p, plast, g, glast, L1) if j < D else L1
        cost = costf(p, **costf_args)
        misc.print_write(outfile, table_fmt.format(j, cost, gsize, *[p[v] for v in vnames]))

        if gsize < EPSILON:
            break
    outfile.close()
    return None

