# cython: wraparound=False
# cython: boundscheck=False
# cython: embedsignature=True

import numpy as np
import cython

cpdef double lagrange(double[:] data_x, double[:] data_y, double x):
    """
    Interpolate a function of one variable using a Lagrange polynomial.

    Parameters
    ----------
    data_x, data_y : numpy 1D array
        Arrays of x- and y-values of the function.
    x : float
        x-value at which to evaluate the Lagrange polynomial.

    Returns
    -------
    y : float
        y-value of the Lagrange polynomial at the given x-value.
    """
    assert data_x.shape[0] == data_y.shape[0]

    cdef Py_ssize_t k = data_x.shape[0]
    cdef unsigned int m, j

    cdef double y 
    y = np.sum([
        data_y[j] * np.prod([
            (x - data_x[m])/(data_x[j] - data_x[m]) for m in range(k) if m != j
        ], axis=0) for j in range(k)
    ], axis=0)

    return y

cpdef double dlagrange(double[:] data_x, double[:] data_y, double x):
    """
    Interpolate the first derivative of a function of one variable using the
    first derivative of a Lagrange polynomial.

    Parameters
    ----------
    data_x, data_y : numpy 1D array
        Arrays of x- and y-values of the function.
    x : float
        x-value at which to evaluate the Lagrange polynomial first derivative.

    Returns
    -------
    y : numpy ndarray or float or int
        y-value of the Lagrange polynomial's first derivative at the given
        x-value.
    """
    assert data_x.shape[0] == data_y.shape[0]

    cdef Py_ssize_t k = data_x.shape[0]
    cdef unsigned int m, j, i

    cdef double y
    y = np.sum([
        data_y[j] * np.sum([
            1/(data_x[j] - data_x[i]) * np.prod([
                (x - data_x[m])/(data_x[j] - data_x[m]) for m in range(k) if m != j and m != i
            ], axis=0) for i in range(k) if i != j
        ], axis=0) for j in range(k)
    ], axis=0)

    return float(y)

@cython.boundscheck(True)
cpdef list find_zeros(double[:] x, double[:] y, str kind="both", bint as_ndarray=False):
    """
    Find the positions `x0` where the function `y` cross zero and interpolate to
    find true positions.

    Parameters
    ----------
    x, y : numpy 1D array
        Sampled points `y` as a function of `x`.
    kind : str in {'rising', 'falling', 'both'}
        Look for y=0 crossings from below, from above, or both. Default to both.
    as_ndarray : bool
        Controls whether the zeros are returned as a list or numpy ndarray.

    Returns
    -------
    x0 : numpy 1D array or list
        Zeros of `y`. Type of `x0` is determined by `as_ndarray`.
    """
    assert x.shape[0] == y.shape[0]
    assert x.shape[0] > 2
    assert kind in {"rising", "falling", "both"}

    cdef bint rising, falling
    if kind == "rising":
        rising, falling = True, False
    elif kind == "falling":
        rising, falling = False, True
    elif kind == "both":
        rising, falling = True, True

    cdef unsigned int i
    x0 = list()

    for i in range(2, len(y)-2):
        if y[i] == 0:
            x0.append(x[i])
        elif y[i]*y[i-1] <= 0 \
                and ((rising and y[i]-y[i-1]>0) or (falling and y[i]-y[i-1]<0)):
            x_interp = x[i-2:i+2]
            y_interp = y[i-2:i+2]
            assert len(x_interp) == len(y_interp)
            if len(x_interp) < 4:
                print(f"{__name__}.find_zeros: WARNING: attempting to interpolate near an edge of the given data; some accuracy may be lost")
            zero = lagrange(y_interp, x_interp, 0)
            x0.append(zero)

    return np.array(x0) if as_ndarray else x0

