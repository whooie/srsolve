"""
Physical constants. All quantities taken from NIST:
https://physics.nist.gov/cuu/Constants/index.html
"""

from math import pi, prod
import re
from periodictable import elements

# planck constant [kg m^2 s^-1]
h = 6.62607015e-34         # +/- 0 (exact)

# reduced planck constant [kg m^2 s^-1]
hbar = h/2/pi               # +/- 0 (exact)

# speed of light in vacuum [m s^-1]
c = 2.99792458e8            # +/- 0 (exact)

# Avogadro's number
NA = 6.02214076e23         # +/- 0 (exact)

# Boltzmann's constant [J K^-1]
kB = 1.380649e-23         # +/- 0 (exact)

# electric permittivity in vacuum [F m^-1]
e0 = 8.8541878128e-12       # +/- 0.0000000013e-12

# magnetic permeability in vacuum [N A^-2]
u0 = 1.25663706212e-6       # +/- 0.00000000019e-6

# Newtonian gravitational constant [m^3 kg^-1 s^-2]
G = 6.67430e-11             # +/- 0.00015e-11

# gravitational acceleration near Earth's surface [m s^-2]
g = 9.80665                 # +/- 0 (exact)

# elementary charge [C]
e = 1.602176634e-19         # +/- 0 (exact)

# electron mass [kg]
me = 9.1093837015e-31       # +/- 0.0000000028e-31

# proton mass [kg]
mp = 1.67262192369e-27      # +/- 0.00000000051e-27

# unified atomic mass unit [kg]
mu = 1.66053906660e-27      # +/- 0.0000000005e-27

# Rydberg constant [m^-1]
Rinf = 10973731.568160   # +/- 0.000021

# fine structure constant
alpha = 7.2973525693e-3     # +/- 0.0000000011e-3

# molar gas constant
R = 8.314462618             # +/- 0 (exact)

# Stefan-Boltzmann constant
SB = (pi**2*kB**4)/(60*hbar**3*c**2) # +/- 0 (exact)

# Bohr radius [m]
a0 = 5.29177210903e-11      # +/- 0.00000000080e-11

# Bohr magneton [J T^-1]
uB = 9.2740100783e-24       # +/- 0.0000000028e-24

# Hartree energy [J] = 2*Rinf*h*c
Eh = 4.3597447222071e-18    # +/- 0.0000000000085e-18

# unified atomic mass unit [kg] = 1/NA/1000
amu = 1.66053906660e-27     # +/- 0.00000000050e-27

amu2kg = amu
#amu2kg = 1/NA/1000
kg2amu = 1/amu2kg

a02m = a0
m2a0 = 1/a02m

cm2J = 100*h*c
J2cm = 1/cm2J

Hz2J = h
J2Hz = 1/Hz2J

K2J = kB
J2K = 1/K2J

Eh2J = Eh
J2Eh = 1/Eh2J

eV2J = e
J2eV = 1/eV2J

def reduced_mass(O1, O2=None) -> float:
    """
    Calculate the reduced mass (in kilograms) of a system of objects `O1` and
    `O2` where each is given as a float, a string, or a tuple following

        Type    Format                          Examples
        ----    ------                          -------
        float   > 0                             1.427e-25, 5.644e-27
        str     '[mass]<symbol>[quantity]'      '86Sr', '170Yb2'
        tuple   (<symbol>, [mass], [quantity])  ('Sr', 86), ('Yb', 170, 2)

    with `<..>` and `[..]` denoting required and optional items, respectively.
    Note that each object is considered as its own indivisible element in the
    system, so for example

        reduced_mass('86Sr', '86Sr') != reduced_mass('86Sr2')

    If mass numbers are not given, the most abundant isotope is used.
    """
    assert O1 is not None
    masses = list()
    for Oi in [O1, O2]:
        if Oi is None:
            masses *= 2
            continue
        elif isinstance(Oi, (float, int)):
            if Oi < 1:
                masses.append(Oi)
            else:
                masses.append(Oi * amu2kg)
            continue
        elif isinstance(Oi, str):
            oi = Oi
        elif isinstance(Oi, tuple):
            if len(Oi) == 1:
                oi = Oi[0]
            elif len(Oi) == 2:
                oi = str(Oi[1])+Oi[0]
            elif len(Oi) >= 3:
                oi = str(Oi[1])+Oi[0]+str(Oi[2])
        else:
            raise Exception("phys.reduced_mass: FATAL: invalid identifier provided")

        match = re.match(r'([0-9]*)([A-Z][a-z]?)([0-9]*)', oi)
        try:
            element = elements.isotope(match.group(2))
        except:
            raise Exception("phys.reduced_mass: FATAL: invalid identifier provided")
        if match.group(1) == "":
            abund = np.array(map(lambda i: element[i].abundance, element.isotopes))
            m = element[element.isotopes[abund.argmax()]].mass * amu2kg
        else:
            m = element[int(match.group(1))].mass * amu2kg
        masses.append(m*int(match.group(3)) if match.group(3) != "" else m)
    return prod(masses)/sum(masses)

def molecule_str(O1, O2=None):
    """
    Generate a string representing a molecule of two objects `O1` and `O2`. See
    `help(libSr.phys.reduced_mass)` for formatting.
    """
    assert O1 is not None
    molecule = str()
    for Oi in [O1, O2]:
        if Oi is None:
            molecule *= 2
            continue
        elif isinstance(Oi, (float, int)):
            molecule += "+UNKN" if molecule != "" else "UNKN"
            continue
        elif isinstance(Oi, str):
            oi = Oi
        elif isinstance(Oi, tuple):
            if len(Oi) == 1:
                oi = Oi[0]
            elif len(Oi) == 2:
                oi = str(Oi[1])+Oi[0]
            elif len(Oi) >= 3:
                oi = str(Oi[1])+Oi[0]+str(Oi[2])
        else:
            raise Exception("phys.molecule_str: FATAL: invalid identifier provided")

        match = re.match(r'([0-9]*)([A-Z][a-z]?)([0-9]*)', oi)
        try:
            I = elements.isotope(match.group(1)+(len(match.group(1))>0)*"-"+match.group(2))
        except:
            raise Exception("phys.molecule_str: FATAL: invalid identifier provided")
        molecule += oi
    return molecule


#def reduced_mass(m1, m2) -> float:
#    """
#    Calculate reduced mass for general `m1`, `m2` given in kg.
#    """
#    return m1*m2/(m1 + m2)
#
#def reduced_mass_amu(amu1, amu2) -> float:
#    """
#    Calculate reduced mass (in kg) for general `amu1`, `amu2` given in amu.
#    """
#    return reduced_mass(amu2kg*amu1, amu2kg*amu2)

def rescale_length(r, a1, a2, inv=False):
    """
    Rescale the length `r` from units of `a1` to units of `a2`. Passing
    `inv=True` inverts this operation.
    """
    return (a2/a1)*r if inv else (a1/a2)*r

def rescale_energy(E, e1, e2, inv=False):
    """
    Rescale the energy `E` from units of `e1` to units of `e2`. Passing
    `inv=True` inverts this operation.
    """
    return (e2/e1)*E if inv else (e1/e2)*E

def confinement_energy(mu, alpha) -> float:
    """
    Calculate the energy of confinment for a particle of mass `mu` to a length
    `alpha`.
    """
    return hbar**2/(2*mu*alpha**2)

