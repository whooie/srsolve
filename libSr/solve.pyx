# cython: wraparound=False
# cython: boundscheck=False
# cython: embedsignature=True

"""
Provides functions to compute solutions to the 1D Schrödinger equation. Makes
use of the Numerov method [1], the Johnson renormalized Numerov method [2], the
matrix Numerov method [3], and the energy-wavefunction coupling method [4].

Lengths and energies are assumed to be in natural units given by
    
    r' = r/alpha, E' = (2*mu*alpha^2/hbar^2)*E

for some pre-determined length alpha.

[1] B. Numerov, "Note on the numerical integration of d2x/dt2 = f(x,t)."
Astronomische Nachrichten 230 (19) (1927).

[2] B. R. Johnson, "New numerical methods applied to solving the one-dimensional
eigenvalue problem." J. Chem. Phys. 67:4086 (1977).

[3] M. Pillai, J. Goglio, and T. Walker, "Matrix Numerov method for solving
Schrödinger's equation." American Journal of Physics 80 (11):1017-1019 (2012).

[4] A. Udal, R. Reeder, E. Velmre, and P.Harrison, "Comparison of methods for
solving the Schrödinger equation for multiquantum well heterostructure
applications." Proc. Estonian Acad. Sci. Eng. 12 (2006).
"""

import copy

import numpy as np
import numpy.linalg as la
cimport numpy as np

import cython

import libSr.phys as phys
import libSr.interp as interp
from libSr.potentials import MolecularPotential
from libSr.misc import Real

DTYPE = np.float64
ctypedef np.float64_t DTYPE_t
ctypedef fused NPReal:
    np.ndarray[DTYPE_t, ndim=1]
    double

cdef class Solution:
    """
    Holds the energy and wavefunction of a solution. The wavefunction array may
    be empty in the case of a solving method called with `compute_wf=False`.
    Binary comparison operators (<, >, etc.) are defined for sorting purposes
    and act on energy only.

    Attributes
    ----------
    E : float
        Energy.
    u : numpy 1D array or None
        Wavefunction.
    r : numpy 1D array or None
        Spatial coordinates over which `u` is defined.
    """
    cdef public double E
    cdef np.ndarray u, r

    def __init__(self, double E=0, np.ndarray[DTYPE_t, ndim=1] u=None,
            np.ndarray[DTYPE_t, ndim=1] r=None):
        """
        Constructor.

        Parameters
        ----------
        E : float
            Energy.
        u : numpy 1D array or None
            Wavefunction.
        r : numpy 1D array or None
            Spatial coordinates over which `u` is defined.
        """
        self.E = E
        self.u = np.array([], dtype=DTYPE) if u is None else copy.deepcopy(u)
        self.r = np.array([], dtype=DTYPE) if r is None else copy.deepcopy(r)

    @property
    def u(self) -> (np.ndarray, type(None)):
        return self.u
    @u.setter
    def u(self, np.ndarray[DTYPE_t, ndim=1] u) -> None:
        if self.r is not None:
            assert u.shape[0] == self.r.shape[0]
        self.u = copy.deepcopy(u)

    @property
    def r(self) -> (np.ndarray, type(None)):
        return self.r
    @r.setter
    def r(self, np.ndarray[DTYPE_t, ndim=1] r) -> None:
        if self.u is not None:
            assert r.shape[0] == self.u.shape[0]
        self.r = copy.deepcopy(r)

    def __eq__(self, other) -> bint:
        #assert isinstance(other, (*Real, Solution))
        if isinstance(other, Solution):
            return self.E == other.E
        else:
            return self.E == other

    def __gt__(self, other) -> bint:
        #assert isinstance(other, (*Real, Solution))
        if isinstance(other, Solution):
            return self.E > other.E
        else:
            return self.E > other

    def __lt__(self, other) -> bint:
        #assert isinstance(other, (*Real, Solution))
        if isinstance(other, Solution):
            return self.E < other.E
        else:
            return self.E < other
    
    def __ge__(self, other) -> bint:
        return (self > other) or (self == other)

    def __le__(self, other) -> bint:
        return (self < other) or (self == other)

    def __ne__(self, other) -> bint:
        return not (self == other)

    def __len__(self) -> int:
        return len(self.u)

    def rescale_length(self, double a1, double a2, inv=False) -> Solution:
        """
        Rescale the spatial coordinate grid and the wavefunction's normalization
        from units of `a1` to units of `a2`. Passing `inv=True` inverts this.
        """
        cdef np.ndarray[DTYPE_t, ndim=1] _u \
                = (np.sqrt(a1/a2) if inv else np.sqrt(a2/a1))*self.u
        cdef np.ndarray[DTYPE_t, ndim=1] _r \
                = (a2/a1 if inv else a1/a2)*self.r
        return Solution(self.E, _u, _r)

    def rescale_energy(self, double e1, double e2, inv=False) -> Solution:
        """
        Rescale the energy from units of `e1` to units of `e2`. Passing
        `inv=True` inverts this.
        """
        cdef double _E = (e2/e1 if inv else e1/e2)*self.E
        return Solution(_E, self.u, self.r)

cpdef np.ndarray[DTYPE_t, ndim=1] numerov(np.ndarray[DTYPE_t, ndim=1] r,
        np.ndarray[DTYPE_t, ndim=1] V, double E, int l=0, double j2=0):
    """
    Use the Numerov method to solve the radial time-independent Schrodinger
    equation in u(r) for potential `V` at energy `E` with angular momentum `l`.

    Parameters
    ----------
    r : numpy 1D array
        Spatial grid over which u(r) is solved.
    V : numpy 1D array
        Potential energy in units of E_alpha. Should be of equal shape to `r`.
        Should not contain a centrifugal barrier.
    E : float
        Energy at which to integrate u(r) in units of E_alpha.
    l : int
        Orbital angular momentum quantum number.
    j2 : float >= 0 (optional)
        Extra angular momentum to add on (results in the addition of an extra
        `j2`*hbar^2/2*mu*r^2 term in the governing equation).
    
    Returns
    -------
    u : numpy 1D array
        Normalized u(r) wavefunction.
    """ 
    assert r.shape[0] == V.shape[0]
    assert l >= 0
    
    # integration quantities
    cdef double dr = np.abs(r[1] - r[0])
    cdef double a = dr**2/12

    # add in the centrifugal barrier if necessary
    cdef np.ndarray[DTYPE_t, ndim=1] g = E - (V + (l*(l+1)+j2)/r**2)
    cdef double[:] _g = g

    # initialize the integrated function array
    cdef np.ndarray[DTYPE_t, ndim=1] u = np.zeros(r.shape[0], dtype=DTYPE)
    cdef double[:] _u = u
    _u[1] = dr # set boundary condition: u(~r[0]) = 1

    # perform Numerov
    cdef unsigned int i
    for i in range(2, r.shape[0]):
        _u[i] = (2*(1 - 5*a*_g[i-1])*_u[i-1] - (1 + a*_g[i-2])*_u[i-2])/(1 + a*_g[i])

    # normalize
    u = u/np.sqrt(np.trapz(u**2, dx=dr))

    return u

cpdef np.ndarray[DTYPE_t, ndim=1] _renorm_numerov_T(
        np.ndarray[DTYPE_t, ndim=1] r, np.ndarray[DTYPE_t, ndim=1] V, double E,
        int l=0, double j2=0):
    """
    Compute `T`, the rescaled forcing term in the TISE.

    Parameters
    ----------
    r : numpy 1D array
        Spatial grid.
    V : numpy 1D array
        Potential energy in units of E_alpha. Should be of equal shape to `r`.
        Should not contain a centrifugal barrier.
    E : float
        Energy in units of E_alpha.
    l : int
        Orbital angular momentum quantum number.
    j2 : float >= 0 (optional)
        Extra angular momentum to add on (results in the addition of an extra
        `j2`*hbar^2/2*mu*r^2 term in the governing equation).
    
    Returns
    -------
    T : numpy 1D array
        Rescaled forcing function.
    """
    cdef double dr = np.abs(r[1] - r[0])
    cdef np.ndarray[DTYPE_t, ndim=1] T = -dr**2/12*(E - (V + (l*(l+1)+j2)/r**2))
    return T

cpdef np.ndarray[DTYPE_t, ndim=1] _renorm_numerov_U(
        np.ndarray[DTYPE_t, ndim=1] T):
    """
    Compute `U`, the 'renormalized' forcing term for the renormalized
    wavefunction `R`.

    Parameters
    ----------
    T : numpy 1D array
        Rescaled forcing term in the regular TISE.

    Returns
    -------
    U : numpy 1D array
        'Renormalized' forcing term.
    """
    cdef np.ndarray[DTYPE_t, ndim=1] U = (2 + 10*T)/(1 - T)
    return U

cpdef np.ndarray[DTYPE_t, ndim=1] _renorm_numerov_R_l(
        np.ndarray[DTYPE_t, ndim=1] U_l):
    """
    Perform the left-to-right integration of the renormalized wavefunction `R`.
    Assumes a zero boundary condition at the left side.

    Parameters
    ----------
    U_l : numpy 1D array
        'Renormalized' forcing term to the left of (and including) the matching
        point.

    Returns
    -------
    R_l : numpy 1D array
        Renormalized wavefunction.
    """
    # initialize the integrated function array and apply zero-boundary BC
    cdef Py_ssize_t N = U_l.shape[0]
    cdef np.ndarray[DTYPE_t, ndim=1] R_l = np.zeros(N, dtype=DTYPE)
    cdef double[:] _R_l = R_l
    _R_l[0] = np.inf

    # integrate
    cdef unsigned int i
    cdef double[:] _U_l = U_l
    for i in range(1, N):
        _R_l[i] = _U_l[i] - 1/_R_l[i-1]

    return R_l

cpdef np.ndarray[DTYPE_t, ndim=1] _renorm_numerov_R_r(
        np.ndarray[DTYPE_t, ndim=1] U):
    """
    Perform the right-to-left integration of the renormalized wavefunction `R`.
    Assumes a zero boundary condition at the right side. Integration
    automatically stops when `R` is less than or equal to one, when the
    matching point is determined.

    Parameters
    ----------
    U : numpy 1D array
        'Renormalized' forcing term.

    Returns
    -------
    R_r : numpy 1D array
        Renormalized wavefunction to the right of (and including) the matching
        point. May be of shorter length than `U`.
    """
    # initialize the integrated function array and apply zero-boundary BC
    cdef Py_ssize_t N = U.shape[0]
    cdef np.ndarray[DTYPE_t, ndim=1] R_r = np.zeros(N, dtype=DTYPE)
    cdef double[:] _R_r = R_r
    _R_r[N-1] = np.inf

    # integrate
    cdef unsigned int i
    cdef double[:] _U = U
    for i in range(N-2, -1, -1):
        _R_r[i] = _U[i] - 1/_R_r[i+1]
        if _R_r[i] <= 1:
            break

    return R_r[i:N]

cpdef np.ndarray[DTYPE_t, ndim=1] _renorm_numerov_R(
        np.ndarray[DTYPE_t, ndim=1] T):
    """
    Integrate `R`, the renormalized wavefunction.

    Parameters
    ----------
    T : numpy 1D array
        Rescaled forcing function.
    
    Returns
    -------
    R : numpy 1D array
        Renormalized R wavefunction.
    """
    # compute the forcing term in the recursion relation
    cdef np.ndarray[DTYPE_t, ndim=1] U = (2 + 10*T)/(1 - T)
    cdef double[:] _U = U

    # initialize the integrated function arrays
    cdef Py_ssize_t N = T.shape[0]
    cdef np.ndarray[DTYPE_t, ndim=1] R = np.zeros(N, dtype=DTYPE)
    cdef double[:] _R = R
    _R[0] = np.inf

    # integrate
    cdef unsigned int i
    for i in range(1, N):
        _R[i] = _U[i] - 1/_R[i-1]

    return R

cpdef np.ndarray[DTYPE_t, ndim=1] _renorm_numerov_u_l(
        np.ndarray[DTYPE_t, ndim=1] T_l, np.ndarray[DTYPE_t, ndim=1] R_l):
    """
    Back out from the renormalized wavefunction `R` to the regular
    (non-normalized) wavefunction `u`. Assumes operation on `R_l`.

    Parameters
    ----------
    T_l : numpy 1D array
        Rescaled forcing term in the TISE to the left of (and including) the
        matching point.
    R_l : numpy 1D array
        Left-to-right-integrated renormalized wavefunction.

    Returns
    -------
    u_l : numpy 1D array
        Regular wavefunction to the left of (and including) the matching point.
    """
    # initialize the wavefunction array and apply zero-value BC
    cdef Py_ssize_t N = R_l.shape[0]
    cdef np.ndarray[DTYPE_t, ndim=1] u_l = np.zeros(N, dtype=DTYPE)
    cdef double[:] _u_l = u_l
    _u_l[N-1] = 1/(1 - T_l[N-1])

    # compute `u` from the right end of `R`
    cdef double[:] _T_l = T_l
    cdef double[:] _R_l = R_l
    cdef unsigned int i
    for i in range(N-2, -1, -1):
        _u_l[i] = (1 - _T_l[i+1])/(1 - _T_l[i])*_u_l[i+1]/_R_l[i]
    cdef long[:] i_inf = np.where(u_l == np.inf)[0]
    if i_inf.shape[0] > 0:
        u_l[i_inf] = 0

    return u_l

cpdef np.ndarray[DTYPE_t, ndim=1] _renorm_numerov_u_r(
        np.ndarray[DTYPE_t, ndim=1] T_r, np.ndarray[DTYPE_t, ndim=1] R_r):
    """
    Back out from the renormalized wavefunction `R` to the regular
    (non-normalized) wavefunction `u`. Assumes operation on `R_r`.

    Parameters
    ----------
    T_r : numpy 1D array
        Rescaled forcing term in the TISE to the right of (and including) the
        matching point.
    R_r : numpy 1D array
        Right-to-left-integrated renormalized wavefunction.

    Returns
    -------
    u_r : numpy 1D array
        Regular wavefunction to the right of (and including) the matching point.
    """
    # initialize the wavefunction array and apply zero-value BC
    cdef Py_ssize_t N = R_r.shape[0]
    cdef np.ndarray[DTYPE_t, ndim=1] u_r = np.zeros(N, dtype=DTYPE)
    cdef double[:] _u_r = u_r
    _u_r[0] = 1/(1 - T_r[0])

    # compute `u` from the left end of `R`
    cdef double[:] _T_r = T_r
    cdef double[:] _R_r = R_r
    cdef unsigned int i
    for i in range(1, N):
        _u_r[i] = (1 - _T_r[i-1])/(1 - _T_r[i])*_u_r[i-1]/_R_r[i]
    cdef long[:] i_inf = np.where(u_r == np.inf)[0]
    if i_inf.shape[0] > 0:
        u_r[i_inf] = 0

    return u_r

cpdef np.ndarray[DTYPE_t, ndim=1] renorm_numerov_u(
        np.ndarray[DTYPE_t, ndim=1] T, np.ndarray[DTYPE_t, ndim=1] R):
    """
    Back out from the renormalized wavefunction `R` to the regular
    (non-normalized) wavefunction `u`. Assumes operation on `R_l`.

    Parameters
    ----------
    T : numpy 1D array
        Rescaled forcing term in the TISE to the left of (and including) the
        matching point.
    R : numpy 1D array
        Left-to-right-integrated renormalized wavefunction.

    Returns
    -------
    u_l : numpy 1D array
        Regular wavefunction to the left of (and including) the matching point.
    """
    return _renorm_numerov_u_l(T, R)

cpdef np.ndarray[DTYPE_t, ndim=1] renorm_numerov(np.ndarray[DTYPE_t, ndim=1] r,
        np.ndarray[DTYPE_t, ndim=1] V, double E, int l=0, double j2=0):
    """
    Use the Johnson renormalized Numerov method to solve the 1D time-independent
    Schrodinger equation in u(r) for potential `V` at energy `E` with angular
    momentum number `l`.

    Parameters
    ----------
    r : numpy 1D array
        Spatial grid over which u(r) is solved.
    V : numpy 1D array
        Potential energy in units of E_alpha. Should be of equal shape to `r`.
        Should not contain a centrifugal barrier.
    E : float
        Energy at which to integrate u(r) in units of E_alpha.
    l : int
        Orbital angular momentum quantum number.
    j2 : float >= 0 (optional)
        Extra angular momentum to add on (results in the addition of an extra
        `j2`*hbar^2/2*mu*r^2 term in the governing equation).
    
    Returns
    -------
    u : numpy 1D array
        Normalized u(r) wavefunction.
    """
    assert r.shape[0] == V.shape[0]
    assert l >= 0
    assert j2 >= 0

    # method quantities
    cdef Py_ssize_t N = r.shape[0]
    cdef double dr = np.abs(r[1] - r[0])
    cdef np.ndarray[DTYPE_t, ndim=1] T = _renorm_numerov_T(r, V, E, l, j2)
    cdef np.ndarray[DTYPE_t, ndim=1] U = _renorm_numerov_U(T)

    # perform the R_r integration to find the matching point (index) `M` and
    # finish the integration of R from the other side
    cdef np.ndarray[DTYPE_t, ndim=1] R_r = _renorm_numerov_R_r(U)
    cdef unsigned int M = N - R_r.shape[0]
    cdef np.ndarray[DTYPE_t, ndim=1] R_l = _renorm_numerov_R_l(U[:M+1])

    # back out to the wavefunction
    cdef np.ndarray[DTYPE_t, ndim=1] u_r = _renorm_numerov_u_r(T[M:], R_r)
    cdef np.ndarray[DTYPE_t, ndim=1] u_l = _renorm_numerov_u_l(T[:M+1], R_l)
    cdef np.ndarray[DTYPE_t, ndim=1] u = np.zeros(N, dtype=DTYPE)
    u[:M+1] = u_l
    u[M:] = u_r
    u = u/np.sqrt(np.trapz(u**2, dx=dr))

    return u

cpdef np.ndarray[DTYPE_t, ndim=1] renorm_numerov_scatter(
        np.ndarray[DTYPE_t, ndim=1] r, np.ndarray[DTYPE_t, ndim=1] V, double E,
        int l=0, double j2=0):
    """
    Use the Johnson renormalized Numerov method to solve the 1D time-independent
    Schrodinger equation in u(r) for potential `V` at energy `E` with angular
    momentum number `l` by integrating only from the left. This should only be
    used for scattering (unbound) states.

    Parameters
    ----------
    r : numpy 1D array
        Spatial grid over which u(r) is solved.
    V : numpy 1D array
        Potential energy in units of E_alpha. Should be of equal shape to `r`.
        Should not contain a centrifugal barrier.
    E : float
        Energy at which to integrate u(r) in units of E_alpha.
    l : int
        Orbital angular momentum quantum number.
    j2 : float >= 0 (optional)
        Extra angular momentum to add on (results in the addition of an extra
        `j2`*hbar^2/2*mu*r^2 term in the governing equation).
    
    Returns
    -------
    u : numpy 1D array
        Normalized u(r) wavefunction.
    """
    assert r.shape[0] == V.shape[0]
    assert l >= 0
    assert j2 >= 0

    cdef np.ndarray[DTYPE_t, ndim=1] T = _renorm_numerov_T(r, V, E, l, j2)
    cdef np.ndarray[DTYPE_t, ndim=1] U = _renorm_numerov_U(T)
    cdef np.ndarray[DTYPE_t, ndim=1] R = _renorm_numerov_R_l(U)
    cdef np.ndarray[DTYPE_t, ndim=1] u = _renorm_numerov_u_l(T, R)

    return u

cpdef np.ndarray[DTYPE_t, ndim=1] _renorm_numerov_dlog(double dr,
        np.ndarray[DTYPE_t, ndim=1] T, np.ndarray[DTYPE_t, ndim=1] R):
    """
    Compute the log-derivative of the wavefunction from the renormalized
    wavefunction `R` and the rescaled forcing function `T`.

    Parameters
    ----------
    dr : float
        Spatial grid spacing.
    T : numpy 1D array
        Rescaled forcing function.
    R : numpy 1D array
        Renormalized wavefunction.

    Returns
    -------
    dlog : numpy 1D array
        Log-derivative of the wavefunction.
    """
    # method quantities
    cdef Py_ssize_t N = T.shape[0]
    cdef double[:] _T = T
    cdef np.ndarray[DTYPE_t, ndim=1] A = (0.5 - T)/(1 - T)
    cdef double[:] _A = A
    cdef double[:] _R = R

    # back out to the log-derivative; normalization factors don't matter here
    cdef np.ndarray[DTYPE_t, ndim=1] dlog = np.zeros(N, dtype=DTYPE)
    cdef double[:] _dlog = dlog
    cdef unsigned int i
    for i in range(1, N-1):
        _dlog[i] = (1 - _T[i])*(_A[i+1]*_R[i] - _A[i-1]/_R[i-1])/dr
    _dlog[0] = _dlog[1]
    _dlog[N-1] = _dlog[N-2]

    return dlog

cpdef np.ndarray[DTYPE_t, ndim=1] renorm_numerov_dlog(
        np.ndarray[DTYPE_t, ndim=1] r, np.ndarray[DTYPE_t, ndim=1] V, double E,
        int l=0, double j2=0):
    """
    Use the Johnson renormalized Numerov method to solve the 1D time-independent
    Schrodinger equation in u(r) for potential `V` at energy `E` with angular
    momentum number `l` and compute its log-derivative, u'/u.

    Parameters
    ----------
    r : numpy 1D array
        Spatial grid over which u(r) is solved.
    E : float
        Energy at which to integrate u(r) in units of E_alpha.
    V : numpy 1D array
        Potential energy in units of E_alpha. Should be of equal shape to `r`.
        Should not contain a centrifugal barrier.
    l : int
        Orbital angular momentum quantum number.
    j2 : float >= 0 (optional)
        Extra angular momentum to add on (results in the addition of an extra
        `j2`*hbar^2/2*mu*r^2 term in the governing equation).
    
    Returns
    -------
    dlog : numpy 1D array
        Log-derivative of the normalized u(r) wavefunction.
    """
    assert r.shape[0] == V.shape[0]
    assert l >= 0
    assert j2 >= 0

    cdef double dr = np.abs(r[1] - r[0])
    cdef np.ndarray[DTYPE_t, ndim=1] T = _renorm_numerov_T(r, V, E, l, j2)
    cdef np.ndarray[DTYPE_t, ndim=1] R = _renorm_numerov_R(T)
    cdef np.ndarray[DTYPE_t, ndim=1] dlog = _renorm_numerov_dlog(dr, T, R)

    return dlog

cpdef long node_count(np.ndarray[DTYPE_t, ndim=1] r,
        np.ndarray[DTYPE_t, ndim=1] V, double E, int l=0, double j2=0):
    """
    Count the nodes of a wavefunction defined over `r` at energy `E` in
    potential `V`.

    Parameters
    ----------
    r : numpy 1D array
        Spatial grid in alpha scaled units.
    V : Potential energy in units of E_alpha. Should be of equal shape to `r`.
        Should not contain a centrifugal barrier.
    E : float
        Energy in E_alpha scaled units.
    l : int >= 0 (optional)
        Orbital angular momentum quantum number.
    j2 : float >= 0 (optional)
        Extra angular momentum to add on (results in the addition of an extra
        `j2`*hbar^2/2*mu*r^2 term in the governing equation).

    Returns
    -------
    v : int
        Number of nodes found in the wavefunction.
    """
    assert r.shape[0] == V.shape[0]
    assert l >= 0
    assert j2 >= 0

    cdef Py_ssize_t N = r.shape[0]
    
    #cdef np.ndarray[DTYPE_t, ndim=1] T = _renorm_numerov_T(r, V, E, l, j2)
    #cdef np.ndarray[DTYPE_t, ndim=1] R = _renorm_numerov_R(T)
    #return np.sum(((E > V)[1:N-1]*R[1:N-1]) < 0)
    
    cdef np.ndarray[DTYPE_t, ndim=1] u = renorm_numerov(r, V, E, l, j2)
    return np.sum((E > V + (l*(l+1)+j2)/r**2)[1:]*(u[1:N]*u[0:N-1] < 0))

def shoot_two_sided_single(np.ndarray[DTYPE_t, ndim=1] r,
        np.ndarray[DTYPE_t, ndim=1] V, double E, int l=0, double j2=0) \
        -> double:
    """
    Compute the difference in log-derivatives between left-to-right- and
    right-to-left-integrated solutions at a single energy.

    Parameters
    ----------
    r : numpy 1D array
        Spatial grid in alpha scaled units.
    V : Potential energy in units of E_alpha. Should be of equal shape to `r`.
        Should not contain a centrifugal barrier.
    E : float
        Energy in E_alpha scaled units.
    l : int >= 0
        Orbital angular momentum quantum number.
    j2 : float >= 0 (optional)
        Extra angular momentum to add on (results in the addition of an extra
        `j2`*hbar^2/2*mu*r^2 term in the governing equation).

    Returns
    -------
    dlog_diff : float
        Difference in log-derivative at the matching point.
    """
    assert r.shape[0] == V.shape[0]
    assert l >= 0
    assert j2 >= 0

    # method quantities
    cdef Py_ssize_t N = r.shape[0]
    cdef np.ndarray[DTYPE_t, ndim=1] T = _renorm_numerov_T(r, V, E, l, j2)
    cdef np.ndarray[DTYPE_t, ndim=1] U = _renorm_numerov_U(T)
    
    # perform the R_r integration to find the matching point (index) `M` and
    # then finish the integration of R from the other side
    cdef np.ndarray[DTYPE_t, ndim=1] R_r = _renorm_numerov_R_r(U)
    cdef unsigned int M = max(N - R_r.shape[0], 2)
    cdef np.ndarray[DTYPE_t, ndim=1] R_l = _renorm_numerov_R_l(U[:M+1])

    #return 1/R_r[0] - R_l[M]
    return ((0.5 - T[M+1])/(1 - T[M+1])*(1/R_r[1] - R_l[M]) \
            - (0.5 - T[M-1])/(1 - T[M-1])*(R_r[0] - 1/R_l[M-1]))*(1 - T[M])

def shoot_two_sided(np.ndarray[DTYPE_t, ndim=1] r,
        np.ndarray[DTYPE_t, ndim=1] V, double[:] E, int l=0, double j2=0) \
        -> np.ndarray:
    """
    Implement the two-sided shooting method with energy E as the varied
    parameter. This function should only be used to find potential solutions for
    bound wavefunctions in the potential.

    Parameters
    ----------
    r : numpy 1D array
        Spatial grid over which u(r) is solved.
    E : numpy 1D array
        Array of energies in units of E_alpha.
    V : numpy 1D array
        Potential energy units of E_alpha. Should be of equal shape to `r`.
        Should not contain a centrifugal barrier.
    l : int >= 0
        Orbital angular momentum quantum number.
    j2 : float >= 0 (optional)
        Extra angular momentum to add on (results in the addition of an extra
        `j2`*hbar^2/2*mu*r^2 term in the governing equation).
    
    Returns
    -------
    dlog_diff : numpy 1D array
        Values of the difference in the log derivatives of the left-to-right-
        and right-to-left-integrated solutions, evaluated at the matching point.
    """
    cdef Py_ssize_t n = E.shape[0]
    cdef np.ndarray[DTYPE_t, ndim=1] dlog_diff = np.zeros(n, dtype=DTYPE)
    cdef double[:] _dlog_diff = dlog_diff
    cdef unsigned int i
    for i in range(n):
        _dlog_diff[i] = shoot_two_sided_single(r, V, E[i], l, j2)

    return dlog_diff

def matrix_numerov(np.ndarray[DTYPE_t, ndim=1] r, np.ndarray[DTYPE_t, ndim=1] V,
        int l=0, double j2=0, bint compute_wf=True) -> list[Solution]:
    """
    Use the matrix Numerov method to solve for up to N energies and
    eigenfunctions of the radial time-independent Schrodingen equation in u(r)
    for potential `V` with angular momentum `l`, where N is the size of the grid
    specified in `rgrid`. Returned energies and eigenfunctions will sorted in
    order of increasing energy.

    Parameters
    ----------
    r : numpy 1D array
        Spatial grid over which u(r) is solved.
    V : numpy 1D array
        Potential energy in units of E_alpha. Should be of equal shape to `r`.
        Should not contain a centrifugal barrier.
    l : int >= 0 (optional)
        Orbital angular momentum quantum number.
    j2 : float >= 0 (optional)
        Extra angular momentum to add on (results in the addition of an extra
        `j2`*hbar^2/2*mu*r^2 term in the governing equation).
    compute_wf : bool (optional)
        Compute the normalized wavefunctions alongside energies.

    Returns
    -------
    EU : list[Solution]
        List of Solution objects. See `help(libSr.solve.Solution)` for more
        information.
    """
    assert r.shape[0] == V.shape[0]
    assert l >= 0
    assert j2 >= 0

    # method constants
    cdef double dr = np.abs(r[1] - r[0])
    cdef Py_ssize_t N = r.shape[0]
    
    # add in the centrifugal barrier if necessary
    cdef np.ndarray[DTYPE_t, ndim=1] v = V + (l*(l+1)+j2)/r**2
    cdef double[:] _v = v

    # construct matrices
    cdef np.ndarray[DTYPE_t, ndim=2] A = np.zeros((N, N), dtype=DTYPE)
    np.fill_diagonal(A[:,:], -2/dr**2)
    np.fill_diagonal(A[:N-1,1:], 1/dr**2)
    np.fill_diagonal(A[1:,:N-1], 1/dr**2)

    cdef np.ndarray[DTYPE_t, ndim=2] B = np.zeros((N, N), dtype=DTYPE)
    np.fill_diagonal(B[:,:], 10/12)
    np.fill_diagonal(B[:N-1,1:], 1/12)
    np.fill_diagonal(B[1:,:N-1], 1/12)

    cdef np.ndarray[DTYPE_t, ndim=2] V_mat = np.diag(_v)

    cdef unsigned int j
    if compute_wf: 
        E, U = la.eigh(-la.inv(B)@A + V_mat)
        return [Solution(E[j], U.T[j]/np.sqrt(np.trapz(U[:,j]**2, dx=dr)), r) for j in range(N)]
    else:
        E = la.eigvalsh(-la.inv(B)@A + V_mat)
        return [Solution(E[j]) for j in range(N)]

cdef np.ndarray[DTYPE_t, ndim=2] _ewc_system(double dr,
        np.ndarray[DTYPE_t, ndim=1] y, np.ndarray[DTYPE_t, ndim=1] v):
    """
    Construct the EWC system comprising one iteration of the total method.

    Parameters
    ----------
    dr : float
        Grid spacing in r.
    y : numpy 1D array
        Sampled values of the approximate energy-coupled wavefunction. Should be
        of the form (considering `u` to be zero-indexed)
            y = [E, u[1], u[2], ..., u[N-1]]
    v : numpy 1D array
        Total potential energy (i.e. including a centrifugal barrier) in units
        of E_alpha.

    Returns
    -------
    JF : numpy 2D array
        The linear system combined into one NxN+1 array, where the first N
        columns are the Jacobi matrix J, and the last is the negative of the
        constraining vector F.
    """
    # method constants
    cdef double E = y[0]
    cdef double c = 1/dr**2
    cdef Py_ssize_t N = y.shape[0]
    y[0] = y[N-1]

    # construct the system
    cdef np.ndarray[DTYPE_t, ndim=2] JF = np.zeros((N, N+1), dtype=DTYPE)
    cdef double[:,:] _J = JF[0:N,0:N]
    cdef double[:] _F = JF[:,N]
    cdef unsigned int i
    _F[0] = -(dr*np.sum(y[1:N]**2) - 1)
    #for i in range(1, N-1):
    #    _F[i] = -(c*(y[i+1] - 2*y[i] + y[i-1]) + (E - v[i])*y[i])
    _F[1:N-1] = -(c*(y[2:N] - 2*y[1:N-1] + y[0:N-2]) + (E - v[1:N-1])*y[1:N-1])
    _F[N-1] = -y[N-1]

    _J[0,1:] = 2*y[1:]
    for i in range(1, N-1):
        _J[i,i-1] = c
        _J[i,i] = -2*c + E - v[i]
        _J[i,i+1] = c
        _J[i,0] = y[i]
    _J[1,N-1] = c
    _J[N-1,N-1] = 1

    return JF

def ewc(np.ndarray[DTYPE_t, ndim=1] r, np.ndarray[DTYPE_t, ndim=1] u,
        np.ndarray[DTYPE_t, ndim=1] V, double E, int l=0, double j2=0,
        double EPSILON=1e-6, int maxiters=20) -> (np.ndarray, np.float64, int):
    """
    Implement the EWC iterative method to solve the radial time-independent
    Schrodinger equation in `u` for potential `V` at energy `E` and angular
    momentum `l`.
    
    Parameters
    ----------
    r : numpy 1D array
        Spatial coordinate values in the grid over which the initial guess `u`
        is sampled.
    u : numpy 1D array
        The initial guess for the u(r) radial wavefunction sampled over the
        coordinates in `r`.
    E : float
        The initial guess for the energy of `u` in units of E_alpha.
    V : numpy 1D array
        Potential energy in units of E_alpha. Should be of equal shape to the
        spatial grid. Should not contain a centrifugal barrier.
    l : int >= 0 (optional)
        Orbital angular momentum quantum number.
    j2 : float >= 0 (optional)
        Extra angular momentum to add on (results in the addition of an extra
        `j2`*hbar^2/2*mu*r^2 term in the governing equation).
    EPSILON: float > 0 (optional)
        Set the tolerance in the magnitude of the correction vector to detect
        convergence.
    maxiters : int > 0 (optional)
        Maximum number of iterations to compute if convergence is not reached.
    
    Returns
    -------
    u : numpy 1D array
        Normalized u(r) wavefunction at the points specified in `r`.
    E : float
        Energy of the wavefunction in units of E_alpha.
    k : int
        Number of iterations before termination.
    """
    assert r.shape[0] == V.shape[0]
    assert r.shape[0] == u.shape[0]
    assert l >= 0
    assert j2 >= 0
    assert EPSILON > 0
    assert maxiters > 0

    # method constants
    cdef double dr = np.abs(r[1] - r[0])
    cdef double[:] y = np.copy(u)
    y[0] = E
    cdef unsigned int N = u.shape[0]

    # add in a centrifugal barrier if necessary
    cdef np.ndarray[DTYPE_t, ndim=1] v = V + (l*(l+1)+j2)/r**2
    cdef double[:] _v = v

    # perform EWC
    cdef np.ndarray[DTYPE_t, ndim=1] dy = np.zeros(N, dtype=DTYPE)
    cdef np.ndarray[DTYPE_t, ndim=2] JF = np.zeros((N, N+1), dtype=DTYPE)
    cdef double[:,:] _J = JF[0:N,0:N]
    cdef double[:] _F = JF[:,N]
    cdef unsigned int k
    for k in range(maxiters):
        JF = _ewc_system(dr, y, v)
        try:
            dy = la.solve(_J, _F)
        except la.LinAlgError:
            print(f"{__name__}.ewc: singular matrix encountered; break iteration loop")
            break
        y += dy
        if abs(dy[0]) < EPSILON:
            break

    E_ = y[0]
    cdef np.ndarray[DTYPE_t, ndim=1] u_ = np.copy(y)
    u_[0] = 0

    return (u_, E_, k+1)

@cython.wraparound(True)
@cython.boundscheck(True)
def solve_matrix_numerov(np.ndarray[DTYPE_t, ndim=1] r,
        np.ndarray[DTYPE_t, ndim=1] V, int l=0, double j2=0, E_filter=None,
        list idx=[], bint compute_wf=True) -> list[Solution]:
    """
    Solve for energy, wavefunction solutions to the TISE for the given potential
    grid `V` over spatial grid `r` via the matrix-Numerov method.

    Parameters
    ----------
    r, V : numpy 1D array
        Spatial and potential grids to solve over.
    l : int >= 0 (optional)
        Orbital angular momentum quantum number.
    j2 : float >= 0 (optional)
        Extra angular momentum to add on (results in the addition of an extra
        `j2`*hbar^2/2*mu*r^2 term in the governing equation).
    E_filter : function (optional)
        Filter function to apply to the list of Solution objects.
    idx : list[int] (optional)
        Indices to select from the list of (energy, wavefunction) tuples
        returned by the solving method function.
    compute_wf : bool (optional)
        Compute the wavefunctions for each energy solution found.

    Returns
    -------
    EU : list[Solution]
        List of Solution objects. See `help(libSr.solve.Solution)` for more
        information.
    """
    cdef list EU = matrix_numerov(r, V, l, j2, compute_wf)
    if E_filter is not None:
        EU = list(filter(E_filter, EU))
    EU.sort()
    cdef int i
    if len(idx) > 0:
        EU = [EU[i] for i in idx]
    return EU

@cython.wraparound(True)
@cython.boundscheck(True)
def solve_shooting(np.ndarray[DTYPE_t, ndim=1] r, np.ndarray[DTYPE_t, ndim=1] V,
        double[:] E, int l=0, double j2=0, E_filter=None, list idx=[],
        bint compute_wf=True) -> list[Solution]:
    """
    Solve for energy, wavefunction solutions to the TISE for the given potential
    grid `V` over spatial grid `r` via the Johnson renormalized (shooting)
    method.

    Parameters
    ----------
    r, V : numpy 1D array
        Spatial and potential grids to solve over.
    E : numpy 1D array
        Array of energies at which to perform the shooting method.
    l : int >= 0 (optional)
        Orbital angular momentum quantum number.
    j2 : float >= 0 (optional)
        Extra angular momentum to add on (results in the addition of an extra
        `j2`*hbar^2/2*mu*r^2 term in the governing equation).
    E_filter : function (optional)
        Filter function to apply to the list of Solution objects.
    idx : list[int] (optional)
        Indices to select from the list of (energy, wavefunction) tuples
        returned by the solving method function.
    compute_wf : bool (optional)
        Compute the wavefunctions for each energy solution found.

    Returns
    -------
    EU : list[Solution]
        List of Solution objects. See `help(libSr.solve.Solution)` for more
        information.
    """
    cdef np.ndarray[DTYPE_t, ndim=1] dlog_diff = shoot_two_sided(r, V, E, l, j2)
    cdef list e = interp.find_zeros(E, dlog_diff, kind="rising")
    cdef list EU
    cdef int i
    if compute_wf:
        EU = [Solution(e[i], renorm_numerov(r, V, e[i], l, j2), r) for i in range(len(e))]
    else:
        EU = [Solution(e[i]) for i in range(len(e))]
    if E_filter is not None:
        EU = list(filter(E_filter, EU))
    if len(idx) > 0:
        EU = [EU[i] for i in idx]
    return EU

@cython.wraparound(True)
@cython.boundscheck(True)
def solve_johnson(np.ndarray[DTYPE_t, ndim=1] r,
        np.ndarray[DTYPE_t, ndim=1] V, tuple bounds, E0=None,
        nu=None, int l=0, double j2=0, bint compute_wf=True,
        double EPSILON=1e-6, int maxiters=500) -> list[Solution]:
    """
    Solve for a specific energy, wavefunction solution to the TISE for the given
    potential grid `V` over spatial grid `r` via the Johnson renormalized
    log-derivative searching method. `E0` and `bounds` are used to control the
    initial guessing values of the search.

    Parameters
    ----------
    r, V : numpy 1D array
        Spatial and potential grids to solve over.
    bounds : (float, float)
        (E_L, E_H) defining (initial) lower and upper boundaries for the search.
    E0 : float (optional)
        Initial guess for the energy solution.
    nu : int >= 0 (optional)
        Vibrational level of the solution (or equivalently the number of nodes
        in its wavefunction). If left undefined, set as that of the initial
        guess energy.
    l : int >= 0 (optional)
        Orbital angular momentum quantum number.
    j2 : float >= 0 (optional)
        Extra angular momentum to add on (results in the addition of an extra
        `j2`*hbar^2/2*mu*r^2 term in the governing equation).
    compute_wf : bool (optional)
        Compute the wavefunctions for each energy solution found.
    EPSILON : float > 0 (optional)
        The minimum value that the log-derivative difference in the search
        method can attain before convergence is detected.
    maxiters : int > 0 (optional)
        The maximum number of search iterations in each step (node counting,
        then log-derivative calculations) of the search process.

    Returns
    -------
    EU : list[Solution]
        List of Solution objects. See `help(libSr.solve.Solution)` for more
        information. Only one Solution is returned in this case, but it's placed
        in a list to agree with the behavior of the other solve methods.
    """
    assert EPSILON > 0
    assert maxiters > 0
    
    cdef double E_L, E_H, E, Elast, D_L, D_H, D
    cdef int v, n, n_L, n_H
    cdef unsigned int i
    cdef np.ndarray[DTYPE_t, ndim=1] wf
    
    # establish initial values
    E_L, E_H = bounds
    if E0 is None:
        E = (E_L + E_H)/2
    else:
        E = E0
    n = node_count(r, V, E, l, j2)
    v = n if nu is None else abs(nu)

    # get upper and lower bounds inside the vibrational range
    for i in range(maxiters):
        n_L = node_count(r, V, E_L, l, j2)
        n_H = node_count(r, V, E_H, l, j2)
        if n_L == v and n_H == v:
            break
        if n < v:
            E_L = E
            E = (E_L + E_H)/2
            n = node_count(r, V, E, l, j2)
        elif n > v:
            E_H = E
            E = (E_L + E_H)/2
            n = node_count(r, V, E, l, j2)
        else:
            E_L = (E_L + E)/2 if n_L < v else E_L
            E_H = (E + E_H)/2 if n_H > v else E_H
    if n_L != v or n_H != v:
        raise ValueError("solve.solve_johnson: FATAL: vibrational level search failed to find correct interval")
    if i == maxiters-1:
        print("solve.solve_johnson: WARNING: vibrational level search reached maxiters")

    # now find the energy solution
    for i in range(maxiters):
        D_L = shoot_two_sided_single(r, V, E_L, l, j2)
        D_H = shoot_two_sided_single(r, V, E_H, l, j2)
        while D_H == D_L:
            E_H += EPSILON*abs(E_H)
            E_L -= EPSILON*abs(E_L)
            D_H = shoot_two_sided_single(r, V, E_H, l, j2)
            D_L = shoot_two_sided_single(r, V, E_L, l, j2)
        Elast = E
        E = -D_H*(E_H - E_L)/(D_H - D_L) + E_H # secant method
        n = node_count(r, V, E, l, j2)
        while n > v: # if secant method extrapolates above the vibrational level
            E = E_H + abs(E - E_H)/2
            n = node_count(r, V, E, l, j2)
        while n < v: # if secant method extrapolates below the vibrational level
            E = E_L - abs(E - E_L)/2
            n = node_count(r, V, E, l, j2)
        D = shoot_two_sided_single(r, V, E, l, j2)
        if D_L*D_H > 0:
            (E_L, E_H) = (E, E_L) if D_L > 0 else (E_H, E)
        elif D_L*D_H < 0:
            if D > 0:
                E_H = E
            elif D < 0:
                E_L = E
        else:
            E = E_H if D_H == 0 else E_L
            break
        if (abs(2*(E_H-E_L)/(E_H+E_L)) < EPSILON and D_H > 0 and D_L < 0) \
                or abs(2*(E-Elast)/(E_H+E_L)) < EPSILON:
            break
    if i == maxiters-1:
        print("solve.solve_johnson: WARNING: energy convergence reached maxiters")

    if compute_wf:
        return [Solution(E, renorm_numerov(r, V, E, l, j2), r)]
    else:
        return [Solution(E)]

def solve(np.ndarray[DTYPE_t, ndim=1] r, np.ndarray[DTYPE_t, ndim=1] V, int l=0,
        double j2=0, E_filter=None, idx=[], bint compute_wf=True,
        str method="matrix-numerov", **kwargs) -> list[Solution]:
    """
    Wrapper around methods to solve for the energy and wavefunction
    solutions to the TISE for the given potential.

    Parameters
    ----------
    r, V : numpy 1D array
        Spatial and potential grids to solve over.
    l : int >= 0 (optional)
        Orbital angular momentum quantum number.
    j2 : float >= 0 (optional)
        Extra angular momentum to add on (results in the addition of an extra
        `j2`*hbar^2/2*mu*r^2 term in the governing equation).
    E_filter : function (optional)
        Filter function to apply to the list of Solution objects.
    idx : list[int] (optional)
        Indices to select from the list of (energy, wavefunction) tuples
        returned by the solving method function.
    compute_wf : bool (optional)
        Compute the wavefunctions for each energy solution found.
    method : str in {'matrix-numerov', 'shooting', 'johnson'}
        Determines the method used to compute solutions.
    **kwargs
        Additional method-dependent arguments:

        method='shooting' parameters
        ----------------------------
        E : numpy 1D array
            Array of energies at which to perform the shooting method.

        method='johnson' parameters
        ---------------------------
        bounds : (float, float)
            (E_L, E_H) defining (initial) lower and upper boundaries for the
            search.
        E0 : float
            Initial guess for the energy solution.
        nu : int >= 0 (optional)
            Vibrational level of the solution energy (or equivalently the number
            of nodes in its wavefunction). If left undefined, set as that of the
            initial guess energy.
        EPSILON : float > 0 (optional)
            The minimum value that the log-derivative difference in the search
            method can attain before convergence is detected.
        maxiters : int > 0 (optional)
            The maximum number of search iterations in each step (node counting,
            then log-derivative calculations) of the search process.

    Returns
    -------
    EU : list[Solution]
        List of Solution objects. See `help(libSr.solve.Solution)` for more
        information.
    """
    if method == "matrix-numerov":
        return solve_matrix_numerov(r, V, l, j2, E_filter, idx, compute_wf)
    elif method == "shooting":
        E = kwargs.get("E", None)
        if E is None:
            raise Exception("solve.solve: kwarg `E` must be defined for method='shooting'")
        return solve_shooting(r, V, E, l, j2, E_filter, idx, compute_wf)
    elif method == "johnson":
        bounds = kwargs.get("bounds", None)
        if bounds is None:
            raise Exception("solve.solve: kwarg `bounds` must be defined for method='johnson'")
        E0 = kwargs.get("E0", None)
        nu = kwargs.get("nu", None)
        EPSILON = kwargs.get("EPSILON", 1e-6)
        maxiters = kwargs.get("maxiters", 500)
        return solve_johnson(r, V, bounds, E0, nu, l, j2, compute_wf, EPSILON, maxiters)
    else:
        raise Exception(f"solve.solve: Invalid method '{method}'")

def multisolve(*solves) -> list[list[Solution]]:
    """
    Perform multiple calls of `solve.solve`. See `help(libSr.solve.solve)` for
    expected arguments.

    Parameters
    ----------
    *solves
        Dictionaries of keyword arguments to pass to each call of `solve.solve`.

    Returns
    -------
    [EU] : list[list[Solution]]
        List of returns from each call of `solve.solve`.
    """
    cdef list _EU_ = list()
    cdef dict _solve_
    for _solve_ in solves:
        _EU_.append(solve(**_solve_))
    return _EU_

cdef class Solver:
    """
    Simple class to drive the solving methods for general potentials.

    Attributes
    ----------
    r : numpy 1D array
        Spatial grid coordinates.
    V : numpy 1D array
        Potential energy. Does not contain a centrifugal barrier.
    """
    cdef public np.ndarray x, V

    def __init__(self, np.ndarray[DTYPE_t, ndim=1] x,
            np.ndarray[DTYPE_t, ndim=1] V):
        """
        Constructor.

        Parameters
        ----------
        r, V : numpy 1D array
            Spatial and potential grids to solve over.
        """
        self.x = copy.deepcopy(x)
        self.V = copy.deepcopy(V)

    def solve(self, *args, **kwargs) -> list[Solution]:
        """
        Thin wrapper around `libSr.solve.solve` for `r = self.x` and
        `V = self.V`. See `help(libSr.solve.solve)` for more information on
        appropriate args and kwargs.
        """
        return solve(r=self.x, V=self.V, *args, **kwargs)

    def multisolve(self, *solves) -> list[list[Solution]]:
        """
        Thin wrapper around `libSr.solve.multisolve` for `r = self.x` and
        `V = self.V`. See `help(libSr.solve.solve)` for more information on
        appropriate args and kwargs for each solve computed.
        """
        _solves = copy.deepcopy(solves)
        for _solve in _solves:
            _solve.update({"r": self.x, "V": self.V})
        return multisolve(*_solves)

    def node_count(self, *args, **kwargs) -> long:
        """
        Thin wrapper around `libSr.solve.node_count` for `r = self.x` and
        `V = self.V`. See `help(libSr.solve.node_count)` for more information on
        appropriate args and kwargs.
        """
        return node_count(self.x, self.V, *args, **kwargs)


cdef class MPSolver:
    """
    Simple class to drive the solving methods for MolecularPotential objects and
    compactly contain relevant information.

    Attributes
    ----------
    mu : float
        Reduced mass of the dimer in kilograms.
    alpha : float
        Length scaling factor in meters, determined as the range of the
        potential `pot`.
    Ealpha : float
        Natural unit of energy corresponding to `alpha`.
    r : numpy 1D array
        Grid defining the solving region in natural units.
    rmin, Vmin : float
        Location and value of the potential minimum in natural units.
    Vinf : float
        Long-range asymptote of the potential in natural units.
    pot : libSr.potentials.MolecularPotential
        Molecular potential of the dimer.
    """
    cdef public np.ndarray r
    cdef public double mu, alpha, Ealpha
    cdef public double rmin, Vmin, Vinf
    cdef public pot

    def __init__(self, double mu, pot, Emax=None, r=None,
            dict Vmin_kwargs=dict(), dict Vinf_kwargs=dict(),
            dict inner_tp_kwargs=dict(), dict inner_ext_kwargs=dict(),
            dict outer_tp_kwargs=dict(), dict outer_ext_kwargs=dict(),
            **potparams):
        """
        Constructor. `Emax` and `r` are used to determine the spatial grid.
        If both are supplied, `r` overrides `Emax`. At least one of the two must
        be given.

        Parameters
        ----------
        mu : float
            Reduced mass of the dimer in kilograms.
        pot : libSr.potentials.MolecularPotential
            Molecular potential of the dimer.
        Emax : float (optional)
            Estimation of the least-bound energy in Joules, used to determine
            the spatial grid if `r` is not supplied.
        r : numpy 1D array (optional)
            Spatial grid override option. Coordinates are expected in units
            of the range of the potential given.
        Vmin_kwargs : dict (optional)
            Keyword arguments to pass to the potential minimum-finding function.
        Vinf_kwargs : dict (optional)
            Keyword arguments to pass to the function evaluating the long-range
            asymptote of the potential.
        inner_tp_kwargs : dict (optional)
            Keyword arguments to pass to the function finding the inner turning
            point of the potential.
        inner_ext_kwargs : dict (optional)
            Keyword arguments to pass to the function finding the extension into
            the inner forbidden region of the potential.
        outer_tp_kwargs : dict (optional)
            Keyword arguments to pass to the function finding the outer turning
            point of the potential.
        outer_ext_kwargs : dict (optional)
            Keyword arguments to pass to the function finding the extension into
            the outer forbidden region of the potential.
        **potparams
            Potential parameters.
        """
        assert isinstance(pot, MolecularPotential)
        assert not (Emax is None and r is None)

        self.mu = mu
        self.pot = copy.deepcopy(pot)
        self.pot.update_params(**potparams)
        self.alpha = self.pot.rescale_length(self.pot.range(mu), 1, inv=True)
        self.Ealpha = phys.hbar**2/2/mu/self.alpha**2
        if r is None:
            self.r = self._generate_r(Emax, Vmin_kwargs, inner_tp_kwargs,
                inner_ext_kwargs, outer_tp_kwargs, outer_ext_kwargs)
        else:
            self.r = copy.deepcopy(r)
        cdef _rmin, _Vmin, _Vinf
        _rmin, _Vmin = self.pot.minimum(**Vmin_kwargs)
        _Vinf = self.pot.lim_inf(**Vinf_kwargs)
        self.rmin = self.pot.rescale_length(_rmin, self.alpha, inv=True)
        self.Vmin = self.pot.rescale_energy(_Vmin, self.Ealpha, inv=True)
        self.Vinf = self.pot.rescale_energy(_Vinf, self.Ealpha, inv=True)

    def _generate_r(self, double Emax, dict Vmin_kwargs=dict(),
            dict inner_tp_kwargs=dict(), dict inner_ext_kwargs=dict(),
            dict outer_tp_kwargs=dict(), dict outer_ext_kwargs=dict()) \
            -> np.ndarray:
        cdef double _Emax = self.pot.rescale_energy(Emax, 1)
        cdef double _rmin = self.pot.inner_turning_point(_Emax, **inner_tp_kwargs) \
                - self.pot.inner_extension(self.mu, _Emax, **inner_ext_kwargs)
        cdef double _rmax = self.pot.outer_turning_point(_Emax, **outer_tp_kwargs) \
                + self.pot.outer_extension(self.mu, _Emax, **outer_ext_kwargs)
        cdef double _dr = self.pot.min_wavelength(self.mu, _Emax, **Vmin_kwargs)/15
        try:
            r = np.arange(
                self.pot.rescale_length(_rmin, self.alpha, inv=True),
                self.pot.rescale_length(_rmax, self.alpha, inv=True),
                self.pot.rescale_length(_dr, self.alpha, inv=True),
                dtype=DTYPE
            )
        except ValueError as err:
            print(_rmin, _rmax, _dr)
            raise err
        return r

    def node_count(self, *args, **kwargs) -> long:
        """
        Thin wrapper around `libSr.solve.node_count` for `r = self.r` and
        `V = self.V(self.r)`. See `help(libSr.solve.node_count)` for more
        information on appropriate args and kwargs.
        """
        return node_count(self.r, self.V(self.r), *args, **kwargs)

    def V(self, NPReal r) -> NPReal:
        """
        Functional form of the strontium potential in scaled units, using
        potential parameters supplied upon construction.

        Parameters
        ----------
        r : float or numpy 1D array
            Radial coordinate or array of coordinates.
        """
        cdef NPReal _r = self.pot.rescale_length(r, self.alpha)
        return self.pot.rescale_energy(self.pot(_r, order=0), self.Ealpha, inv=True)

    def dV(self, NPReal r) -> NPReal:
        """
        Functional form of the first derivative of the strontium potential in
        scaled units, using potential parameters supplied upon construction.

        Parameters
        ----------
        r : float or numpy 1D array
            Radial coordinate or array of coordinates.
        """
        cdef NPReal _r = self.pot.rescale_length(r, self.alpha)
        return self.pot.rescale_energy(self.pot(_r, order=1), self.Ealpha, inv=True)

    def d2V(self, NPReal r) -> NPReal:
        """
        Functional form of the second derivative of the strontium potential in
        scaled units, using potential parameters supplied upon construction.

        Parameters
        ----------
        r : float or numpy 1D array
            Radial coordinate or array of coordinates.
        """
        cdef NPReal _r = self.pot.rescale_length(r, self.alpha)
        return self.pot.rescale_energy(self.pot(_r, order=2), self.Ealpha, inv=True)

    def solve(self, **kwargs) -> list[Solution]:
        """
        Thin wrapper around `libSr.solve.solve` with `r = self.r` and
        `V = self.V(self.r)`. See `help(libSr.solve.solve)` for more information
        on appropriate kwargs.
        """
        cdef dict _kwargs = copy.deepcopy(kwargs)
        _kwargs.update({
            "r": self.r,
            "V": self.V(self.r),
            "bounds": _kwargs.get("bounds", (self.Vmin, self.Vinf))
        })
        return solve(**_kwargs)

    def multisolve(self, *solves) -> list[list[Solution]]:
        """
        Thin wrapper around `libSr.solve.multisolve` for `r = self.r` and
        `V = self.V(self.r)`. See `help(libSr.solve.solve)` for more information
        on appropriate kwargs for each solve computed.
        """
        cdef list _solves = copy.deepcopy(solves)
        cdef dict _solve
        for _solve in _solves:
            _solve.update({
                "r": self.r,
                "V": self.V(self.r),
                "bounds": _solve.get("bounds", (self.Vmin, self.Vinf))
            })
        return multisolve(*_solves)

    def rescale_length(self, NPReal r, double a, bint inv=False) -> NPReal:
        """
        Rescale the length `r` between units of `self.alpha` and units of `a`.
        The default operation (`inv=False`) is to do the conversion
            [a] -> [self.alpha]
        Setting `inv=True` inverts this to do the conversion
            [self.alpha] -> [a]

        Parameters
        ----------
        r : float or numpy ndarray
            Length to be rescaled.
        a : float
            New unit length in meters.
        inv : bool (optional)
            Invert the default unit conversion.

        Returns
        -------
        rnew : float or numpy ndarray
            Length `r` rescaled to units of `a`.
        """
        return (self.alpha/a)*r if inv else (a/self.alpha)*r

    def rescale_energy(self, NPReal E, double e, bint inv=False) -> NPReal:
        """
        Rescale the energy `E` between units of `self.Ealpha` and units of `e`.
        The default operation (`inv=False`) is to do the conversion
            [e] -> [self.Ealpha]
        Setting `inv=True` inverts this to do the conversion
            [self.Ealpha] -> [e]

        Parameters
        ----------
        E : float or numpy ndarray
            Energy to be rescaled.
        e : float
            New unit energy in Joules.
        inv : bool (optional)
            Invert the default unit conversion.

        Returns
        -------
        Enew : float or numpy ndarray
            Energy `E` rescaled to units of `e`.
        """
        return (self.Ealpha/e)*E if inv else (e/self.Ealpha)*E

    def rescale_meter(self, NPReal r, bint inv=False) -> NPReal:
        """
        Rescale a length in meters to units of `self.alpha`. Passing
        `inv=True` inverts this conversion.
        """
        return self.rescale_length(r, 1, inv)

    def rescale_bohr(self, NPReal r, bint inv=False) -> NPReal:
        """
        Rescale a length in Bohr radii to units of `self.alpha`. Passing
        `inv=True` inverts this conversion.
        """
        return self.rescale_length(r, phys.a0, inv)

    def rescale_joule(self, NPReal E, bint inv=False) -> NPReal:
        """
        Rescale an energy in Joules to units of `self.Ealpha`. Passing
        `inv=True` inverts this conversion.
        """
        return self.rescale_energy(E, 1, inv)

    def rescale_wavenumber(self, NPReal E, bint inv=False) -> NPReal:
        """
        Rescale an energy in cm^-1 to units of `self.Ealpha`. Passing
        `inv=True` inverts this conversion.
        """
        return self.rescale_energy(E, 100*phys.h*phys.c, inv)

    def rescale_hertz(self, NPReal E, bint inv=False) -> NPReal:
        """
        Rescale an energy in Hertz to units of `self.Ealpha`. Passing
        `inv=True` inverts this conversion.
        """
        return self.rescale_energy(E, phys.h, inv)

    def rescale_kelvin(self, NPReal E, bint inv=False) -> NPReal:
        """
        Rescale an energy in Kelvin to units of `self.Ealpha`. Passing
        `inv=True` inverts this conversion.
        """
        return self.rescale_energy(E, phys.kB, inv)

    def rescale_hartree(self, NPReal E, bint inv=False) -> NPReal:
        """
        Rescale an energy in Hartree energies to units of `self.Ealpha`. Passing
        `inv=True` inverts this conversion.
        """
        return self.rescale_energy(E, phys.Eh, inv)

    def rescale_electronvolt(self, NPReal E, bint inv=False) -> NPReal:
        """
        Rescale an energy in electronvolts to units of `self.Ealpha`. Passing
        `inv=True` inverts this conversion.
        """
        return self.rescale_energy(E, phys.e, inv)

# name retained for backwards-compatibility
cdef class SrSolver(MPSolver):
    pass

