"""
Functions to compute the Sr potential provided by Stein et al.
All functions can handle as `r` inputs of type float, int, or numpy array.
Suggested parameters from the paper are contained in `PARAMS`.
    
A. Stein, H. Knöckel, and E. Tiemann, "The 1S+1S asymptote of Sr2 studied by
Fourier-transform spectroscopy." Eur. Phys. J. D 57 (171) (2010).
"""

import numpy as np
import libSr.phys as phys
from libSr.potentials.potentials import MolecularPotential
from libSr.misc import typecheck, NPReal, Parameter

DTYPE = np.float64

# default potential parameters
PARAMS = {
    "A": -1.3328825e3,          # cm^-1
    "B": +3.321662099e10,       # cm^-1 Ao^`n`
    "n": +12.362,               # dimensionless
    "T_m": -1081.6384,          # cm^-1
    "a_i": [ # a_1, a_2, ...    # cm^-1
        -6.5000000000e-02,
        +1.5939056000e+04,
        -2.9646778000e+04,
        -6.2697770000e+03,
        +4.4952358000e+04,
        +8.7090160000e+03,
        -1.0054929000e+05,
        +5.9478415200e+05,
        -9.9523912600e+05,
        -1.1449671700e+07,
        +4.6064630550e+07,
        +3.7466657300e+07,
        -5.4391571460e+08,
        +9.3648339400e+08,
        +1.3878797480e+09,
        -8.4009054730e+09,
        +1.5781752106e+10,
        -1.5721037673e+10,
        +8.3760430610e+09,
        -1.8898488000e+09
    ],
    "b": -0.17,                 # dimensionless
    "R_m": +4.6719018,          # Ao
    "U_inf": 0,                 # cm^-1
    "C_6": +1.525e7,            # cm^-1 Ao^6
    "C_8": +5.159e8,            # cm^-1 Ao^8
    "C_10": +1.91e10,           # cm^-1 Ao^10
    "R_i": +3.963,              # Ao
    "R_a": +10.5                # Ao
}
PARAMSu = {
    "C_6": 0.005e7,
    "C_8": 1.5e8,
    "C_10": 0.27e10
}

@typecheck({
    "r": NPReal,
    "A": Parameter,
    "B": Parameter,
    "n": Parameter
})
def V_short(r, A=None, B=None, n=None, **params) -> NPReal:
    """
    Short-range portion of the strontium potential. Returns float or numpy array
    corresponding to the type of `r` as

                          B
        V_short(r) = A + ---
                         r^n

    Parameters `A`, `B`, and `n` are optionally adjustable, with default values
    as defined in Stein et al. 2010.
    """
    _A = PARAMS["A"] if A is None else A
    _B = PARAMS["B"] if B is None else B
    _n = PARAMS["n"] if n is None else n
    return _A + _B/r**_n

@typecheck({
    "r": NPReal,
    "A": Parameter,
    "B": Parameter,
    "n": Parameter
})
def dV_short(r, A=None, B=None, n=None, **params) -> NPReal:
    """
    First derivative of the short-range portion of the strontium potential.
    Returns float or numpy array corresponding to the type of `r` as

        dV_short            B
        --------(r) = -n -------
           dr            r^(n+1)

    Parameters `A`, `B`, and `n` are optionally adjustable, with default values
    as defined in Stein et al. 2010.
    """
    _B = PARAMS["B"] if B is None else B
    _n = PARAMS["n"] if n is None else n
    return -_n*_B/r**(_n+1)

@typecheck({
    "r": NPReal,
    "A": Parameter,
    "B": Parameter,
    "n": Parameter
})
def d2V_short(r, A=None, B=None, n=None, **params) -> NPReal:
    """
    Second derivative of the short-range portion of the strontium potential.
    Returns float or numpy array corresponding to the type of `r` as

        d2V_short                 B
        ---------(r) = n (n+1) -------
           dr2                 r^(n+2)

    Parameters `A`, `B`, and `n` are optionally adjustable, with default values
    as defined in Stein et al. 2010.
    """
    _B = PARAMS["B"] if B is None else B
    _n = PARAMS["n"] if n is None else n
    return _n*(_n+1)*_B/r**(_n+2)

@typecheck({
    "r": NPReal,
    "T_m": Parameter,
    "a_i": (list, type(None)),
    "b": Parameter,
    "R_m": Parameter
})
def V_middle(r, T_m=None, a_i=None, b=None, R_m=None, **params) -> NPReal:
    """
    Mid-range portion of the strontium potential. Returns float or numpy array
    corresponding tothe type of `r` as

                                      r - R_m
        V_middle(r) = T_m + sum[a_i (---------)^i]
                           i >= 1    r + b R_m

    Parameters `T_m`, `a`, `b`, and `R_m` are optionally adjustable, with
    default values as defined in Stein et al. 2010.
    """
    _T_m = PARAMS["T_m"] if T_m is None else T_m
    _a_i = PARAMS["a_i"] if a_i is None else a_i
    _b = PARAMS["b"] if b is None else b
    _R_m = PARAMS["R_m"] if R_m is None else R_m

    x = (r - _R_m)/(r + _b*_R_m)

    #cdef unsigned int i
    #return _T_m + sum([_a_i[i]*x**(i+1) for i in range(len(_a_i))])
    return np.polyval(_a_i[::-1]+type(_a_i)([_T_m]), x)

@typecheck({
    "r": NPReal,
    "T_m": Parameter,
    "a_i": (list, type(None)),
    "b": Parameter,
    "R_m": Parameter
})
def dV_middle(r, T_m=None, a_i=None, b=None, R_m=None, **params) -> NPReal:
    """
    First derivative of the mid-range portion of the strontium potential.
    Returns float or numpy array corresponding to the type of `r` as

        dV_middle                  (b+1) R_m     r - R_m
        ---------(r) = sum[i a_i ------------- (---------)^(i-1)]
            dr        i >= 1     (r + b R_m)^2  r + b R_m

    Parameters `T_m`, `a`, `b`, and `R_m` are optionally adjustable, with
    default values as defined in Stein et al. 2010.
    """
    _a_i = PARAMS["a_i"] if a_i is None else a_i
    _b = PARAMS["b"] if b is None else b
    _R_m = PARAMS["R_m"] if R_m is None else R_m

    x = (r - _R_m)/(r + _b*_R_m)
    dx = (_b+1)*_R_m/(r + _b*_R_m)**2
    da_i = np.arange(1, len(_a_i)+1, dtype=DTYPE)*np.array(_a_i)
    
    return dx*np.polyval(da_i[::-1], x)

@typecheck({
    "r": NPReal,
    "T_m": Parameter,
    "a_i": (list, type(None)),
    "b": Parameter,
    "R_m": Parameter
})
def d2V_middle(r, T_m=None, a_i=None, b=None, R_m=None, **params) -> NPReal:
    """
    Second derivative of the mid-range portion of the strontium potential.
    Returns float or numpy array corresponding to the type of `r` as

        d2V_middle                         (b+1) R_m        r - R_m
        ----------(r) = sum[i (i-1) a_i (-------------)^2 (---------)^(i-2)]
            dr2        i >= 2            (r + b R_m)^2     r + b R_m
                                       -2       (b+1) R_m     r - R_m
                        + sum[i a_i --------- ------------- (---------)^(i-1)]
                         i >= 1     r + b R_m (r + b R_m)^2  r + b R_m

    Parameters `T_m`, `a`, `b`, and `R_m` are optionally adjustable, with
    default values as defined in Stein et al. 2010.
    """
    _a_i = PARAMS["a_i"] if a_i is None else a_i
    _b = PARAMS["b"] if b is None else b
    _R_m = PARAMS["R_m"] if R_m is None else R_m

    x = (r - _R_m)/(r + _b*_R_m)
    dx = (_b+1)*_R_m/(r + _b*_R_m)**2
    d2x = -2/(r + _b*_R_m)*dx
    i = np.arange(1, len(_a_i)+1, dtype=DTYPE)
    da_i = i*np.array(_a_i)
    d2a_i = i*(i-1)*np.array(_a_i)

    return dx**2*np.polyval(d2a_i[len(_a_i)-1:0:-1], x) + d2x*np.polyval(da_i[::-1], x)

@typecheck({
    "r": NPReal,
    "U_inf": Parameter,
    "C_6": Parameter,
    "C_8": Parameter,
    "C_10": Parameter
})
def V_long(r, U_inf=None, C_6=None, C_8=None, C_10=None, **params) -> NPReal:
    """
    Long-range portion of the strontium potential. Returns float or numpy array
    corresponding to the type of `r` as

                            C_6   C_8   C_10
        V_long(r) = U_inf - --- - --- - ----
                            r^6   r^8   r^10

    Parameters `U_inf`, `C_6`, `C_8`, and `C_10` are optionally adjustable, with
    default values as defined in Stein et al. 2010.
    """
    _U_inf = PARAMS["U_inf"] if U_inf is None else U_inf
    _C_6 = PARAMS["C_6"] if C_6 is None else C_6
    _C_8 = PARAMS["C_8"] if C_8 is None else C_8
    _C_10 = PARAMS["C_10"] if C_10 is None else C_10
    return _U_inf - _C_6/r**6 - _C_8/r**8 - _C_10/r**10

@typecheck({
    "r": NPReal,
    "U_inf": Parameter,
    "C_6": Parameter,
    "C_8": Parameter,
    "C_10": Parameter
})
def dV_long(r, U_inf=None, C_6=None, C_8=None, C_10=None, **params) -> NPReal:
    """
    First derivative of the long-range portion of the strontium potential.
    Returns float or numpy array corresponding to the type of `r` as

        dV_long        C_6     C_8      C_10
        -------(r) = 6 --- + 8 --- + 10 ----
           dr          r^7     r^9      r^11

    Parameters `U_inf`, `C_6`, `C_8`, and `C_10` are optionally adjustable, with
    default values as defined in Stein et al. 2010.
    """
    _C_6 = PARAMS["C_6"] if C_6 is None else C_6
    _C_8 = PARAMS["C_8"] if C_8 is None else C_8
    _C_10 = PARAMS["C_10"] if C_10 is None else C_10
    return 6*_C_6/r**7 + 8*_C_8/r**9 + 10*_C_10/r**11

@typecheck({
    "r": NPReal,
    "U_inf": Parameter,
    "C_6": Parameter,
    "C_8": Parameter,
    "C_10": Parameter
})
def d2V_long(r, U_inf=None, C_6=None, C_8=None, C_10=None, **params) -> NPReal:
    """
    First derivative of the long-range portion of the strontium potential.
    Returns float or numpy array corresponding to the type of `r` as

        d2V_long          C_6       C_8       C_10
        --------(r) = -42 --- - 72 ---- - 110 ----
           dr2            r^8      r^10       r^12

    Parameters `U_inf`, `C_6`, `C_8`, and `C_10` are optionally adjustable, with
    default values as defined in Stein et al. 2010.
    """
    _C_6 = PARAMS["C_6"] if C_6 is None else C_6
    _C_8 = PARAMS["C_8"] if C_8 is None else C_8
    _C_10 = PARAMS["C_10"] if C_10 is None else C_10
    return -42*_C_6/r**8 + 72*_C_8/r**10 - 110*_C_10/r**12

@typecheck({
    "r": NPReal,
    "R_i": Parameter,
    "R_a": Parameter
})
def V(r, R_i=None, R_a=None, **params) -> NPReal:
    """
    Full strontium potential. Returns float or numpy array corresponding to the
    type of `r` as

               | V_short(r)     if 0 < R_i
        V(r) = | V_middle(r)    if R_i <= r <= R_a
               | V_long(r)      if r > R_a

    Parameters `R_i` and `R_a` are optionally adjustable, with default values as
    defined in Stein et al. 2010. All the parameters of `V_short`, `V_middle`,
    and `V_long` are also optionally adjustable as keyword arguments.
    """
    _R_i = PARAMS["R_i"] if R_i is None else R_i
    _R_a = PARAMS["R_a"] if R_a is None else R_a

    res = (r > 0)*(r < _R_i) * V_short(r, **params) \
            + (r >= _R_i) * (r <= _R_a) * V_middle(r, **params) \
            + (r > _R_a) * V_long(r, **params)
    return res

@typecheck({
    "r": NPReal,
    "R_i": Parameter,
    "R_a": Parameter
})
def dV(r, R_i=None, R_a=None, **params) -> NPReal:
    """
    First derivative of the full strontium potential. Returns float or numpy
    array corresponding to the type of `r` as

        dV      | dV_short/dr(r)    if 0 < R_i
        --(r) = | dV_middle/dr(r)   if R_i <= r <= R_a
        dr      | dV_long/dr(r)     if r > R_a

    Parameters `R_i` and `R_a` are optionally adjustable, with default values as
    defined in Stein et al. 2010. All the parameters of `dV_short`, `dV_middle`,
    and `dV_long` are also optionally adjustable as keyword arguments.
    """
    _R_i = PARAMS["R_i"] if R_i is None else R_i
    _R_a = PARAMS["R_a"] if R_a is None else R_a

    res = (r > 0)*(r < _R_i) * dV_short(r, **params) \
            + (r >= _R_i) * (r <= _R_a) * dV_middle(r, **params) \
            + (r > _R_a) * dV_long(r, **params)
    return res

@typecheck({
    "r": NPReal,
    "R_i": Parameter,
    "R_a": Parameter
})
def d2V(r, R_i=None, R_a=None, **params) -> NPReal:
    """
    Second derivative of the full strontium potential. Returns float or numpy
    array corresponding to the type of `r` as

        d2V      | d2V_short/dr2(r)    if 0 < R_i
        ---(r) = | d2V_middle/dr2(r)   if R_i <= r <= R_a
        dr2      | d2V_long/dr2(r)     if r > R_a

    Parameters `R_i` and `R_a` are optionally adjustable, with default values as
    defined in Stein et al. 2010. All the parameters of `dV_short`, `dV_middle`,
    and `dV_long` are also optionally adjustable as keyword arguments.
    """
    _R_i = PARAMS["R_i"] if R_i is None else R_i
    _R_a = PARAMS["R_a"] if R_a is None else R_a

    res = (r > 0)*(r < _R_i) * d2V_short(r, **params) \
            + (r >= _R_i) * (r <= _R_a) * d2V_middle(r, **params) \
            + (r > _R_a) * d2V_long(r, **params)
    return res

Tiemann = MolecularPotential(
    V, dV, d2V,
    a0=1e-10, E0=100*phys.h*phys.c, units_tag="angstrom -> cm^-1",
    **PARAMS
)

def compute_AB(**params) -> dict:
    """
    Compute the values of A and B for some value of n such that the potential is
    continuous and smooth at the exchange between the short- and mid-range
    portions of the potential.
    """
    _n = params.get("n", PARAMS["n"])
    _R_i = params.get("R_i", PARAMS["R_i"])
    B = -_R_i**(_n+1)/_n*dV_middle(_R_i, **params)
    A = V_middle(_R_i, **params) - B/_R_i**_n
    return {"A": A, "B": B}

def compute_TmAB(**params) -> dict:
    """
    Compute the values of T_m, A, and B for some values of C_6, C_8, and C_10
    such that the potential is continuous at the exchanges between the mid- and
    long-, and the short- and mid-range portions of the potential.
    """
    _C_6 = params.get("C_6", PARAMS["C_6"])
    _C_8 = params.get("C_8", PARAMS["C_8"])
    _C_10 = params.get("C_10", PARAMS["C_10"])
    _R_a = params.get("R_a", PARAMS["R_a"])
    _T_m = params.get("T_m", PARAMS["T_m"])
    dT_m = V_long(_R_a, **params) - V_middle(_R_a, **params)
    new_params = dict()
    new_params.update(params)
    new_params["T_m"] = _T_m+dT_m
    new_params.update(compute_AB(**new_params))
    return new_params

