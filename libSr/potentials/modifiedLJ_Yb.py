"""
Functions to compute a Lennard-Jones potential with added C_8 term.
All functions can handle as `r` inputs of type float, int, or numpy array.
Suggested parameters are taken from Kitagawa et al. 2008.
"""

import numpy as np
import libSr.phys as phys
from libSr.potentials.potentials import MolecularPotential
from libSr.misc import typecheck, NPReal, Parameter

# default potential parameters
PARAMS = {
    "sigma":    9.0109362,  # +/- not given
    "C_6":      1931.7,     # +/- 30.0
    "C_8":      1.93e5,     # +/- 0.50e5
}
PARAMSu = {
    "sigma":    0,
    "C_6":      30,
    "C_8":      0.5e5
}

@typecheck({
    "r": NPReal,
    "sigma": Parameter,
    "C_6": Parameter,
    "C_8": Parameter
})
def V(r, sigma=None, C_6=None, C_8=None, **params) \
        -> NPReal:
    """
    Modified Lennard-Jones potential. Returns float or numpy array corresponding
    to the type of `r` as

                 C_6      sigma^6    C_8
        V(r) = - --- (1 - -------) - ---
                 r^6        r^6      r^8

    Constants `sigma`, `C_6`, `C_8`.
    """
    _sigma = PARAMS["sigma"] if sigma is None else sigma
    _C_6 = PARAMS["C_6"] if C_6 is None else C_6
    _C_8 = PARAMS["C_8"] if C_8 is None else C_8
    return -_C_6/r**6 * (1 - _sigma**6/r**6) - _C_8/r**8

@typecheck({
    "r": NPReal,
    "sigma": Parameter,
    "C_6": Parameter,
    "C_8": Parameter
})
def dV(r, sigma=None, C_6=None, C_8=None, **params) \
        -> NPReal:
    """
    First derivative of the modified Lennard-Jones potential. Returns float or
    numpy array corresponding to the type of `r` as

        dV          C_6 sigma^6     C_6     C_8
        --(r) = -12 ----------- + 6 --- + 8 ---
        dr              r^13        r^7     r^9

    Constants `sigma`, `C_6`, `C_8`.
    """
    _sigma = PARAMS["sigma"] if sigma is None else sigma
    _C_6 = PARAMS["C_6"] if C_6 is None else C_6
    _C_8 = PARAMS["C_8"] if C_8 is None else C_8
    return -12*_C_6*_sigma**6/r**13 + 6*_C_6/r**7 + 8*_C_8/r**9

@typecheck({
    "r": NPReal,
    "sigma": Parameter,
    "C_6": Parameter,
    "C_8": Parameter
})
def d2V(r, sigma=None, C_6=None, C_8=None, **params) \
        -> NPReal:
    """
    Second derivative of the modified Lennard-Jones potential. Returns float or
    numpy array corresponding to the type of `r` as

        dV          C_6 sigma^6      C_6       C_8
        --(r) = 156 ----------- - 42 --- - 72 ----
        dr              r^14         r^8      r^10

    Constants `sigma`, `C_6`, `C_8`.
    """
    _sigma = PARAMS["sigma"] if sigma is None else sigma
    _C_6 = PARAMS["C_6"] if C_6 is None else C_6
    _C_8 = PARAMS["C_8"] if C_8 is None else C_8
    return 156*_C_6*_sigma**6/r**14 - 42*_C_6/r**8 - 72*_C_8/r**10

ModifiedLJ_Yb = MolecularPotential(
    V, dV, d2V,
    a0=phys.a0, E0=phys.Eh, units_tag="Bohr -> Hartree",
    **PARAMS
)

