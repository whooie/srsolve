# cython: wraparound=False
# cython: boundscheck=False
# cython: embedsignature=True

"""
Provides the main MolecularPotential class to house the functional form and
parameter values of molecular potentials.
"""

import copy
import numpy as np
cimport numpy as np
import libSr.phys as phys
from libSr.misc import Function

DTYPE = np.float64
ctypedef np.float64_t DTYPE_t
ctypedef fused NPReal:
    np.ndarray[DTYPE_t, ndim=1]
    double

cdef class MolecularPotential:
    """
    Class to house the functional form and parameter values of molecular
    potentials.

    Attributes
    ----------
    V, dV, d2V : callable
        Python functions giving the forms of the potential and its first two
        derivatives, respectively. These functions follow the signatures
            f(r: [float or numpy array], **params) -> [float or numpy array]
        where `r` is the spatial coordinate and the values of parameters in the
        potential can be supplied as keyword arguments. See the
        `libSr.potentials` submodule for example definitions of these functions.
    a0, e0 : float
        Unit length and energy scales (corresponding to those for the function
        inputs and outputs of `V`, `dV`, and `d2V`) of the potential expressed
        in meters and Joules.
    params : dict
        Numerical values of parameters in the potential.
    units_tag : str
        Identifying tag for the units of the potential. This is a purely
        cosmetic attribute.
    """
    cdef public V, dV, d2V
    cdef public double a0, E0
    cdef public dict params
    cdef public str units_tag

    def __init__(self, V, dV, d2V, double a0=1, double E0=1,
            str units_tag="MKS", **params):
        """
        Constructor.

        Parameters
        ----------
        V, dV, d2V : callable
            Python functions giving the forms of the potential and its first and
            second derivatives, respectively. These functions are expected to
            follow the signatures
                f(r: [float or numpy array], **params) -> [float or numpy array]
            where `r` is the spatial coordinate and the values of parameters in
            the potential are supplied as keyword arguments. The functions are
            expected to return either a float or numpy array corresponding to
            the type of `r`.
        a0 : float (optional)
            Unit length of the spatial coordinate argument in meters.
        E0 : float (optional)
            Unit energy of the computed potential energy quantities in Joules.
        units_tag : str (optional)
            Identifying tag for the units of the potential. This is a purely
            cosmetic attribute.
        **params
            Numerical values of parameters in the potential.
        """
        #assert isinstance(V, (type(lambda:0), Function))
        #assert isinstance(dV, (type(lambda:0), Function))
        #assert isinstance(d2V, (type(lambda:0), Function))
        self.V = V
        self.dV = dV
        self.d2V = d2V
        self.a0 = a0
        self.E0 = E0
        self.units_tag = units_tag
        self.params = params

    def __getitem__(self, str param) -> double:
        return self.params[param]

    def __setitem__(self, str param, double val) -> None:
        self.params[param] = val

    def set_params(self, **params) -> None:
        """
        Set the values of potential parameters. Any parameter values not
        included as arguments to this function call will be lost.
        """
        self.params = params

    def update_params(self, **params) -> None:
        """
        Update the values of potential parameters. Any parameter values not
        included as arguments to this function call will be retained.
        """
        self.params.update(params)

    def __call__(self, r, int order=0) -> NPReal:
        """
        Call `self.V(r, **self.params)`. `self.dV` and `self.d2V` can be
        accessed with `order=1` and `order=2`, respectively.

        Parameters
        ----------
        r : float or numpy ndarray
            Spatial coordinate.
        order : int in {0, 1, 2} (optional)
            Order of the function to call with respect to differentiation.
            `order=0` here refers to the function itself.

        Returns
        -------
        V : float or numpy ndarray
            Value of the function called at `r`.
        """
        assert isinstance(r, (int, float, np.ndarray))
        assert order in {0, 1, 2}

        if order == 0:
            return self.V(r, **self.params)
        elif order == 1:
            return self.dV(r, **self.params)
        elif order == 2:
            return self.d2V(r, **self.params)

    def rescale_length(self, NPReal r, double a, bint inv=False) -> NPReal:
        """
        Rescale the length `r` between units of `self.alpha` and units of `a`.
        The default operation (`inv=False`) is to do the conversion
            [a] -> [self.a0]
        Setting `inv=True` inverts this to do the conversion
            [self.a0] -> [a]

        Parameters
        ----------
        r : float or numpy ndarray
            Length to be rescaled.
        a : float
            New unit length in meters.
        inv : bool (optional)
            Invert the default unit conversion.

        Returns
        -------
        rnew : float or numpy ndarray
            Length `r` rescaled to units of `a`.
        """
        return (self.a0/a)*r if inv else (a/self.a0)*r

    def rescale_energy(self, NPReal E, double e, bint inv=False) -> NPReal:
        """
        Rescale the energy `E` between units of `self.Ealpha` and units of `e`.
        The default operation (`inv=False`) is to do the conversion
            [e] -> [self.E0]
        Setting `inv=True` inverts this to do the conversion
            [self.E0] -> [e]

        Parameters
        ----------
        E : float or numpy ndarray
            Energy to be rescaled.
        e : float
            New unit energy in Joules.
        inv : bool (optional)
            Invert the default unit conversion.

        Returns
        -------
        Enew : float or numpy ndarray
            Energy `E` rescaled to units of `e`.
        """
        return (self.E0/e)*E if inv else (e/self.E0)*E

    def minimum(self, double ri=1, int maxiters=500, double EPSILON=1e-6) \
            -> (double, double):
        """
        Compute the minimum of the potential by way of a Newton's method search.

        Parameters
        ----------
        ri : float (optional)
            Initial guess for the search in units of `self.a0`.
        maxiters : int > 0 (optional)
            Terminate the search after `maxiters` number of steps.
        EPSILON : float > 0 (optional)
            Terminate the search when the step size is less than `EPSILON`.

        Returns
        -------
        rmin : float
            Location of the potential minimum.
        Vmin : float
            Minimum value of the potential.
        """
        assert ri > 0
        assert maxiters > 0
        assert EPSILON > 0

        cdef double rmin = ri
        cdef double dr
        cdef unsigned int i
        for i in range(maxiters):
            dr = -self.dV(rmin, **self.params)/self.d2V(rmin, **self.params)
            rmin += dr
            if abs(dr) < EPSILON:
                break
        if i == maxiters - 1:
            print("MolecularPotential.minimum: WARNING: reached maxiters")
        return rmin, self.V(rmin)

    def range(self, double mu, double Ri=50, int maxiters=500,
            double EPSILON=1e-6, dict lim_inf_kwargs=dict()) -> double:
        """
        Compute the range of the potential by way of a Newton's method search.

        Parameters
        ----------
        mu : float
            Reduced mass of the particles bound in the potential in kilograms.
        Ri : float (optional)
            Initial guess for the search in units of `self.a0`.
        maxiters : int > 0 (optional)
            Terminate the search after `maxiters` number of steps.
        EPSILON : float > 0 (optional)
            Terminate the search when the step size is less than `EPSILON`.
        lim_inf_kwargs : dict (optional)
            Keyword arguments to pass to `self.lim_inf`.

        Returns
        -------
        R : float
            Range of the potential.
        """
        assert Ri > 0
        assert maxiters > 0
        assert EPSILON > 0

        cdef double K = phys.hbar**2/(2*mu)/(self.a0**2*self.E0)
        cdef double Vinf = self.lim_inf(**lim_inf_kwargs)
        cdef double R = Ri
        cdef double dR
        cdef unsigned int i
        for i in range(maxiters):
            dR = -(self.V(R, **self.params)-Vinf + K/R**2)/(self.dV(R, **self.params) - 2*K/R**3)
            R += dR
            if abs(dR) < EPSILON:
                break
        if i == maxiters - 1:
            print("MolecularPotential.range: WARNING: reached maxiters")
        return R

    def lim_inf(self, double ri=1000, double stepsize=100, int maxiters=500,
            double EPSILON=1e-12) -> double:
        """
        Compute the limit of the potential as r -> infinity using Aitken
        extrapolation.

        Parameters
        ----------
        ri : float > 0 (optional)
            Initial point.
        stepsize : float > 0 (optional)
            Distance between evaluation points.
        maxiters : int > 0 (optional)
            Terminate after `maxiters` number of steps.
        EPSILON : float > 0 (optional)
            Terminate when the difference in consecutive evaluations is less
            than `EPSILON`.

        Returns
        -------
        Vlim : float
            Limit of the potential as r -> infinity.
        """
        # using Aitkens is probably overkill for this, but whatever
        assert ri > 0
        assert stepsize > 0
        assert maxiters > 0
        assert EPSILON > 0

        cdef double rn0 = ri
        cdef double rn1 = rn0+stepsize
        cdef double rn2 = rn1+stepsize
        cdef double Vn0 = self.V(rn0, **self.params)
        cdef double Vn1 = self.V(rn1, **self.params)
        cdef double Vn2 = self.V(rn2, **self.params)
        cdef double DVn, D2Vn, AVn
        cdef unsigned int i
        for i in range(maxiters):
            DVn = Vn1 - Vn0
            D2Vn = Vn0 - 2*Vn1 + Vn2
            AVn = Vn0 - DVn**2/D2Vn
            if abs(AVn - Vn2) < EPSILON:
                break
            rn0, rn1, rn2 = rn1, rn2, rn2+stepsize
            Vn0, Vn1, Vn2 = Vn1, Vn2, self.V(rn2, **self.params)
        if i == maxiters - 1:
            print("MolecularPotential.lim_inf: WARNING: reached maxiters")
        return AVn

    def min_wavelength(self, double mu, double E=0,
            dict minimum_kwargs=dict()) -> double:
        """
        Find the minimum wavelength a hypothetical wavefunction at energy `E`
        can attain for use in determining grid spacings.

        Parameters
        ----------
        mu : float
            Reduced mass in kilograms.
        E : float (optional)
            Energy of the wavefunction in units of `self.E0`.
        minimum_kwargs : dict (optional)
            Keyword arguments to pass to `self.minimum`.

        Returns
        -------
        lambda_min : float
            Minimum wavelength attainable in units of `self.a0`.
        """
        cdef double rmin, Vmin
        rmin, Vmin = self.minimum(**minimum_kwargs)
        return 2*np.pi/np.sqrt(2*mu/phys.hbar**2 * self.E0*(E - Vmin))/self.a0

    def turning_point(self, double E, double Ri, int maxiters=500,
            double EPSILON=1e-6) -> double:
        """
        Compute a turning point for a particle of energy `E` by way of a
        Newton's method search.

        Parameters
        ----------
        E : float
            Energy of the particle in units of `self.E0`.
        Ri : float
            Initial guess for the search in units of `self.a0`.
        maxiters : int > 0 (optional)
            Terminate the search after `maxiters` number of steps.
        EPSILON : float > 0 (optional)
            Terminate the search when the step size is less than `EPSILON`.

        Returns
        -------
        R : float
            Outer turning point.
        """
        assert Ri > 0
        assert maxiters > 0
        assert EPSILON > 0

        cdef double R = Ri
        cdef double dR
        cdef unsigned int i
        for i in range(maxiters):
            dR = -(self.V(R, **self.params) - E)/self.dV(R, **self.params)
            R += dR
            if abs(dR) < EPSILON:
                break
        if i == maxiters - 1:
            print("MolecularPotential.turning_point: WARNING: reached maxiters")
        return R

    def inner_turning_point(self, double E, double Ri=1, int maxiters=500,
            double EPSILON=1e-6) -> double:
        """
        Compute the inner turning point for a particle of energy `E` by way of a
        Newton's method search. Thin wrapper for `self.turning_point` with a
        default value `Ri=1`. See `self.turning_point` for more information.
        """
        return self.turning_point(E, Ri, maxiters, EPSILON)

    def outer_turning_point(self, double E, double Ri=50, int maxiters=500,
            double EPSILON=1e-6) -> double:
        """
        Compute the outer turning point for a particle of energy `E` by way of a
        Newton's method search. Thin wrapper for `self.turning_point` with a
        default value `Ri=50`. See `self.turning_point` for more information.
        """
        return self.turning_point(E, Ri, maxiters, EPSILON)

    def inner_extension(self, double mu, double E, double k=1000,
            double n=12, double r0=0.001, dict turning_point_kwargs=dict(),
            dict lim_inf_kwargs=dict(), dict minimum_kwargs=dict()) -> double:
        """
        Compute the length of the extension into the inner classically forbidden
        region necessary for the amplitude of a hypothetical wavefunction at
        energy `E` to be reduced by a factor of `k` > 1. Note that in the
        interest of speed, this computation approximates the inner wall of the
        potential as a function of the form A/r^n + E where A, E, and n are
        constants: n and E are given, and A is calculated to match the slope of
        the true potential at the inner turning point. This approximation
        neglects the negative offset of the A/r^n function that would normally
        occur due to the long-range components of the potential, so this
        computation likely gives a slight under-estimate of the true length. The
        error of this estimate should decrease with increasing `k`.

        Parameters
        ----------
        mu : float
            Reduced mass in kilograms.
        E : float
            Energy of the wavefunction in units of `self.E0`.
        k : float > 1 (optional)
            Wavefunction amplitude reduction factor.
        n : float > 0 (optional)
            Exponent of the approximating A/r^n term.
        r0 : float > 0 (optional)
            Minimum allowable value for the left-hand boundary of the spatial
            grid.
        turning_point_kwargs : dict (optional)
            Keyword arguments to pass to `self.inner_turning_point`.
        lim_inf_kwargs : dict (optional)
            Keyword arguments to pass to `self.lim_inf`.
        minimum_kwargs : dict (optional)
            Keyword arguments to pass to `self.minimum`.

        Returns
        -------
        rext : float > 0
            Length of the extension into the inner classically forbidden region
            in units of `self.a0`.
        """
        assert k > 1
        assert n > 0
        assert r0 > 0

        cdef double Vinf = self.lim_inf(**lim_inf_kwargs)
        cdef double Vmin = self.minimum(**minimum_kwargs)[1]
        cdef double rA = self.inner_turning_point(E, **turning_point_kwargs)
        cdef double Vslope = self.dV(rA, **self.params)
        cdef double qcoeff = np.sqrt(2*mu/phys.hbar**2 * rA/n*self.E0*abs(Vslope))
        cdef double rp = rA/(abs(np.log(1/k))/qcoeff * (n-2)/(2*rA) + 1)**(2/(n-2))
        return min(
            max(
                rA - rp,
                3*(rA - self.inner_turning_point(Vinf - Vmin, **turning_point_kwargs))
            ),
            rA - r0
        )

    def outer_extension(self, double mu, double E, double k=1000,
            dict lim_inf_kwargs=dict()) -> double:
        """
        Compute the length of the extension into the outer classically forbidden
        region necessary for the amplitude of a hypothetical wavefunction at
        energy `E` to be reduced by a factor of `k` > 1. Note that in the
        interest of speed, the computation approximates the potential as a
        constant equal to its limit as r -> infinity and as such likely gives a
        slight under-estimate of the true length. The error of this estimate
        should decrease with increasing `k`.

        Parameters
        ----------
        mu : float
            Reduced mass in kilograms.
        E : float
            Energy of the wavefunction in units of `self.E0`.
        k : float > 1 (optional)
            Wavefunction amplitude reduction factor.
        lim_inf_kwargs : dict (optional)
            Keyword arguments to pass to `self.lim_inf`.

        Returns
        -------
        rext : float > 0
            Length of the extension into the outer classically forbidden region
            in units of `self.a0`.
        """
        assert k > 1

        cdef double Vlim = self.lim_inf(**lim_inf_kwargs)
        return abs(np.log(1/k))/np.sqrt(2*mu/phys.hbar**2 * self.E0*(Vlim - E))/self.a0

