# cython: wraparound=False
# cython: boundscheck=False
# cython: embedsignature=True

"""
Functions to compute the MLR formulation of the Sr potential provided by Stein
et al. 2010. All functions can handle as `r` inputs of type float, int, or numpy
array. Suggested parameters from the paper are contained in `PARAMS`.

A. Stein, H. Knöckel, and E. Tiemann, "The 1S+1S asymptote of Sr2 studied by
Fourier-transform spectroscopy." Eur. Phys. J. D 57 (171) (2010).
"""

import numpy as np
cimport numpy as np
import libSr.phys as phys
from libSr.potentials.potentials import MolecularPotential

DTYPE = np.float64
ctypedef np.float64_t DTYPE_t
ctypedef fused NPReal:
    np.ndarray[DTYPE_t, ndim=1]
    double

# default potential parameters
PARAMS = {
    "D_e": 1081.6352,
    "R_e": 4.6719032,
    "R_ref": 5.5,
    "p": 5,
    "q": 3,
    "beta_i": [ # beta_0, beta_1, ...
        -1.458760592449,
        -0.031511799120,
        -0.912033020316,
        -0.033913017557,
        -0.465124882150,
        +0.287008644909,
        -0.585285048765,
        +2.490073196261,
        +3.065232204927,
        -8.521057394373,
        -0.771989570637,
        +17.679758812808,
        -11.143746759192,
        -1.704727367184
    ],
    "C_6": 1.525e7,
    "C_8": 5.159e8,
    "C_10": 1.910e10
}

def U_LR(NPReal r, C_6=None, C_8=None, C_10=None, **params) \
        -> NPReal:
    """
    Long-range portion of the MLR strontium potential. Returns float or numpy
    array corresponding to the type of `r` as

                  C_6   C_8   C_10
        U_LR(r) = --- + --- + ----
                  r^6   r^8   r^10

    Parameters `C_6`, `C_8`, and `C_10` are optionally adjustable, with default
    values as defined in Stein et al. 2010.
    """
    cdef double _C_6 = PARAMS["C_6"] if C_6 is None else C_6
    cdef double _C_8 = PARAMS["C_8"] if C_8 is None else C_8
    cdef double _C_10 = PARAMS["C_10"] if C_10 is None else C_10
    return _C_6/r**6 + _C_8/r**8 + _C_10/r**10

def dU_LR(NPReal r, C_6=None, C_8=None, C_10=None, **params) \
        -> NPReal:
    """
    First derivative of the long-range portion of the MLR strontium potential.
    Returns float or numpy array corresponding to the type of `r` as

        dU_LR         C_6     C_8      C_10
        -----(r) = -6 --- - 8 --- - 10 ----
          dr          r^7     r^9      r^11

    Parameters `C_6`, `C_8`, and `C_10` are optionally adjustable, with default
    values as defined in Stein et al. 2010.
    """
    cdef double _C_6 = PARAMS["C_6"] if C_6 is None else C_6
    cdef double _C_8 = PARAMS["C_8"] if C_8 is None else C_8
    cdef double _C_10 = PARAMS["C_10"] if C_10 is None else C_10
    return -6*_C_6/r**7 - 8*_C_8/r**9 - 10*_C_10/r**11

def d2U_LR(NPReal r, C_6=None, C_8=None, C_10=None, **params) \
        -> NPReal:
    """
    Second derivative of the long-range portion of the MLR strontium potential.
    Returns float or numpy array corresponding to the type of `r` as

        d2U_LR         C_6       C_8       C_10
        ------(r) = 42 --- + 72 ---- + 110 ----
          dr2          r^8      r^10       r^12

    Parameters `C_6`, `C_8`, and `C_10` are optionally adjustable, with default
    values as defined in Stein et al. 2010.
    """
    cdef double _C_6 = PARAMS["C_6"] if C_6 is None else C_6
    cdef double _C_8 = PARAMS["C_8"] if C_8 is None else C_8
    cdef double _C_10 = PARAMS["C_10"] if C_10 is None else C_10
    return 42*_C_6/r**8 + 72*_C_8/r**10 + 110*_C_10/r**12

def y_map(NPReal r, double R, double b, **params) -> NPReal:
    """
    y mapping function. Returns

               r^b - R^b
        y(r) = ---------
               r^b + R^b
    
    where `R` corresponds to either R_e or R_ref, and `b` corresponds to either
    p = 5 or q = 3, as defined in Stein et al. 2010.
    """
    return (r**b - R**b)/(r**b + R**b)

def dy_map(NPReal r, double R, double b, **params) -> NPReal:
    """
    First derivative of the y mapping function. Returns

        dy      2 R^b b r^(b-1)
        --(r) = --------------
        dr       (r^b + R^b)^2

    where `R` corresponds to either R_e or R_ref, and `b` corresponds to either
    p = 5 or q = 3, as defined in Stein et al. 2010.
    """
    return 2*R**b*b*r**(b-1)/(r**b + R**b)**2

def d2y_map(NPReal r, double R, double b, **params) -> NPReal:
    """
    Second derivative of the y mapping function. Returns

        d2y                       (b-1) R^b - (b+1) r^b
        ---(r) = 2 R^b b r^(b-2) (---------------------)
        dr2                           (r^b + R^b)^3

    where `R` corresponds to either R_e or R_ref and `b` corresponds to either
    p = 5 or q = 3, as defined in Stein et al. 2010.
    """
    return 2*R**b*b*r**(b-2)*((b-1)*R**b - (b+1)*r**b)/(r**b + R**b)**3

def beta(NPReal r, beta_i=None, D_e=None, R_e=None, R_ref=None, p=None, q=None,
        **params) -> NPReal:
    """
    beta exponential function. Returns


        beta(r) = beta_infty y^ref_p(r)

                 + (1 - y^ref_p(r)) sum[beta_i (y^ref_q(r))^i]
                                   i >= 0

    where y^ref_b(r) is the y mapping function (`y_map(r, R_ref, b)` in this
    module) for b = p and b = q, and beta_infty is ln(2*D_e/U_LR(R_e)).
    Parameters `beta_i`, `D_e`, `R_e`, `R_ref`, `p`, and `q` are optionally
    adjustable with default values as defined in Stein et al.
    All the parameters of U_LR are also optionally adjustable as keyword
    arguments.
    """
    cdef list _beta_i = PARAMS["beta_i"] if beta_i is None else beta_i
    cdef double _D_e = PARAMS["D_e"] if D_e is None else D_e
    cdef double _R_e = PARAMS["R_e"] if R_e is None else R_e
    cdef double _R_ref = PARAMS["R_ref"] if R_ref is None else R_ref
    cdef double _p = PARAMS["p"] if p is None else p
    cdef double _q = PARAMS["q"] if q is None else q

    cdef double beta_inf = np.log(2*_D_e/U_LR(_R_e, **params))
    cdef NPReal y_ref_p = y_map(r, _R_ref, _p)
    cdef NPReal y_ref_q = y_map(r, _R_ref, _q)
    return y_ref_p*beta_inf + (1 - y_ref_p)*np.polyval(_beta_i[::-1], y_ref_q)

def dbeta(NPReal r, beta_i=None, D_e=None, R_e=None, R_ref=None, p=None, q=None,
        **params) -> NPReal:
    """
    First derivative of the beta exponential function. Returns

        dbeta      dy^ref_p
        -----(r) = --------(r) (beta_infty - sum[beta_i (y^ref_q(r))^i])
          dr          dr                    i >= 0
                                     dy^ref_q
                  + (1 - y^ref_p(r)) --------(r) sum[i beta_i (y^ref_q(r))^(i-1)]
                                        dr      i >= 1

    where y^ref_b(r) is the y mapping function (`y_map(r, R_ref, b)` in this
    module) for b = p and b = q, and beta_infty is ln(2*D_e/U_LR(R_e)).
    Parameters `beta_i`, `D_e`, `R_e`, `R_ref`, `p`, and `q` are optionally
    adjustable with default values as defined in Stein et al. 2010.
    All the parameters of U_LR are also optionally adjustable as keyword
    arguments.
    """
    cdef list _beta_i = PARAMS["beta_i"] if beta_i is None else beta_i
    cdef double _D_e = PARAMS["D_e"] if D_e is None else D_e
    cdef double _R_e = PARAMS["R_e"] if R_e is None else R_e
    cdef double _R_ref = PARAMS["R_ref"] if R_ref is None else R_ref
    cdef double _p = PARAMS["p"] if p is None else p
    cdef double _q = PARAMS["q"] if q is None else q

    cdef double beta_inf = np.log(2*_D_e/U_LR(_R_e, **params))
    cdef NPReal y_ref_p = y_map(r, _R_ref, _p)
    cdef NPReal dy_ref_p = dy_map(r, _R_ref, _p)
    cdef NPReal y_ref_q = y_map(r, _R_ref, _q)
    cdef NPReal dy_ref_q = dy_map(r, _R_ref, _q)
    cdef np.ndarray[DTYPE_t, ndim=1] dbeta_i \
            = np.arange(1, len(_beta_i))*np.array(_beta_i[1:], dtype=DTYPE)
    return dy_ref_p*(beta_inf - np.polyval(_beta_i[::-1], y_ref_q)) \
            + (1 - y_ref_p)*dy_ref_q*np.polyval(dbeta_i[::-1], y_ref_q)

def d2beta(NPReal r, beta_i=None, D_e=None, R_e=None, R_ref=None, p=None,
        q=None, **params) -> NPReal:
    """
    Second derivative of the beta exponential function. Returns

        d2beta      d2y^ref_p
        ------(r) = ---------(r) (beta_infty - sum[beta_i (y^ref_q(r))^i])
          dr2          dr2                    i >= 0
                                       d2y^ref_q     dy^ref_p dy^ref_q
                   + ((1 - y^ref_p(r)) --------- - 2 -------- --------) sum[i beta_i (y^ref_q(r))^(i-1)]
                                          dr2           dr       dy    i >= 1
                                       dy^ref_q
                   + (1 - y^ref_p(r)) (--------)^2 sum[i (i-1) beta_i (y^ref_q(r))^(i-2)]
                                          dr      i >= 2

    where y^ref_b(r) is the y mapping function (`y_map(r, R_ref, b)` in this
    module) for b = p and b = 1, and beta_infty is ln(2*D_e/U_LR(R_e)).
    Parameters `beta_i`, `D_e`, `R_e`, `R_ref`, `p`, and `q` are optionally
    adjustable with default values as defined in Stein et al. 2010.
    All the parameters of U_LR are also optionally adjustable as keyword
    arguments.
    """
    cdef list _beta_i = PARAMS["beta_i"] if beta_i is None else beta_i
    cdef double _D_e = PARAMS["D_e"] if D_e is None else D_e
    cdef double _R_e = PARAMS["R_e"] if R_e is None else R_e
    cdef double _R_ref = PARAMS["R_ref"] if R_ref is None else R_ref
    cdef double _p = PARAMS["p"] if p is None else p
    cdef double _q = PARAMS["q"] if q is None else q

    cdef double beta_inf = np.log(2*_D_e/U_LR(_R_e, **params))
    cdef NPReal y_ref_p = y_map(r, _R_ref, _p)
    cdef NPReal dy_ref_p = dy_map(r, _R_ref, _p)
    cdef NPReal d2y_ref_p = d2y_map(r, _R_ref, _p)
    cdef NPReal y_ref_q = y_map(r, _R_ref, _q)
    cdef NPReal dy_ref_q = dy_map(r, _R_ref, _q)
    cdef NPReal d2y_ref_q = d2y_map(r, _R_ref, _q)
    cdef np.ndarray[DTYPE_t, ndim=1] di \
            = np.arange(1, len(_beta_i), dtype=DTYPE)
    cdef np.ndarray[DTYPE_t, ndim=1] d2i \
            = np.arange(2, len(_beta_i))*np.arange(1, len(_beta_i)-1, dtype=DTYPE)
    cdef np.ndarray[DTYPE_t, ndim=1] dbeta_i \
            = di*np.array(_beta_i[1:], dtype=DTYPE)
    cdef np.ndarray[DTYPE_t, ndim=1] d2beta_i \
            = d2i*np.array(_beta_i[2:], dtype=DTYPE)
    return d2y_ref_p*(beta_inf - np.polyval(_beta_i[::-1], y_ref_q)) \
            + ((1 - y_ref_p)*d2y_ref_q - 2*dy_ref_p*dy_ref_q)*np.polyval(dbeta_i[::-1], y_ref_q) \
            + (1 - y_ref_p)*dy_ref_q**2*np.polyval(d2beta_i[::-1], y_ref_q)

def V(NPReal r, **params) -> NPReal:
    """
    Full MLR formulation of the strontium potential. Returns float or numpy
    array corresponding to the type of `r` as

                         U_LR(r)
        V(r) = D_e (1 - --------- exp(-beta(r) y^e_p(r)))^2
                        U_LR(R_e)

    All parameters (including `D_e`, `R_e`, and `p` as used here) are optionally
    adjustable as keyword arguments with default values as defined in Stein et
    al. 2010.
    """
    cdef double _D_e = params.get("D_e", PARAMS["D_e"])
    cdef double _R_e = params.get("R_e", PARAMS["R_e"])
    cdef double _p = params.get("p", PARAMS["p"])

    cdef NPReal ULR_r = U_LR(r, **params)/U_LR(_R_e, **params)
    cdef NPReal beta_r = beta(r, **params)
    cdef NPReal yep_r = y_map(r, _R_e, _p)
    return _D_e*(1 - ULR_r*np.exp(-beta_r*yep_r))**2

def dV(NPReal r, **params) -> NPReal:
    """
    First derivative of the MLR formulation of the strontium potential. Returns
    float or numpy array corresponding to the type of `r` as

        dV        2 D_e         U_LR(r)
        --(r) = --------- (1 - --------- exp(-beta(r) y^e_p(r)))
        dr      U_LR(R_e)      U_LR(R_e)
                            dbeta                       dy^e_p       dU_LR
                * (U_LR(r) (-----(r) y^e_p(r) + beta(r) ------(r)) - -----(r))
                              dr                          dr           dr
                * exp(-beta(r) y^e_p(r))

    All parameters (including `D_e`, `R_e`, and `p` as used here) are optionally
    adjustable as keyword arguments with default values as defined in Stein et
    al. 2010.
    """
    cdef double _D_e = params.get("D_e", PARAMS["D_e"])
    cdef double _R_e = params.get("R_e", PARAMS["R_e"])
    cdef double _p = params.get("p", PARAMS["p"])

    cdef NPReal ULR_r = U_LR(r, **params)
    cdef NPReal dULR_r = dU_LR(r, **params)
    cdef double ULR_Re = U_LR(_R_e, **params)
    cdef NPReal beta_r = beta(r, **params)
    cdef NPReal dbeta_r = dbeta(r, **params)
    cdef NPReal yep_r = y_map(r, _R_e, _p)
    cdef NPReal dyep_r = dy_map(r, _R_e, _p)
    return 2*_D_e/ULR_Re \
            * (1 - ULR_r/ULR_Re*np.exp(-beta_r*yep_r)) \
            * (ULR_r*(dbeta_r*yep_r + beta_r*dyep_r) - dULR_r)*np.exp(-beta_r*yep_r)

def d2V(NPReal r, **params) -> NPReal:
    """
    Second derivative of the MLR formulation of the strontium potential. Returns
    float or numpy array corresponding to the type of `r` as

        d2V        2 D_e        1                dbeta              dy^e_p    dU_LR
        ---(r) = --------- (--------- ((U_LR(r) (----- y^e_p + beta ------) - -----) exp(-beta y^e_p))^2
        dr2      U_LR(R_e)  U_LR(R_e)              dr                 dr        dr
                                    U_LR(r)
                            + (1 - --------- exp(-beta y^e_p))
                                   U_LR(R_e)
                                            dU_LR   dbeta              dy^e_p 
                              * ((U_LR(r) + -----) (----- y^e_p + beta ------)
                                              dr      dr                 dr
                                             d2beta           dbeta dy^e_p        d2y^e_p
                                  + U_LR(r) (------ y^e_p + 2 ----- ------ + beta -------)
                                               dr2              dr    dr            dr2
                                    dU_LR   d2U_LR
                                  - ----- - ------) exp(-beta y^e_p))
                                      dr      dr2

    All parameters (including `D_e`, `R_e`, and `p` as used here) are optionally
    adjustable as keyword arguments with default values as defined in Stein et
    al. 2010.
    """
    cdef double _D_e = params.get("D_e", PARAMS["D_e"])
    cdef double _R_e = params.get("R_e", PARAMS["R_e"])
    cdef double _p = params.get("p", PARAMS["p"])

    cdef NPReal ULR_r = U_LR(r, **params)
    cdef NPReal dULR_r = dU_LR(r, **params)
    cdef NPReal d2ULR_r = d2U_LR(r, **params)
    cdef double ULR_Re = U_LR(_R_e, **params)
    cdef NPReal beta_r = beta(r, **params)
    cdef NPReal dbeta_r = dbeta(r, **params)
    cdef NPReal d2beta_r = d2beta(r, **params)
    cdef NPReal yep_r = y_map(r, _R_e, _p)
    cdef NPReal dyep_r = dy_map(r, _R_e, _p)
    cdef NPReal d2yep_r = d2y_map(r, _R_e, _p)
    return 2*_D_e/ULR_Re \
            * (1/ULR_Re*((ULR_r*(dbeta_r*yep_r + beta_r*dyep_r) - dULR_r)*np.exp(-beta_r*yep_r))**2 \
                + (1 - ULR_r/ULR_Re*np.exp(-beta_r*yep_r)) \
                    * ((ULR_r + dULR_r)*(dbeta_r*yep_r + beta_r*dyep_r) \
                        + ULR_r*(d2beta_r*yep_r + 2*dbeta_r*dyep_r + beta_r*d2yep_r) \
                        - dULR_r - d2ULR_r) \
                    * np.exp(-beta_r*yep_r))

TiemannMLR = MolecularPotential(
    V, dV, d2V,
    a0=1e-10, E0=100*phys.h*phys.c, units_tag="angstrom -> cm^-1",
    **PARAMS
)

