"""
Functions to compute a Lennard-Jones potential with added C_8 and C_10 terms.
All functions can handle as `r` inputs of type float, int, or numpy array.
Suggested parameters are calculated to closely replicate the potential from
Stein et al. 2010 defined in `libSr.potentials.tiemann`.
"""

import numpy as np
import libSr.phys as phys
from libSr.potentials.potentials import MolecularPotential
from libSr.misc import typecheck, NPReal, Parameter

# default potential parameters
PARAMS = {
    "sigma":    6.14988026003984451e9,
    "C_6":      3164.3077176399183,
    "C_8":      382271.68312989624,
    "C_10":     50540286.813837774
}
PARAMSu = {
    "sigma":    0,
    "C_6":      10,
    "C_8":      100,
    "C_10":     7e6
}

@typecheck({
    "r": NPReal,
    "sigma": Parameter,
    "C_6": Parameter,
    "C_8": Parameter,
    "C_10": Parameter
})
def V(r, sigma=None, C_6=None, C_8=None, C_10=None, **params) -> NPReal:
    """
    Modified Lennard-Jones potential. Returns float or numpy array corresponding
    to the type of `r` as

               sigma   C_6   C_8   C_10
        V(r) = ----- - --- - --- - ----
                r^12   r^6   r^8   r^10

    Constants `sigma`, `C_6`, `C_8`, `C_10` can be adjusted.
    """
    _sigma = PARAMS["sigma"] if sigma is None else sigma
    _C_6 = PARAMS["C_6"] if C_6 is None else C_6
    _C_8 = PARAMS["C_8"] if C_8 is None else C_8
    _C_10 = PARAMS["C_10"] if C_10 is None else C_10
    return _sigma/r**12 - _C_6/r**6 - _C_8/r**8 - _C_10/r**10

@typecheck({
    "r": NPReal,
    "sigma": Parameter,
    "C_6": Parameter,
    "C_8": Parameter,
    "C_10": Parameter
})
def dV(r, sigma=None, C_6=None, C_8=None, C_10=None, **params) -> NPReal:
    """
    First derivative of the modified Lennard-Jones potential. Returns float or
    numpy array corresponding to the type of `r` as

        dV          sigma     C_6     C_8      C_10
        --(r) = -12 ----- + 6 --- + 8 --- + 10 ----
        dr           r^13     r^7     r^9      r^11

    Constants `sigma`, `C_6`, `C_8`, `C_10` can be adjusted.
    """
    _sigma = PARAMS["sigma"] if sigma is None else sigma
    _C_6 = PARAMS["C_6"] if C_6 is None else C_6
    _C_8 = PARAMS["C_8"] if C_8 is None else C_8
    _C_10 = PARAMS["C_10"] if C_10 is None else C_10
    return -12*_sigma/r**13 + 6*_C_6/r**7 + 8*_C_8/r**9 + 10*_C_10/r**11

@typecheck({
    "r": NPReal,
    "sigma": Parameter,
    "C_6": Parameter,
    "C_8": Parameter,
    "C_10": Parameter
})
def d2V(r, sigma=None, C_6=None, C_8=None, C_10=None, **params) -> NPReal:
    """
    Second derivative of the modified Lennard-Jones potential. Returns float or
    numpy array corresponding to the type of `r` as

        dV          sigma      C_6       C_8       C_10
        --(r) = 156 ----- - 42 --- - 72 ---- - 110 ----
        dr           r^14      r^8      r^10       r^12

    Constants `sigma`, `C_6`, `C_8`, `C_10` can be adjusted.
    """
    _sigma = PARAMS["sigma"] if sigma is None else sigma
    _C_6 = PARAMS["C_6"] if C_6 is None else C_6
    _C_8 = PARAMS["C_8"] if C_8 is None else C_8
    _C_10 = PARAMS["C_10"] if C_10 is None else C_10
    return 156*_sigma/r**14 - 42*_C_6/r**8 - 72*_C_8/r**10 - 110*_C_10/r**12

ModifiedLJ = MolecularPotential(
    V, dV, d2V,
    a0=phys.a0, E0=phys.Eh, units_tag="Bohr -> Hartree",
    **PARAMS
)

