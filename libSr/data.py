import numpy as np
from libSr import reduced_mass, molecule_str
from libSr.misc import typecheck, Real, NPReal, Int

class DimerData:
    """
    Simple class to hold measurements and uncertainties in numpy arrays.
    Arithmetic operations (+, -, *, /, pow) with ints, floats, numpy arrays, and
    other Data objects, as well as `append`, and `insert` are supported.

    Attributes
    ----------
    molecule : str
        Composition of the molecule.
    mu : float
        Reduced mass of the dimer.
    m : numpy 1D array
        Measurements.
    u : numpy 1D array
        Uncertainties.
    v : numpy 1D array
        Vibrational quantum numbers.
    l : numpy 1D array
        Rotational quantum numbers.
    """
    @typecheck({
        "OO": (tuple, str),
        "m": np.ndarray,
        "u": np.ndarray,
        "v": np.ndarray,
        "l": np.ndarray
    })
    def __init__(self, OO, m, u, v, l):
        """
        Constructor.

        Parameters
        ----------
        OO : tuple or str
            Object(s) O1 (and O2) of the dimer as a tuple of args suppliable to
            `phys.reduced_mass`. See `help(libSr.phys.reduced_mass)`.
        m : numpy 1D array
            Measurements.
        u : numpy 1D array
            Uncertainties.
        v : numpy 1D array
            Vibrational quantum numbers.
        l : numpy 1D array
            Rotational quantum numbers.
        """
        assert m.shape == u.shape == v.shape == l.shape
        if isinstance(OO, str):
            self.mu = reduced_mass(OO)
            self.molecule = molecule_str(OO)
        elif isinstance(OO, tuple):
            self.mu = reduced_mass(*OO)
            self.molecule = molecule_str(*OO)
        self._OO = OO
        self.m = m
        self.u = np.abs(u)
        self.v = v
        self.l = l

    def trunc_u(self):
        self._trunc_u(self.u)

    def _trunc_u(self, x):
        if len(x.shape) == 1:
            for i in range(x.shape[0]):
                exp = np.floor(np.log10(x[i]))
                x[i] = pow(10, exp)*int(pow(10, -exp)*x[i])
        else:
            for i in range(x.shape[0]):
                self._trunc_u(x[i])

    def __getitem__(self, key, /):
        _m = np.array([self.m[key]]) if not isinstance(self.m[key], np.ndarray) else self.m[key]
        _u = np.array([self.u[key]]) if not isinstance(self.m[key], np.ndarray) else self.u[key]
        _v = np.array([self.v[key]]) if not isinstance(self.v[key], np.ndarray) else self.v[key]
        _l = np.array([self.l[key]]) if not isinstance(self.l[key], np.ndarray) else self.l[key]
        return DimerData(self._OO, _m, _u, _v, _l)

    def __setitem__(self, key, val, /):
        assert len(val) == 4
        self.m[key] = val[0]
        self.u[key] = val[1]
        self.v[key] = val[2]
        self.l[key] = val[3]

    def __add__(self, other, /):
        assert isinstance(other, (*Real, np.ndarray, DimerData))
        if isinstance(other, (*Real, np.ndarray)):
            return DimerData(self._OO, self.m + other, self.u, self.v, self.l)
        elif isinstance(other, DimerData):
            assert self.mu == other.mu
            return DimerData(self._OO, self.m + other.m,
                np.sqrt(self.u**2 + other.u**2), self.v, self.l)

    def __radd__(self, other, /):
        return self + other

    def __sub__(self, other, /):
        assert isinstance(other, (*Real, np.ndarray, DimerData))
        if isinstance(other, (*Real, np.ndarray)):
            return DimerData(self._OO, self.m - other, self.u, self.v, self.l)
        elif isinstance(other, DimerData):
            assert self.mu == other.mu
            return DimerData(self._OO, self.m - other.m,
                np.sqrt(self.u**2 + other.u**2), self.v, self.l)

    def __rsub__(self, other, /):
        return -(self - other)

    def __mul__(self, other, /):
        assert isinstance(other, (*Real, np.ndarray, DimerData))
        if isinstance(other, (*Real, np.ndarray)):
            return DimerData(self._OO, self.m*other, self.u*other, self.v, self.l)
        elif isinstance(other, DimerData):
            assert self.mu == other.mu
            return DimerData(self._OO, self.m*other.m,
                np.sqrt((self.u*other.m)**2 + (self.m*other.u)**2), self.v,
                self.l)

    def __rmul__(self, other, /):
        return self*other

    def __truediv__(self, other, /):
        assert isinstance(other, (*Real, np.ndarray, DimerData))
        if isinstance(other, (*Real, np.ndarray)):
            return DimerData(self._OO, self.m/other, self.u/other, self.v, self.l)
        elif isinstance(other, DimerData):
            assert self.mu == other.mu
            return DimerData(self._OO, self.m/other.m,
                np.sqrt((self.u/other.m)**2 + (self.m/other.m**2*other.u)**2),
                self.v, self.l)

    def __rtruediv__(self, other, /):
        assert isinstance(other, (*Real, np.ndarray, DimerData))
        if isinstance(other, (*Real, np.ndarray)):
            return DimerData(other/self.m, other.m/self.m**2*self.u, self.v, self.l)
        elif isinstance(other, DimerData):
            assert self.mu == other.mu
            return DimerData(self._OO, other.m/self.m,
                np.sqrt((other.m/self.m**2*self.u)**2 + (other.u/self.m)**2),
                self.v, self.l)

    def __neg__(self, /):
        return DimerData(-self.m, self.u)

    def __pow__(self, other, mod=None, /):
        assert isinstance(other, (*Real, np.ndarray, DimerData))
        if isinstance(other, (*Real, np.ndarray)):
            return DimerData(self._OO, pow(self.m, other, mod),
                other*pow(self.m, other-1, mod)*self.u, self.v, self.l)
        elif isinstance(other, DimerData):
            assert self.mu == other.mu
            return DimerData(self._OO, pow(self.m, other.m, mod),
                np.sqrt((other.m*pow(self.m, other.m-1, mod)*self.u)**2 \
                        + (np.log(self.m)*pow(self.m, other.m, mod)*other.u)**2),
                self.v, self.l)

    def __repr__(self, /):
        return "DimerData({" \
                + "\n  molecule: "+str(self.molecule) \
                + "\n  mu: "+str(self.mu) \
                + "\n  m: "+str(self.m) \
                + "\n  u: "+str(self.u) \
                + "\n  v: "+str(self.v) \
                + "\n  l: "+str(self.l) \
                + "\n})"

    def __str__(self, /):
        return self.__repr__()

    def __iter__(self, /):
        return iter(zip(self.m, self.u, self.v, self.l))

    def __len__(self, /):
        assert self.m.shape == self.u.shape == self.v.shape == self.l.shape
        return len(self.m)

    def append(self, values, axis=None, /):
        assert isinstance(values, (*Real, np.ndarray, DimerData))
        if isinstance(values, Real):
            self.m = np.append(self.m, values, axis)
            self.u = np.append(self.u, 0, axis)
            self.v = np.append(self.v, 0, axis)
            self.l = np.append(self.l, 0, axis)
        elif isinstance(values, np.ndarray):
            self.m = np.append(self.m, values, axis)
            self.u = np.append(self.u, np.zeros(values.shape), axis)
            self.v = np.append(self.v, np.zeros(values.shape), axis)
            self.l = np.append(self.l, np.zeros(values.shape), axis)
        elif isinstance(values, DimerData):
            assert self.mu == values.mu
            self.m = np.append(self.m, values.m, axis)
            self.u = np.append(self.u, values.u, axis)
            self.v = np.append(self.v, values.v, axis)
            self.l = np.append(self.l, values.l, axis)

    def insert(self, idx, values, axis=None, /):
        assert isinstance(values, (*Real, np.ndarray, DimerData))
        if isinstance(values, Real):
            self.m = np.insert(self.m, idx, values, axis)
            self.u = np.insert(self.u, idx, 0, axis)
            self.v = np.insert(self.v, idx, 0, axis)
            self.l = np.insert(self.l, idx, 0, axis)
        elif isinstance(values, np.ndarray):
            self.m = np.insert(self.m, idx, values, axis)
            self.u = np.insert(self.u, idx, np.zeros(values.shape), axis)
            self.v = np.insert(self.v, idx, np.zeros(values.shape), axis)
            self.l = np.insert(self.l, idx, np.zeros(values.shape), axis)
        elif isinstance(values, DimerData):
            assert self.mu == values.mu
            self.m = np.insert(self.m, idx, values.m, axis)
            self.u = np.insert(self.u, idx, values.u, axis)
            self.v = np.insert(self.v, idx, values.v, axis)
            self.l = np.insert(self.l, idx, values.l, axis)

# strontium
# isotope   nu          l   Energy [Hz]
# -------   --          -   -----------
# 84        -2 (60)     0   -644.7372(2)e6
#           -1 (61)     0   -13.7162(2)e6
#           -2 (60)     2   -519.6177(5)e6
# 86        -1 (62)     0   -83.00(7)(20)e3
# 88        -1 (62)     0   -136.7(2)e6
#           -1 (62)     2   -66.6(2)e6

# ytterbium
# isotope   nu          l   Energy [Hz]
# -------   --          -   -----------
# 170       -1 (70)     0   -27.661(23)e6
#           -1 (70)     2   -3.651(26)e6
# 171       -1 (70)     0   -64.418(40)e6
#           -1 (70)     2   -31.302(50)e6
# 172       -1 (70)     0   -123.269(26)e6
#           -1 (70)     2   -81.786(19)e6
# 173       -1 (71)     0   -1.539(74)e6
# 174       -2 (70)     0   -325.607(18)e6
#           -1 (71)     0   -10.612(38)e6
#           -1 (71)     0   -10.606(17)e6
#           -2 (70)     2   -268.575(21)e6
# 176       -1 (71)     0   -70.404(11)e6
#           -1 (71)     2   -37.142(13)e6

sr84 = DimerData("84Sr",
    np.array([-644.7372e6, -13.7162e6, -519.6177e6]),
    np.array([0.0002e6, 0.0002e6, 0.0005e6]),
    np.array([60, 61, 60]),
    np.array([0, 0, 2])
)
sr86 = DimerData("86Sr",
    np.array([-83.0e3]),
    np.array([0.2e3]),
    np.array([62]),
    np.array([0])
)
sr88 = DimerData("88Sr",
    np.array([-136.7e6, -66.6e6]),
    np.array([0.2e6, 0.2e6]),
    np.array([62, 62]),
    np.array([0, 2])
)
yb170 = DimerData("170Yb",
    np.array([-27.661e6, -3.651e6]),
    np.array([0.023e6, 0.026e6]),
    np.array([70, 70]),
    np.array([0, 2])
)
yb171 = DimerData("171Yb",
    np.array([-64.418e6, -31.302e6]),
    np.array([0.040e6, 0.050e6]),
    np.array([70, 70]),
    np.array([0, 2])
)
yb172 = DimerData("172Yb",
    np.array([-123.269e6, -81.786e6]),
    np.array([0.026e6, 0.019e6]),
    np.array([70, 70]),
    np.array([0, 2])
)
yb173 = DimerData("173Yb",
    np.array([-1.539e6]),
    np.array([0.074e6]),
    np.array([71]),
    np.array([0])
)
yb174 = DimerData("174Yb",
    np.array([-325.607e6, -10.609e6, -268.575e6]),
    np.array([0.018e6, 0.021e6, 0.021e6]),
    np.array([70, 71, 70]),
    np.array([0, 0, 2])
)
yb176 = DimerData("176Yb",
    np.array([-70.404e6, -37.142e6]),
    np.array([0.011e6, 0.013e6]),
    np.array([71, 71]),
    np.array([0, 2])
)
