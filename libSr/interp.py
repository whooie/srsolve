import numpy as np
from libSr.misc import typecheck, Real, NPReal, List_like, NPList_like

@typecheck({
    "data_x": NPList_like,
    "data_y": NPList_like,
    "x": NPReal
})
def lagrange(data_x, data_y, x) -> NPReal:
    """
    Interpolate a function of one variable using a Lagrange polynomial.

    Parameters
    ----------
    data_x, data_y : list-like or numpy 1D array
        Arrays of x and y values of the function.
    x : numpy array or float or int
        x-value(s) at which to evaluate the Lagrange polynomial.

    Returns
    -------
    y : numpy ndarray or float or int
        y-value(s) of the Lagrange polynomial. Returns as float or numpy array,
        depending on the type of `x`.
    """
    assert data_x.shape == data_y.shape

    k = len(data_x)
    y = np.sum([
        data_y[j] * np.prod([
            (x - data_x[m])/(data_x[j] - data_x[m]) for m in range(k) if m != j
        ], axis=0) for j in range(k)
    ], axis=0)

    return y

@typecheck({
    "data_x": NPList_like,
    "data_y": NPList_like,
    "x": NPReal
})
def dlagrange(data_x, data_y, x) -> NPReal:
    """
    Interpolate the first derivative of a function of one variable using the
    first derivative of a Lagrange polynomial.

    Parameters
    ----------
    data_x, data_y : list-like or numpy 1D array
        Arrays of x and y values of the function.
    x : numpy ndarray or float or int
        x-value(s) at which to evaluate the Lagrange polynomial first
        derivative.

    Returns
    -------
    y : numpy ndarray or float or int
        y-value(s) of the Lagrange polynomial's first derivative. Returns as
        float or numpy array, depending on the type of `x`.
    """
    assert data_x.shape == data_y.shape

    k = len(data_x)
    y = np.sum([
        data_y[j] * np.sum([
            1/(data_x[j] - data_x[i]) * np.prod([
                (x - data_x[m])/(data_x[j] - data_x[m]) for m in range(k) if m != j and m != i
            ], axis=0) for i in range(k) if i != j
        ], axis=0) for j in range(k)
    ], axis=0)

    return y

@typecheck({
    "x": NPList_like,
    "y": NPList_like,
    "ndarray": "bool",
    "kind": "str",
})
def find_zeros(x, y, kind="both", as_ndarray=False) -> list:
    """
    Find the positions `x` where the function `y` cross zero and interpolate to
    find true positions.

    Parameters
    ----------
    x, y : numpy 1D array or list-like
        Sampled points `y` as a function of `x`.
    kind : str in {'rising', 'falling', 'both'}
        Look for y=0 crossings from below, from above, or both. Default to both.
    as_ndarray : bool
        Controls whether the zeros are returned as a list or numpy ndarray.

    Returns
    -------
    x0 : numpy 1D array or list
        Zeros of `y` as a function of `x`. Type of `x0` is determined by
        `ndarray`.
    """
    assert x.shape == y.shape
    assert x.shape[0] > 2
    assert kind in {"rising", "falling", "both"}

    if kind == "rising":
        rising, falling = True, False
    elif kind == "falling":
        rising, falling = False, True
    elif kind == "both":
        rising, falling = True, True

    x0 = list()

    for i in range(1, len(y)):
        if y[i] == 0:
            x0.append(x[i])
            continue
        elif y[i]*y[i-1] <= 0 \
                and ((rising and y[i]-y[i-1]>0) or (falling and y[i]-y[i-1]<0)):
            y_interp = y[i-4:i+4]
            x_interp = x[i-4:i+4]
            if len(x_interp) < 8:
                print(f"{__name__}.find_zeros: WARNING: attempting to interpolate near an edge of the given data; some accuracy may be lost")
            zero = lagrange(y_interp, x_interp, 0)
            x0.append(zero)

    return np.array(x0) if as_ndarray else x0

