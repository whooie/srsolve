#from distutils.core import setup
from setuptools import setup
from setuptools.extension import Extension
from Cython.Build import cythonize
import numpy as np

source_files = [
    "libSr/interp.pyx",
    "libSr/solve.pyx",
    "libSr/optim.pyx",
    "libSr/potentials/potentials.pyx",
    "libSr/potentials/tiemann.pyx",
    "libSr/potentials/tiemannMLR.pyx",
    "libSr/potentials/modifiedLJ.pyx",
    "libSr/potentials/modifiedLJ_Yb.pyx",
]
extensions = [
    Extension(
        name=X.replace("/", ".").replace(".pyx", ""),
        sources=[X],
        include_dirs=[np.get_include()]
    ) for X in source_files
]
annotate = False

setup(
    name="libSr",
    ext_modules=cythonize(
        module_list=extensions,
        annotate=annotate,
        language_level=3
    ),
    include_dirs=[np.get_include()]
)
