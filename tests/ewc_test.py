import sys
import os.path as path
home_path = path.join(path.dirname(path.abspath(__file__)), "..")
sys.path.insert(0, home_path)
from timeit import default_timer as timer
import numpy as np
from math import factorial
import matplotlib.pyplot as pp
import libSr.plotdefs as pd
import libSr.phys as phys
import libSr.solve as solve
import libSr.interp as interp

# energy in "home" units
def E(n):
    return phys.hbar*omega*(0.5 + np.round(n))

# energy in natural units
def E_scaled(n):
    return 2*m*alpha**2/phys.hbar**2 * E(n)

# potential in "home" units
def V(x):
    #res = 0
    #res += (x <= -35e-9)*(0.5*m*omega**2*(-35e-9)**2)
    #res += (x > -35e-9)*(x < 35e-9)*(0.5*m*omega**2*x**2)
    #res += (x >= 35e-9)*(0.5*m*omega**2*(35e-9)**2)
    #return res
    return 0.5*m*omega**2*x**2

# potential in natural units
def V_scaled(x):
    return 2*m*alpha**2/phys.hbar**2 * V(alpha/alpha0*x)

# find the turning point in natural units
def turning_point(n):
    return np.sqrt(2*E(n)/(m*omega**2*alpha**2))

# define the hermite polynomial
def hermite(x, n):
    res = 0
    for m in range(n//2 + 1):
        res += (-1)**m/(factorial(m)*factorial(n - 2*m)) * (2*x)**(n - 2*m)
    return factorial(n)*res

# wavefunction of the nth energy level
def qho_wf(x, n, a=1):
    return 1/np.sqrt(2**n * factorial(n)) * ((m*a**2*omega)/(np.pi*phys.hbar))**(1/4) \
            * hermite(np.sqrt(m*omega/phys.hbar)*x, n)*np.exp(-(m*omega)/(2*phys.hbar)*x**2)

# wavefunction of the nth energy level defined over the natural length scale
def qho_wf_scaled(x, n):
    return qho_wf(alpha/alpha0*x, n, a=alpha)

alpha0 = 1      # define the "home" length scale [m]
alpha = 1e-9    # "new" length scale [m]
m = phys.me     # electron mass [kg]
omega = 1e12    # oscillator frequency [Hz]

L = 200         # width of the solving area, centered about zero [alpha]
xgridsize = 1000
Egridsize = 1000
method = ["shoot", "matrix-numerov"][0] # set which method to test here
N = 15 # perform analysis for the first N energies
ewc = False # perform EWC iteration on all energies found. don't use this, it's bad

def qho(L, xgridsize, Egridsize, method, N, ewc, plot):
    xgrid = np.linspace(-L/2, L/2, xgridsize)
    # for the shooting method test. We know the actual energies here, so choose
    # to test a best case for the method, over a minimal number of energies
    Egrid = np.linspace(E_scaled(0)/2, E_scaled(N)-E_scaled(0)/2, Egridsize)
    v_scaled = V_scaled(xgrid)

    k_data = list()

    t0 = timer()
    if method == "shoot":
        xgridnum = len(xgrid)
        Egridnum = len(Egrid)
        # had to change the log derivative point from zero because the symmetry of
        # the wavefunctions caused dlog_diff not to go to zero for odd n
        x0 = 5
        
        dlog_diff = solve.shoot_two_sided(xgrid, x0, Egrid, v_scaled, l=0)
        E0 = interp.find_zeros(Egrid, dlog_diff, kind="falling")
        U = [solve.shoot_two_sided_single(xgrid, x0, E0[i], v_scaled, l=0) for i in range(len(E0))]
        exectime = timer() - t0

        if plot:
            plotlim = np.sqrt(np.mean(dlog_diff**2))
            fig = pp.figure("shooting parameter")
            ax = fig.gca()
            ax.plot(Egrid, dlog_diff)
            ax.scatter(E0, np.zeros(len(E0)), 50, 'k')
            ax.set_xlabel(r"Energy $\left[\frac{\hbar^2}{2m\alpha^2}\right]$")
            ax.set_ylabel(f"Difference in log derivative")
            ax.set_title(f"Grid size = x: {xgridnum}, E: {Egridnum}")
            pd.grid(True, ax)
            ax.set_ylim(-plotlim, plotlim)
            pp.savefig(path.join(home_path, f"tests/output/qho_shooting-parameter_N={N}_grid={L//2}-{xgridnum}-{Egridnum}.png"))
    elif method == "matrix-numerov":
        xgridnum = len(xgrid)
        Egridnum = None
        E0, U = solve.matrix_numerov(xgrid, v_scaled, l=0)
        exectime = timer() - t0

        if plot:
            fig = pp.figure("all eigenvalues")
            ax = fig.gca()
            ax.plot(E_scaled(np.arange(len(E0))), "k--", label="Theory")
            ax.scatter(range(len(E0)), E0, 25, label="Computed")
            ax.set_xlabel("$n$")
            ax.set_ylabel(r"Energy $\left[\frac{\hbar^2}{2m\alpha^2}\right]$")
            ax.legend()
            pd.grid(True, ax)
            pp.savefig(path.join(home_path, f"tests/output/qho_all-eigvals_method={method}_N={N}_grid={L//2}-{xgridnum}.png"))
    else:
        print("invalid method")
        sys.exit(0)

    if ewc:
        t0 = timer()
        for i in range(N):
            U[i], E0[i], k = solve.ewc(xgrid, U[i], E0[i], v_scaled, l=0, EPSILON=1e-6, maxiters=20)
            k_data.append(k)
        exectime += timer() - t0


    # plotting/comparison quantities
    n = np.linspace(0, len(E0)-1, len(E0))
    E0_actual = E_scaled(n)
    turn = turning_point(n)
    diff = np.sqrt(np.mean((np.array(E0)[:N] - E0_actual[:N])**2))
    err = np.abs(np.array(E0) - E0_actual)
    nplot = np.linspace(-0.5, (N-1)+0.5, 50*N)
    E0plot = E_scaled(nplot)
    x = xgrid
    v = V_scaled(x)
    U_actual = [qho_wf_scaled(xgrid, i) for i in range(N)]

    if plot:
        # plot wavefunctions in the potential
        plotlim_center = (max(E0[:N]) + min(E0[:N]))/2
        plotlim_half = (max(E0[:N]) - plotlim_center)*1.1
        wf_scale = np.sqrt((max(E0[:N]) - min(E0[:N]))/N)
        fig = pp.figure("calculated wavefunctions")
        ax = fig.gca()
        ax.plot(x, v, "k")
        for i in range(N):
            ax.plot(x, E0_actual[i]+U_actual[i]**2*wf_scale, "k--")
            ax.plot(x, E0[i]+U[i]**2*wf_scale)
        ax.set_xlabel("x")
        ax.set_ylabel(r"Energy $\left[\frac{\hbar^2}{2m\alpha^2}\right]$, $\left|\psi_n\right|^2$")
        pd.grid(True, ax)
        ax.set_ylim(plotlim_center-plotlim_half, plotlim_center+plotlim_half)
        pp.savefig(path.join(home_path, f"tests/output/qho_wavefunctions_method={method}_N={N}_grid={L//2}-{xgridnum}-{Egridnum}.png"))

        # compare the calculated and actual energies side-by-side
        fig = pp.figure("calculated versus actual energies")
        ax = fig.gca()
        ax.plot(nplot, E0plot, "k--", label="Theory")
        ax.scatter(range(N), E0[:N], 50, "C0", label="Calculated")
        ax.set_xlabel("n")
        ax.set_ylabel(r"Energy $\left[\frac{\hbar^2}{2m\alpha^2}\right]$")
        ax.set_title(f"Comparison of calculated and actual energies")
        ax.text(0, 0.97*max(E0[:N]), f"RMS of differences = {diff:.3e}\nSolving boundary @ x = $\\pm${L//2}\nGrid size = x: {xgridnum}, E: {Egridnum}",
            fontsize=22, verticalalignment="top", bbox=dict(facecolor="white"))
        ax.legend(loc="lower right")
        pd.grid(True, ax)
        pp.savefig(path.join(home_path, f"tests/output/qho_energy-comparison_method={method}_N={N}_grid={L//2}-{xgridnum}-{Egridnum}.png"))

        # just the absolute difference
        fig = pp.figure("energy error")
        ax = fig.gca()
        ax.scatter(n[:N], err[:N], 50, "C0")
        ax.set_xlabel("n")
        ax.set_ylabel(r"Energy $\left[\frac{\hbar^2}{2m\alpha^2}\right]$")
        ax.set_title(f"Absolute calculated energy error")
        #ax.text(0, 0.97*max(err[:N]), f"RMS = {diff:.3e}\nSolving boundary @ x = $\\pm${L//2}\nGrid size = x: {xgridnum}, E: {Egridnum}",
        #    fontsize=22, verticalalignment="top", bbox=dict(facecolor="white"))
        pd.grid(True, ax)
        minerr = min(err[:N])
        maxerr = max(err[:N])
        miderr = (maxerr - minerr)/2
        ax.set_ylim(miderr - 1.1*(miderr-minerr), miderr + 1.1*(maxerr - miderr))
        pp.savefig(path.join(home_path, f"tests/output/qho_energy-error_method={method}_N={N}_grid={L//2}-{xgridnum}-{Egridnum}.png"))

        # plot the turning point compared to the solving boundary to make sure any
        # losses in accuracy conform to expectations
        fig = pp.figure("classical turning points")
        ax = fig.gca()
        ax.scatter(n[:N], turn[:N], 50, "C0", label="Turning point")
        ax.plot(n[:N], np.max(xgrid)*np.ones(len(n))[:N], "k--", label="Solving boundary")
        ax.set_xlabel("n")
        ax.set_ylabel(f"Classical turning point $[\\alpha]$")
        ax.set_title("Classical turning point at energy $E_n$")
        ax.legend()
        pd.grid(True, ax)
        pp.savefig(path.join(home_path, f"tests/output/qho_turning-points_N={N}_L={L//2}.png"))

    return (n[:N], err[:N], k_data, exectime, diff)

def ewc_test():
    n1, err1, k1, t1, diff1 = qho(L, xgridsize, Egridsize, "shoot", 15, False, False)
    n2, err2, k2, t2, diff2 = qho(L, xgridsize, Egridsize, "shoot", 15, True, False)

    print(t1, t2)

    fig, [ax_error, ax_iter] = pp.subplots(nrows=2, sharex=True, num="ewc comp", gridspec_kw={"height_ratios": [0.7, 0.3], "hspace": 0.025})
    ax_error.scatter(n1, err1, 50, "C0", label="Without EWC")
    ax_error.scatter(n2, err2, 50, "C1", label="With EWC")
    ax_iter.scatter(n2, k2, 50, "C1", label="With EWC")
    ax_iter.set_ylim(0, 21)
    ax_error.legend(loc=(0.025, 0.6))

    data_min = min(np.min(err1), np.min(err2))
    data_max = max(np.max(err1), np.max(err2))
    ymid = (data_max + data_min)/2
    ymin = ymid + 1.1*(data_min - ymid)
    ymax = ymid + 1.1*(data_max - ymid)
    ax_error.set_ylim(ymin, ymax)

    #ax_error_ins = zoomed_inset_axes(ax_error, 0.5, loc=1)
    ax_error_ins = ax_error.inset_axes([0.35, 0.30, 0.62, 0.62])
    ax_error_ins.scatter(n1[2:], err1[2:], 50, "C0")
    ax_error_ins.scatter(n2[2:], err2[2:], 50, "C1")

    data_min = min(np.min(err1[2:]), np.min(err2[2:]))
    data_max = max(np.max(err1[2:]), np.max(err2[2:]))
    ymid = (data_max + data_min)/2
    ymin = ymid + 1.1*(data_min - ymid)
    ymax = ymid + 1.1*(data_max - ymid)
    ax_error_ins.set_ylim(ymin, ymax)
    ax_error_ins.set_xticks([2,4,6,8,10,12,14])

    #mark_inset(ax_error, ax_error_ins, loc1=2, loc2=1, fc="none", ec="0.5")
    ax_error.indicate_inset_zoom(ax_error_ins)
    pd.grid(True, ax_error_ins)

    ax_error.set_title("Effectiveness of the EWC Method")
    ax_error.set_ylabel(r"Energy error $\left[E_\alpha\right]$")
    ax_iter.set_ylabel("Number of\niterations")
    ax_iter.set_xlabel("n")
    pd.grid(True, ax_error)
    pd.grid(True, ax_iter)
    fig.savefig(path.join(home_path, f"tests/output/qho_ewc-comp_N={N}_grid={L//2}-{xgridsize}-{Egridsize}.png"))


if __name__ == "__main__":
    ewc_test()
