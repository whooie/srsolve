import sys
import os.path as path
home_path = path.join(path.dirname(path.abspath(__file__)), "..")
sys.path.insert(0, home_path)
from timeit import default_timer as timer

import numpy as np
from math import factorial
import matplotlib.pyplot as pp

import libSr.solve as solve
import libSr.interp as interp
import libSr.phys as phys
import libSr.plotdefs as pd

# parameters of the potential/particle
w = 2*np.pi*1e12    # oscillator frequency [Hz]
m = phys.me         # electron mass [kg]

# parameters of the solve
a0 = 1          # "home" length scale [m]
a = 1e-9        # "natural" length scale [m]
L = 150         # width of the solving area, centered at zero [a]
Nx = 1000       # spatial grid size
NE = 5000       # energy grid size (for use in the shooting method)
N = 15          # perform analysis for the first N energies
method = ["matrix-numerov", "shooting", "johnson"][1] # solving method

def En(n):
    """
    Energy levels in SI
    """
    return phys.hbar*w*(0.5 + np.round(n))

def En_nat(n):
    """
    Energy levels in natural units
    """
    return 2*m*a**2/phys.hbar**2 * En(n)

def pot(x):
    """
    Potential in SI
    """
    return 0.5*m*w**2*x**2

def pot_nat(x):
    """
    Potential in natural units
    """
    return 2*m*a**2/phys.hbar**2 * pot(a/a0*x)

def turning_point(n):
    """
    Classical turning point of the nth energy level in SI
    """
    return np.sqrt(2*En(n)/(m*w**2))

def turning_point_nat(n):
    """
    Classical turning point of the nth energy level in natural units
    """
    return turning_point(n)*a0/a

def hermite(x, n):
    """
    Hermite polynomial for the wavefunction
    """
    res = 0
    for m in range(n//2 + 1):
        res += (-1)**m/(factorial(m)*factorial(n-2*m)) * (2*x)**(n-2*m)
    return factorial(n)*res

def wfn(x, n):
    """
    Wavefunction of the nth energy level in SI
    """
    return 1/np.sqrt(2**n * factorial(n)) \
            * ((m*w)/(np.pi*phys.hbar))**(1/4) \
            * hermite(np.sqrt(m*w/phys.hbar)*x, n) \
            * np.exp(-(m*w)/(2*phys.hbar)*x**2)

def wfn_nat(x, n):
    """
    Wavefunction of the nth energy level in natural units
    """
    return wfn(a/a0*x, n)*np.sqrt(a)

xgrid = np.linspace(-L/2, L/2, Nx)
Egrid = np.linspace(En_nat(0)/2, En_nat(N)-En_nat(0)/4, NE) # for method='shooting'
E0 = [En_nat(n) + (1/8)*En_nat(0) for n in range(N)] # for method='johnson'

def main(L, Nx, NE, method, N, plot):
    xgrid = np.linspace(-L/2, L/2, Nx)
    Egrid = np.linspace(En_nat(0)/2, En_nat(N)-En_nat(0)/4, NE)
    E0 = [En_nat(n) + (1/8)*En_nat(0) for n in range(N)]
    V = pot_nat(xgrid)
    X = solve.Solver(xgrid, V)

    t0 = timer()
    exectime = 0
    if method == "matrix-numerov":
        nx = Nx
        ne = None
        EU = X.solve(mathod=method)
        exectime += timer() - t0

        if plot:
            fig, ax = pp.subplots(num="all eigenvalues")
            ax.plot(En_nat(np.arange(len(EU))), "k--", label="Theory")
            ax.scatter(range(Nx), [eu[0] for eu in EU], 25, label="Computed")
            ax.set_xlabel(r"$n$")
            ax.set_ylabel(r"Energy $\left[\hbar^2/2ma^2\right]$")
            ax.legend()
            pd.grid(True, ax)
            pp.savefig(path.join(home_path, f"tests/output/qho_all-eigvals_method={method}_N={N}_grid={L}-{nx}.png"))

    elif method == "shooting":
        nx = Nx
        ne = NE
        #EU = X.solve(method=method, E=Egrid)

        # do the X.solve method by hand so that we can show off the dlog plot
        #solve.shoot_two_sided_single(xgrid, V, Egrid[0])
        dlog_diff = solve.shoot_two_sided(xgrid, V, Egrid)
        E = interp.find_zeros(Egrid, dlog_diff, kind="rising")
        EU = [(e, solve.renorm_numerov(xgrid, V, e)) for e in E]
        exectime += timer() - t0

        if plot:
            plotlim = np.sqrt(np.mean(dlog_diff**2))
            fig, ax = pp.subplots(num="shooting parameter")
            dlog_diff_parts = list()
            i0 = 0
            for i in range(1, len(dlog_diff)):
                if dlog_diff[i]*dlog_diff[i-1] < 0 and dlog_diff[i] < 0:
                    dlog_diff_parts.append((Egrid[i0:i], dlog_diff[i0:i]))
                    i0 = i
            for dlog_diff_part in dlog_diff_parts:
                ax.plot(*dlog_diff_part, "C0")
            #ax.plot(Egrid, dlog_diff)
            ax.scatter(E, np.zeros(len(E)), 50, 'k')
            ax.set_xlabel(r"Energy $\left[\hbar^2/2ma^2\right]$")
            ax.set_ylabel(r"$(y_r - y_l)$ log-derivative difference")
            ax.set_title(f"Grid size = x: {Nx}, E: {NE}")
            ax.set_xlabel(r"Energy [arb. units]")
            ax.set_ylabel(r"$D(E)$ [arb. units]")
            pd.grid(True, ax)
            ax.set_ylim(-plotlim, plotlim)
            pp.savefig(path.join(home_path, f"tests/output/qho_shooting-parameter_N={N}_grid={L}-{nx}-{ne}.png"))

    elif method == "johnson":
        nx = Nx
        ne = NE
        EU = list()
        for i in range(N):
            EU += X.solve(
                method=method,
                E0=En_nat(i),
                bounds=(En_nat(i)-En_nat(0)*(3/8), En_nat(i)+En_nat(0)*(3/8)),
                nu=i,
                maxiters=800,
                EPSILON=1e-6
            )
            #print(L, nx, ne, i, EU[-1][0], En_nat(i))
        exectime += timer() - t0

    else:
        print("invalid method")
        sys.exit(1)

    # plotting/comparison quantities
    n = np.arange(N)
    EU_actual = [(En_nat(v), wfn_nat(xgrid, v)) for v in n]
    turn = turning_point_nat(n)
    err = np.array([eu[0] for eu in EU[:N]]) - np.array([eu[0] for eu in EU_actual[:N]])
    rms = np.sqrt(np.mean(err**2))
    nplot = np.linspace(-0.5, (N-1)+0.5, 50*N)
    Eplot = En_nat(nplot)

    if plot:
        e_max = max([eu[0] for eu in EU[:N]])
        e_min = min([eu[0] for eu in EU[:N]])
        
        # plot wavefunctions in the potential
        plotlim_center = (e_max + e_min)/2
        plotlim_half = (e_max - plotlim_center)*1.1
        wf_scale = np.sqrt((e_max - e_min)/N)
        fig, ax = pp.subplots(num="calculated wavefunctions")
        ax.plot(xgrid, V, "k")
        for i in range(N):
            ax.plot(xgrid, EU_actual[i][0]+wf_scale*EU_actual[i][1]**2, "k--")
            ax.plot(xgrid, EU[i][0]+wf_scale*EU[i][1]**2)
        ax.set_xlabel(r"x [$a$]")
        ax.set_ylabel(r"Energy $\left[\hbar^2/2ma^2\right]$; $\left|\psi_n\right|^2$")
        pd.grid(True, ax)
        ax.set_ylim(plotlim_center-plotlim_half, plotlim_center+plotlim_half)
        pp.savefig(path.join(home_path, f"tests/output/qho_wavefunctions_method={method}_N={N}_grid={L}-{nx}-{ne}.png"))

        # compare the calculated and actual energies side-by-side
        fig, ax = pp.subplots(num="calculated versus actual energies")
        ax.plot(nplot, Eplot, "k--", label="Theory")
        ax.scatter(n, [eu[0] for eu in EU[:N]], 50, "C0", label="Calculated")
        ax.set_xlabel(r"$n$")
        ax.set_ylabel(r"Energy $\left[\hbar^2/2ma^2\right]$")
        ax.set_title(r"Comparison of calculated and actual energies")
        ax.text(0, 0.97*e_max, f"RMS of differences = {rms:.3e}\nSolving boundary @ x = $\\pm${L/2:.3e}\nGrid size = x: {nx}, E: {ne}", fontsize=22, verticalalignment="top", bbox=dict(facecolor="white"))
        ax.legend(loc="lower right")
        pd.grid(True, ax)
        pp.savefig(path.join(home_path, f"tests/output/qho_energy-comparison_method={method}_N={N}_grid={L}-{nx}-{ne}.png"))

        # just the difference
        fig, ax = pp.subplots(num="energy error")
        ax.scatter(n, err, 50, "C0")
        ax.set_xlabel(r"$n$")
        ax.set_ylabel(r"Energy $\left[\hbar^2/2ma^2\right]$")
        ax.set_title(r"Calculated energy error (calculated$-$theory)")
        #ax.text(0, 0.97*max(err[:N]), f"RMS = {diff:.3e}\nSolving boundary @ x = $\\pm${L//2}\nGrid size = x: {xgridnum}, E: {Egridnum}",
        #    fontsize=22, verticalalignment="top", bbox=dict(facecolor="white"))
        pd.grid(True, ax)
        minerr = min(err)
        maxerr = max(err)
        miderr = (maxerr + minerr)/2
        ax.set_ylim(miderr - 1.1*(miderr-minerr), miderr + 1.1*(maxerr-miderr))
        pp.savefig(path.join(home_path, f"tests/output/qho_energy-error_method={method}_N={N}_grid={L}-{nx}-{ne}.png"))

        # plot the turning point compared to the solving boundary to make sure
        # any losses in accuracy conform to expectations
        fig, ax = pp.subplots(num="classical turning points")
        ax.scatter(n, turn, 50, "C0", label="Turning point")
        ax.plot(n, np.max(xgrid)*np.ones(N), "k--", label="Solving boundary")
        ax.set_xlabel(r"$n$")
        ax.set_ylabel(f"Classical turning point [{a} m]")
        ax.set_title("Classical turning point at energy $E_n$")
        ax.legend()
        pd.grid(True, ax)
        pp.savefig(path.join(home_path, f"tests/output/qho_turning-points_N={N}_L={L}.png"))

    return (exectime, rms)

if __name__ == "__main__":
    print(main(L, Nx, NE, method, N, True))
