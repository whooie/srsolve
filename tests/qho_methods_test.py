import sys
import os.path as path
home_path = path.join(path.dirname(path.abspath(__file__)), "..")
sys.path.insert(0, home_path)
import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as pp
import libSr.plotdefs as pd
import harmonic_oscillator as qho

N = 15
L = [50, 100, 150, 200, 250, 300, 350]
x = [500, 1000, 1500, 2000, 2500, 3000, 3500]
#E = [500, 1000, 1500, 2000, 2500, 3000, 3500]
E = [250, 500, 750, 1000, 1250, 1500, 1750]
ntrials = 5

def mkfit(X, Y, d=1, n=500):
    X_ = np.array(X)
    Y_ = np.array(Y)
    A = np.array([X_**(i) for i in range(d+1)]).T
    coeffs = la.solve(A.T@A, A.T@Y_)
    xplot = np.linspace(X_.min(), X_.max(), n)
    yplot = 0
    for i in range(d+1):
        yplot += coeffs[i]*xplot**i
    return xplot, yplot

def mkplot(X, Y, xlabel, title, text, filename_no_ext):
    fig = pp.figure()
    ax = fig.gca()
    if xlabel == "Spatial grid size":
        ax.semilogy(X, Y[0,0,:], "o-C0", label="Matrix Numerov", markersize=10)
        ax.semilogy(*mkfit(X, Y[0,0,:], d=3), "--C0")
    ax.semilogy(X, Y[1,0,:], "D-C1", label="Shooting")
    ax.semilogy(*mkfit(X, Y[1,0,:]), "--C1")
    if xlabel == "Spatial grid size":
        ax.semilogy(X, Y[2,0,:], "^-C2", label="Renorm. Numerov", markersize=10)
        ax.semilogy(*mkfit(X, Y[2,0,:]), "--C2")
    if xlabel == "Spatial grid size":
        dX = X[1]-X[0]
        ylims = ax.get_ylim()
        ax.add_artist(
            pp.Rectangle((x_const-dX/8, ylims[0]), width=dX/4, height=ylims[1]-ylims[0],
                fill=True, color="#d0d0d0")
        )
    ax.set_xlabel(xlabel)
    ax.set_ylabel("Runtime [s]")
    ax.set_title(title)
    ax.legend()
    xmin, xmax = ax.get_xlim()
    ymin, ymax = ax.get_ylim()
    #ax.text(xmin + 0.03*(xmax - xmin), ymin + 0.97*(ymax - ymin), text,
    #    fontsize=22, verticalalignment="top", bbox=dict(facecolor="white"))
    pd.grid(True, ax)
    fig.savefig(filename_no_ext+"_runtime.png")

    fig = pp.figure()
    ax = fig.gca()
    if xlabel == "Spatial grid size":
        ax.semilogy(X, Y[0,1,:], "o-C0", label="Matrix Numerov", markersize=10)
        #ax.semilogy(*mkfit(X, Y[0,1,:]), "--C0")
    ax.semilogy(X, Y[1,1,:], "D-C1", label="Shooting")
    #ax.semilogy(*mkfit(X, Y[1,1,:]), "--C1")
    if xlabel == "Spatial grid size":
        ax.semilogy(X, Y[2,1,:], "^-C2", label="Renorm. Numerov", markersize=10)
        #ax.semilogy(*mkfit(X, Y[2,1,:]), "--C2")
    if xlabel == "Spatial grid size":
        dx = X[1]-X[0]
        ylims = ax.get_ylim()
        ax.add_artist(
            pp.Rectangle((x_const-dx/8, ylims[0]), width=dx/4, height=ylims[1]-ylims[0],
                fill=True, color="#d0d0d0")
        )
    ax.set_xlabel(xlabel)
    ax.set_ylabel(r"RMS energy error [arb. units]")
    ax.set_title(title)
    ax.legend()
    xmin, xmax = ax.get_xlim()
    ymin, ymax = ax.get_ylim()
    #ax.text(xmin + 0.03*(xmax - xmin), ymin + 0.97*(ymax - ymin), text,
    #    fontsize=22, verticalalignment="top", bbox=dict(facecolor="white"))
    pd.grid(True, ax)
    fig.savefig(filename_no_ext+"_error.png")

L_const = 200
x_const = 1000
E_const = 1000

#L_dep = np.zeros((3, 2, len(L)))
x_dep = np.zeros((3, 2, len(x)))
E_dep = np.zeros((3, 2, len(E)))
for k in range(ntrials):
    #for i in range(len(L)):
    #    t, e = qho.main(L[i], x_const, E_const, "matrix-numerov", N, False)
    #    L_dep[0,:,i] += np.array([t, e])
    #    t, e = qho.main(L[i], x_const, E_const, "shooting", N, False)
    #    L_dep[1,:,i] += np.array([t, e])
    #    t, e = qho.main(L[i], x_const, E_const, "johnson", N, False)
    #    L_dep[2,:,i] += np.array([t, e])

    for i in range(len(x)):
        t, e = qho.main(L_const, x[i], E_const, "matrix-numerov", N, False)
        x_dep[0,:,i] += np.array([t, e])
        t, e = qho.main(L_const, x[i], E_const, "shooting", N, False)
        x_dep[1,:,i] += np.array([t, e])
        t, e = qho.main(L_const, x[i], E_const, "johnson", N, False)
        x_dep[2,:,i] += np.array([t, e])

    for i in range(len(E)):
        t, e = qho.main(L_const, x_const, E[i], "matrix-numerov", N, False)
        E_dep[0,:,i] += np.array([t, e])
        t, e = qho.main(L_const, x_const, E[i], "shooting", N, False)
        E_dep[1,:,i] += np.array([t, e])
        t, e = qho.main(L_const, x_const, E[i], "johnson", N, False)
        E_dep[2,:,i] += np.array([t, e])
#L_dep /= ntrials
x_dep /= ntrials
E_dep /= ntrials

L = [x//2 for x in L]
#data_x = [L, x, E]
data_x = [x, E]
#data_y = [L_dep, x_dep, E_dep]
data_y = [x_dep, E_dep]
xlabels = [
    #"Solving boundary [nm]",
    "Spatial grid size",
    "Energy grid size"
]
ylabels = [
    "Runtime [s]",
    r"RMS energy error $\left[E_\alpha\right]$"
]
titles = [
    "",
    "",
    ""
]
texts = [
    #f"Spatial grid size = {x_const}\nEnergy grid size = {E_const}",
    f"Solving boundary @ x = $\\pm${L_const//2}\nEnergy grid size = {E_const}",
    f"Solving boundary @ x = $\\pm${L_const//2}\nSpatial grid size = {x_const}"
]
filenames = [
    #path.join(home_path, "tests/output/qho_methods_test/Ldep"),
    path.join(home_path, "tests/output/qho_methods_test/xdep"),
    path.join(home_path, "tests/output/qho_methods_test/Edep")
]
for i in range(len(data_y)):
    mkplot(data_x[i], data_y[i], xlabels[i], titles[i], texts[i], filenames[i])

