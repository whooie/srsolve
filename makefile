CC := gcc
CFLAGS := -Wall -O3 -shared -pthread -fPIC -fwrapv -fno-strict-aliasing
PYFLAGS := $(shell python-config --cflags --ldflags)
NPFLAGS := -I$(shell python -c 'import numpy; print(numpy.get_include())')
ROOTDIR := $(shell pwd)
SRCDIR := libSr
OBJDIR := build
EXTDIR := libSr
EXTSUBDIR := potentials
EXTSUFF := $(shell python-config --extension-suffix)

SRCS := $(shell find "$(EXTDIR)" -name "*.c")
OBJS := $(addprefix $(OBJDIR)/,$(SRCS:$(SRCDIR)/%.c=%.o))
EXTS := $(SRCS:.c=$(EXTSUFF))

default: $(OBJDIR) $(BUILDDIR) $(EXTS)

$(OBJDIR):
	@test -d "build" || (mkdir "build"; echo "mkdir 'build/'")
	@test -d "build/potentials" || (mkdir "build/potentials"; echo "mkdir 'build/potentials/'")

$(EXTDIR)/%$(EXTSUFF): $(OBJDIR)/%.o
	$(CC) $(CFLAGS) $(LDFLAGS) $(PYFLAGS) $(NPFLAGS) $< -o $@

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	$(CC) $(CFLAGS) $(LDFLAGS) $(PYFLAGS) $(NPFLAGS) -c $< -o $@

clean:
	$(RM) $(EXTS)
