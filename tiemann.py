import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as pp
import mpl_toolkits.mplot3d.axes3d as p3
import matplotlib.cm as cm
import scipy.optimize

import libSr
from libSr import phys, data, optim, misc
from libSr.potentials import tiemann, modifiedLJ, modifiedLJ_Yb
import libSr.misc.plotdefs as pd

import os
import os.path as path
import sys
import getopt
import re
import copy
import multiprocessing as mp

shortopts = ""
longopts = []
try:
    opts, args = getopt.gnu_getopt(sys.argv[1:], shortopts, longopts)
    for opt, optarg in opts:
        pass
except Exception as ERR:
    raise ERR

# experimental data
srdata = [data.sr84, data.sr86, data.sr88]
# extra states of interest
extra_states = [
    data.DimerData("84Sr",
        -13.7162e6*np.ones(4), np.zeros(4),
        np.arange(58, 62, dtype=int), np.zeros(4, dtype=int)
    ),
    data.DimerData("86Sr",
        -83e3*np.ones(8), np.zeros(8),
        np.append(v := np.arange(59, 63, dtype=int), v),
        np.append(np.zeros(4, dtype=int), 2*np.ones(4, dtype=int))
    ),
    data.DimerData("87Sr",
        -25.9e6*np.ones(4), np.zeros(4),
        np.arange(59, 63, dtype=int), np.zeros(4, dtype=int)
    ),
    data.DimerData("88Sr",
        -136.7e6*np.ones(4), np.zeros(4),
        np.arange(59, 63, dtype=int), np.zeros(4, dtype=int)
    )
]
# subset of states in transitions from Tiemann
transition_levels = list(range(24,32))+list(range(34,61)) # specific vibrational levels
transition_states = [ # levels formatted as DimerData objects
    data.DimerData("88Sr",
        np.ones(len(transition_levels)),
        np.zeros(len(transition_levels)),
        np.array(transition_levels, dtype=int),
        8*np.ones(len(transition_levels), dtype=int)
    )
]
transition_lines = np.array([ # experimental data
    16981.4253, 16960.5259, 16940.3769, 16920.9663, 16902.2877, 16884.3381,
    16867.1178, 16850.6161, 16805.4162, 16791.7756, 16778.8340, 16766.5923,
    16755.0439, 16744.1854, 16734.0104, 16724.5123, 16715.6841, 16707.5181,
    16700.0051, 16693.1337, 16686.8926, 16681.2670, 16676.2409, 16671.7954,
    16667.9076, 16664.5535, 16661.7042, 16659.3264, 16657.3840, 16655.8419,
    16654.6530, 16653.7761, 16653.1673, 16652.7760, 16652.5564
])
transition_linesu = np.array([
    0.004, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002,
    0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002,
    0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002,
    0.002, 0.004
])

# rescale to a.u. when inv=False
def rescale_au(P, n, a0, E0, inv=False):
    if not inv:
        return (E0*a0**n) / (phys.Eh*phys.a0**n) * P
    else:
        return (phys.Eh*phys.a0**n) / (E0*a0**n) * P

# match parameter names for proper units in the Tiemann potential
def get_tiemann_units(varname):
    C_pattern = re.compile("C_([0-9]+)")
    R_pattern = re.compile("r_[a-zA-Z]")
    uu = ""
    C_match = C_pattern.match(varname)
    R_match = R_pattern.match(varname)
    if varname in ["A", "T_m", "a_i", "U_inf"]:
        uu = "cm^-1"
    elif varname == "B":
        uu = "cm^-1 Angstrom^n"
    elif varname in ["n", "b"]:
        uu = "Dimensionless"
    elif R_match:
        uu = "Angstrom"
    elif C_match:
        uu = "cm^-1 Angstrom^"+C_match.group(1)
    return uu

# compute the partial chi^2 sum for states in DimerData object `D`
def _chi2_psum(params, pot, D):
    chi2_psum = 0
    X = libSr.MPSolver(D.mu, pot, D.m.max()*phys.Hz2J, **params)
    for m,u,v,l in D:
        try:
            sol = X.solve(method="johnson", l=l, nu=v, Eo=X.rescale_hertz(m),
                bounds=(X.Vmin/3, 0), compute_wf=False, maxiters=6500,
                EPSILON=1e-9)[0]
            chi2_psum += ((X.rescale_hertz(sol.E, inv=True) - m)/u)**2
        except ValueError:
            print("encountered bad potential")
            chi2_psum += np.float64("nan")
    return chi2_psum

# compute the whole chi^2 sum
def chi2(params, pot, data):
    pool = mp.Pool(mp.cpu_count())
    chi2_psums = [pool.apply_async(_chi2_psum, [params, pot, D]) for D in data]
    chi2 = sum([term.get() for term in chi2_psums])
    pool.close()
    return chi2

# compute:
#   C_6, C_10, T_m, A, B
# in the Tiemann potential from associated x_C_6 and x_Cp multipliers
def prop_Cp_TmAB(**params):
    _params = copy.deepcopy(params)
    _params["C_6"] = params["x_C_6"]*libSr.Tiemann["C_6"]
    _params["C_10"] = params["x_Cp"]*libSr.Tiemann["C_10"]/libSr.Tiemann["C_6"]*_params["C_6"]
    return tiemann.compute_TmAB(**_params)

# compute:
#   C_10, T_m, A, B
# in the Tiemann potential from Cp
def Cp_TmAB(**params):
    _params = copy.deepcopy(params)
    _params["C_10"] = params["Cp"]*_params["C_6"]
    return tiemann.compute_TmAB(**_params)

if len(args) < 1:
    print("""
    Available operations:
        optim [ output filename ]
        compute2D [ output directory ]
        plot2D [ input/output directory ]
        plot3D [ input/output directory ]
        fitmincurves [ input/output directory ]
        fitmin2D [ input/output directory ]
        process2D [ input/output directory ]
    """)
    sys.exit(0)

# do gradient descent
elif args[0] in ["optim"]:
    filename = "output/sr_tiemann_optim.txt" if len(args)<2 else args[1]

    Cp = libSr.Tiemann["C_10"]/libSr.Tiemann["C_6"]
    params = optim.HashVector({
        "C_8": libSr.Tiemann["C_8"],
        "C_6": libSr.Tiemann["C_6"],
        "Cp": Cp
    }) * optim.HashVector({
        "C_8": 1.000,
        "C_6": 1.000,
        "Cp": 1.000,
    })
    params["x_C_6"] = params["C_6"]/libSr.Tiemann["C_6"]
    params["x_Cp"] = params["Cp"]/Cp
    vlist = [
        ("x_C_6", prop_Cp_TmAB),
        ("x_Cp", prop_Cp_TmAB)
    ]
    costf_args = dict(pot=libSr.Tiemann, data=srdata)
    stepsize = 1e-12
    L0 = 1e-15
    L1 = 1e-9
    dynamic_until = 1
    EPSILON = 1e-6
    maxiters = 1000
    optim.optim(costf_relerr, params, vlist, costf_args, stepsize, L0, L1,
        dynamic_until, EPSILON, maxiters, filename)

# image the cost function within 2D bounds in C6 and Cp
elif args[0] in ["compute2D"]:
    outdir = "output/sr_tiemann_2D/" if len(args)<2 else args[1]

    C8 = libSr.Tiemann["C_8"] + 0*tiemann.PARAMSu["C_8"]
    C6 = libSr.Tiemann["C_6"]
    Cp = libSr.Tiemann["C_10"]/libSr.Tiemann["C_6"]

    NCp = 100
    Cp_l = (1.093 - 0.010) * Cp # C8 - given
    Cp_r = (1.093 + 0.010) * Cp # C8 - given
    Cp_l = (0.364 - 0.010) * Cp # C8 - upper bound
    Cp_r = (0.364 + 0.010) * Cp # C8 - upper bound
    Cp_l = (1.817 - 0.010) * Cp # C8 - lower bound
    Cp_r = (1.817 + 0.010) * Cp # C8 - lower bound

    NC6 = 100
    C6_l = (0.9945 - 0.001) * C6 # C8 - given
    C6_r = (0.9945 + 0.001) * C6 # C8 - given
    C6_l = (0.9761 - 0.001) * C6 # C8 - upper bound
    C6_r = (0.9761 + 0.001) * C6 # C8 - upper bound
    C6_l = (1.0119 - 0.001) * C6 # C8 - lower bound
    C6_r = (1.0119 + 0.001) * C6 # C8 - lower bound

    _C6 = np.linspace(C6_l, C6_r, NC6)
    _Cp = np.linspace(Cp_l, Cp_r, NCp)

    if not path.isdir(outdir):
        print(":: mkdir "+outdir)
        os.mkdir(outdir)
    np.savetxt(path.join(outdir, "coords_C8.txt"), np.array([C8]).T, fmt="%.24e")
    np.savetxt(path.join(outdir, "coords_C6.txt"), np.array([_C6]).T, fmt="%.24e")
    np.savetxt(path.join(outdir, "coords_Cp.txt"), np.array([_Cp]).T, fmt="%.24e")
    CHI2 = np.zeros((NC6, NCp), dtype=np.float64)
    for i in range(NC6):
        for j in range(NCp):
            params = libSr.HashVector({"C_8": C8, "C_6": _C6[i], "Cp": _Cp[j]})
            params.update(Cp_TmAB(**params))
            CHI2[i,j] = chi2(params, libSr.Tiemann, srdata)
            print(f"\r{i+1:4.0f}/{NC6}; {j+1:4.0f}/{NCp}  ({100*(i*NCp+j+1)/(NC6*NCp):5.1f}%) ",
                end="", flush=True)
    print("")
    np.savetxt(path.join(outdir, "chi2.txt"), CHI2)

# plot the cost function image generated by "compute2D"
elif args[0] in ["plot2D"]:
    outdir = "output/sr_tiemann_2D/" if len(args)<2 else args[1]

    cmap = cm.rainbow
    pp.rcParams["image.cmap"] = "rainbow"

    C6 = libSr.Tiemann["C_6"]
    Cp = libSr.Tiemann["C_10"]/libSr.Tiemann["C_6"]

    C8 = np.loadtxt(path.join(outdir, "coords_C8.txt"), unpack=True)
    _C6 = np.loadtxt(path.join(outdir, "coords_C6.txt"), unpack=True)
    _Cp = np.loadtxt(path.join(outdir, "coords_Cp.txt"), unpack=True)
    CHI2 = np.loadtxt(path.join(outdir, "chi2.txt"))
    logCHI2 = np.log10(CHI2)

    fig, ax = pp.subplots()
    im = ax.imshow(CHI2,
        origin="lower", aspect="auto",
        extent=[_Cp.min()/Cp, _Cp.max()/Cp, _C6.min()/C6, _C6.max()/C6],
        cmap=cmap, vmin=CHI2.min(), vmax=CHI2.max()
    )
    pp.colorbar(mappable=im, ax=ax)
    ax.set_title(f"$\\chi^2$ for $C_8' =$ {C8:.4e}")
    ax.set_ylabel("$C_6'/C_6^{(0)}$")
    ax.set_xlabel("$(C')'/C'^{(0)}$")
    ax.tick_params(axis="x", labelrotation=-30)
    pp.savefig(path.join(outdir, "plot2D_chi2.png"))

    fig, ax = pp.subplots()
    im = ax.imshow(logCHI2,
        origin="lower", aspect="auto",
        extent=[_Cp.min()/Cp, _Cp.max()/Cp, _C6.min()/C6, _C6.max()/C6],
        cmap=cmap, vmin=logCHI2.min(), vmax=logCHI2.max()
    )
    pp.colorbar(mappable=im, ax=ax)
    ax.set_title(f"$\\log_{{10}}\\chi^2$ for $C_8' =$ {C8:.4e}")
    ax.set_ylabel("$C_6'/C_6^{(0)}$")
    ax.set_xlabel("$(C')'/C'^{(0)}$")
    ax.tick_params(axis="x", labelrotation=-30)
    pp.savefig(path.join(outdir, "plot2D_chi2_log10.png"))

# plot the cost function image generated by "compute2D" in 3D
elif args[0] in ["plot3D"]:
    outdir = "output/sr_tiemann_2D/" if len(args)<2 else args[1]

    cmap = cm.rainbow
    pp.rcParams["image.cmap"] = "rainbow"

    C6 = libSr.Tiemann["C_6"]
    Cp = libSr.Tiemann["C_10"]/libSr.Tiemann["C_6"]

    C8 = np.loadtxt(path.join(outdir, "coords_C8.txt"), unpack=True)
    _C6 = np.loadtxt(path.join(outdir, "coords_C6.txt"), unpack=True)
    _Cp = np.loadtxt(path.join(outdir, "coords_Cp.txt"), unpack=True)
    CHI2 = np.loadtxt(path.join(outdir, "chi2.txt"))
    MCp, MC6 = np.meshgrid(_Cp, _C6)

    fig = pp.figure()
    ax = p3.Axes3D(fig)
    ax.plot_surface(MCp/Cp, MC6/C6, CHI2, cmap=cmap)
    ax.set_title(f"$C_8' =$ {C8:.4e}")
    ax.set_xlabel("$(C')'/C'^{(0)}$")
    ax.set_ylabel("$C_6'/C_6^{(0)}$")
    ax.set_zlabel("$\\chi^2$")
    
    fig = pp.figure()
    ax = p3.Axes3D(fig)
    ax.plot_surface(MCp/Cp, MC6/C6, np.log10(CHI2), cmap=cmap)
    ax.set_title(f"$\\log_{{10}}\\chi^2$ for $C_8' =$ {C8:.4e}")
    ax.set_xlabel("$(C')'/C'^{(0)}$")
    ax.set_ylabel("$C_6'/C_6^{(0)}$")
    ax.set_zlabel("$\\chi^2$")
    
    pp.show()

# fit and plot the minimizing curves as dependent on C6 and Cp (separately)
# also fit and plot the path through C6-Cp space
elif args[0] in ["fitmincurves"]:
    outdir = "output/sr_tiemann_2D/" if len(args)<2 else args[1]

    C6 = libSr.Tiemann["C_6"]
    Cp = libSr.Tiemann["C_10"]/libSr.Tiemann["C_6"]

    C8 = np.loadtxt(path.join(outdir, "coords_C8.txt"), unpack=True)
    _C6 = np.loadtxt(path.join(outdir, "coords_C6.txt"), unpack=True)
    _Cp = np.loadtxt(path.join(outdir, "coords_Cp.txt"), unpack=True)
    CHI2 = np.loadtxt(path.join(outdir, "chi2.txt"))

    MCp, MC6 = np.meshgrid(_Cp, _C6)

    _C6_plot = np.linspace(_C6.min(), _C6.max(), 10*_C6.shape[0])
    _Cp_plot = np.linspace(_Cp.min(), _Cp.max(), 10*_Cp.shape[0])

    fig3D = pp.figure()
    ax3D = p3.Axes3D(fig3D)
    ax3D.plot3D(MCp.flatten(), MC6.flatten(), CHI2.flatten(), "ok")

    corr_vs_Cp = np.zeros(2, dtype=np.float64)
    _minC6_vs_Cp = np.zeros(_Cp.shape, dtype=np.float64)
    _minCHI2_vs_Cp = np.zeros(_Cp.shape, dtype=np.float64)
    for j in range(_Cp.shape[0]):
        a, b, c = np.polyfit(_C6, CHI2[:,j], deg=2)
        _minC6_vs_Cp[j] = -b/(2*a)
        _minCHI2_vs_Cp[j] = -b**2/(4*a) + c
        ax3D.plot3D(_Cp[j]*np.ones(_C6_plot.shape), _C6_plot, np.polyval([a,b,c], _C6_plot), "-C0")
        ax3D.plot3D([_Cp[j]], [_minC6_vs_Cp[j]], [_minCHI2_vs_Cp[j]],
            "ob" if _minCHI2_vs_Cp[j] > 0 else "om")
    #ax3D.plot3D(_Cp, _minC6_vs_Cp, _minCHI2_vs_Cp, "ob")
    a, b = np.polyfit(_Cp, _minC6_vs_Cp, deg=1)
    corr_vs_Cp[:] = [a, b]
    _minC6_vs_Cp_plot = a*_Cp_plot + b
    a, b, c = np.polyfit(_Cp, _minCHI2_vs_Cp, deg=2)
    _minCHI2_vs_Cp_plot = a*_Cp_plot**2 + b*_Cp_plot + c

    fig, axs = pp.subplots(nrows=2, sharex=True, gridspec_kw=dict(hspace=0.025))
    axs[0].plot(_Cp_plot/Cp, _minCHI2_vs_Cp_plot, "-C3")
    axs[0].plot(_Cp/Cp, _minCHI2_vs_Cp, "ok")
    axs[0].set_ylabel("$\\chi^2$")
    axs[1].plot(_Cp_plot/Cp, _minC6_vs_Cp_plot/C6, "-C0")
    axs[1].plot(_Cp/Cp, _minC6_vs_Cp/C6, "ok")
    axs[1].set_ylabel("$C_6'/C_6^{(0)}$")
    axs[0].set_title(f"$C_8' =$ {C8:.4e}")
    axs[1].set_xlabel("$(C')'/C'^{(0)}$")
    pd.grid(True, axs[0])
    pd.grid(True, axs[1])
    pp.savefig(path.join(outdir, "fitmincurves_vs_Cp.png"))
    pp.close(fig)

    corr_vs_C6 = np.zeros(2, dtype=np.float64)
    _minCp_vs_C6 = np.zeros(_C6.shape, dtype=np.float64)
    _minCHI2_vs_C6 = np.zeros(_C6.shape, dtype=np.float64)
    for i in range(_C6.shape[0]):
        a, b, c = np.polyfit(_Cp, CHI2[i,:], deg=2)
        _minCp_vs_C6[i] = -b/(2*a)
        _minCHI2_vs_C6[i] = -b**2/(4*a) + c
        ax3D.plot3D(_Cp_plot, _C6[i]*np.ones(_Cp_plot.shape), np.polyval([a,b,c], _Cp_plot), "-C1")
        ax3D.plot3D([_minCp_vs_C6[i]], [_C6[i]], [_minCHI2_vs_C6[i]],
            "or" if _minCHI2_vs_C6[i] > 0 else "oy")
    #ax3D.plot3D(_minCp_vs_C6, _C6, _minCHI2_vs_C6, "or")
    a, b = np.polyfit(_C6, _minCp_vs_C6, deg=1)
    corr_vs_C6[:] = [a, b]
    _minCp_vs_C6_plot = a*_C6_plot + b
    a, b, c = np.polyfit(_C6, _minCHI2_vs_C6, deg=2)
    _minCHI2_vs_C6_plot = a*_C6_plot**2 + b*_C6_plot + c

    outfile = open(path.join(outdir, "fitmincurves_troughline.txt"), 'w')
    misc.print_write(outfile, "C6 = a*C' + b")
    misc.print_write(outfile,
        "".join(["  "+q+" = {:21.14e}\n" for q in ["a","b"]]).format(
            (corr_vs_Cp[0] + 1/corr_vs_C6[0])/2,
            (corr_vs_Cp[1] - corr_vs_C6[1]/corr_vs_C6[0])/2
        )
    )
    outfile.close()

    fig, axs = pp.subplots(nrows=2, sharex=True, gridspec_kw=dict(hspace=0.025))
    axs[0].plot(_C6_plot/C6, _minCHI2_vs_C6_plot, "-C2")
    axs[0].plot(_C6/C6, _minCHI2_vs_C6, "sk")
    axs[0].set_ylabel("$\\chi^2$")
    axs[1].plot(_C6_plot/C6, _minCp_vs_C6_plot/Cp, "-C1")
    axs[1].plot(_C6/C6, _minCp_vs_C6/Cp, "sk")
    axs[1].set_ylabel("$(C')'/C'^{(0)}$")
    axs[0].set_title(f"$C_8' =$ {C8:.4e}")
    axs[1].set_xlabel("$C_6'/C_6^{(0)}$")
    pd.grid(True, axs[0])
    pd.grid(True, axs[1])
    pp.savefig(path.join(outdir, "fitmincurves_vs_C6.png"))
    pp.close(fig)

    fig, ax = pp.subplots()
    ax.plot(_Cp_plot/Cp, _minC6_vs_Cp_plot/C6, "-C0", label="Fit versus $(C')'$")
    ax.plot(_Cp/Cp, _minC6_vs_Cp/C6, "ok")
    ax.plot(_minCp_vs_C6_plot/Cp, _C6_plot/C6, "-C1", label="Fit versus $C_6'$")
    ax.plot(_minCp_vs_C6/Cp, _C6/C6, "sk")
    ax.set_title(f"$C_8' =$ {C8:.4e}")
    ax.set_xlabel("$(C')'/C'^{(0)}$")
    ax.set_ylabel("$C_6'/C_6^{(0)}$")
    pd.grid(True, ax)
    ax.legend()
    pp.savefig(path.join(outdir, "fitmincurves_comp.png"))
    pp.close(fig)

    #ax3D.plot_surface(MCp, MC6, np.zeros(MCp.shape), color="k")

    pp.show()

# fit the cost function data imaged by "compute2D" to
#   z = ax^2 + by^2 + cxy + dx + ey + f
# and compute the its minimum and statistical uncertainties
elif args[0] in ["fitmin2D"]:
    outdir = "output/sr_tiemann_2D/" if len(args)<2 else args[1]

    cmap = cm.rainbow
    pp.rcParams["image.cmap"] = "rainbow"
    
    C6 = libSr.Tiemann["C_6"]
    Cp = libSr.Tiemann["C_10"]/libSr.Tiemann["C_6"]
    
    C8 = np.loadtxt(path.join(outdir, "coords_C8.txt"), unpack=True)
    _C6 = np.loadtxt(path.join(outdir, "coords_C6.txt"), unpack=True)
    _Cp = np.loadtxt(path.join(outdir, "coords_Cp.txt"), unpack=True)
    CHI2 = np.loadtxt(path.join(outdir, "chi2.txt"))

    MCp, MC6 = np.meshgrid(_Cp, _C6)
    _MCp = MCp.flatten()
    _MC6 = MC6.flatten()
    A = np.array([_MCp**2, _MC6**2, _MCp*_MC6, _MCp, _MC6, np.ones(CHI2.shape[0]*CHI2.shape[1])]).T
    coeffs = la.solve(A.T@A, A.T@CHI2.flatten())
    a, b, c, d, e, f = coeffs
    fchi2_fit = lambda x,y: a*x**2 + b*y**2 + c*x*y + d*x + e*y + f
    Cp_min = (c*e - 2*b*d)/(4*a*b - c**2)
    C6_min = (c*d - 2*a*e)/(4*a*b - c**2)

    k = 1/(sum([len(D) for D in srdata]) - 2)
    chi2_min = fchi2_fit(Cp_min, C6_min)
    CHI2_min = CHI2.min() if chi2_min < 0 else chi2_min
    Cp_min_u = np.sqrt((c*C6_min + d)**2 - 4*a*(b*C6_min**2 + e*C6_min + f - (chi2_min+k*CHI2_min)))/(2*a)
    C6_min_u = np.sqrt((c*Cp_min + e)**2 - 4*b*(a*Cp_min**2 + d*Cp_min + f - (chi2_min+k*CHI2_min)))/(2*b)

    mins_head, mins_table_fmt = misc.gen_table_fmt(
        ("C'_min", "e"), ("+/-", "e", dict(l=5, p=0)),
        ("C6_min", "e"), ("+/-", "e", dict(l=5, p=0)),
        ("chi^2_min", "e"),
        L=15, P=8, K=2
    )
    mins_reduced_head, mins_reduced_table_fmt = misc.gen_table_fmt(
        ("C'_min/C'^(0)", "e"), ("+/-", "e", dict(l=5, p=0)),
        ("C6_min/C6^(0)", "e"), ("+/-", "e", dict(l=5, p=0)),
        ("chi^2_min", "e"),
        L=15, P=8, K=2
    )
    outfile = open(path.join(outdir, "fitmin2D.txt"), 'w')
    misc.print_write(outfile, "chi^2 = a*C'^2 + b*C6^2 + c*C'*C6 + d*C' + e*C6 + f")
    misc.print_write(outfile,
        "".join(["  "+q+" = {:21.14e}\n" for q in ["a","b","c","d","e","f"]]).format(*coeffs)
    )
    misc.print_write(outfile, "")
    misc.print_write(outfile, mins_head)
    misc.print_write(outfile, mins_table_fmt.format(
        Cp_min, Cp_min_u,
        C6_min, C6_min_u,
        chi2_min
    ))
    misc.print_write(outfile, "")
    misc.print_write(outfile, mins_reduced_head)
    misc.print_write(outfile, mins_reduced_table_fmt.format(
        Cp_min/Cp, Cp_min_u/Cp,
        C6_min/C6, C6_min_u/C6,
        chi2_min
    ))
    outfile.close()

    CHI2_fit = fchi2_fit(MCp, MC6)
    fig, ax = pp.subplots()
    im = ax.imshow(CHI2_fit,
        origin="lower", aspect="auto",
        extent=[_Cp.min()/Cp, _Cp.max()/Cp, _C6.min()/C6, _C6.max()/C6],
        cmap=cmap, vmin=CHI2_fit.min(), vmax=CHI2_fit.max()
    )
    pp.colorbar(mappable=im, ax=ax)
    ax.set_title(f"$\\chi^2$ for $C_8' =$ {C8:.4e}")
    ax.set_ylabel("$C_6'/C_6^{(0)}$")
    ax.set_xlabel("$(C')'/C'^{(0)}$")
    ax.tick_params(axis="x", labelrotation=-30)
    pp.savefig(path.join(outdir, "fitmin2D_chi2.png"))

    logCHI2_fit = np.log10(CHI2_fit)
    fig, ax = pp.subplots()
    im = ax.imshow(logCHI2_fit,
        origin="lower", aspect="auto",
        extent=[_Cp.min()/Cp, _Cp.max()/Cp, _C6.min()/C6, _C6.max()/C6],
        cmap=cmap, vmin=logCHI2_fit.min(), vmax=logCHI2_fit.max()
    )
    pp.colorbar(mappable=im, ax=ax)
    ax.set_title(f"$\\log_{{10}}\\chi^2$ for $C_8' =$ {C8:.4e}")
    ax.set_ylabel("$C_6'/C_6^{(0)}$")
    ax.set_xlabel("$(C')'/C'^{(0)}$")
    ax.tick_params(axis="x", labelrotation=-30)
    pp.savefig(path.join(outdir, "fitmin2D_chi2_log10.png"))

# use the results from "fitmin2D" as parameters in computing the desired states
# and transition lines
elif args[0] in ["process2D"]:
    outdir = "output/sr_tiemann_2D/" if len(args)<2 else args[1]

    C8 = np.loadtxt(path.join(outdir, "coords_C8.txt"), unpack=True)
    data = np.loadtxt(path.join(outdir, "fitmin2D.txt"), skiprows=10, unpack=True, max_rows=1)
    Cp_min, Cp_min_u, C6_min, C6_min_u, chi2_min = data
    params = optim.HashVector({
        "C_8": C8,
        "C_6": C6_min,
        "C_10": C6_min*Cp_min
    })
    paramsu = optim.HashVector({
        "C_8": 1.5e8,
        "C_6": C6_min_u,
        "C_10": Cp_min_u*C6_min
    })
    params.update(tiemann.compute_TmAB(**params))

    outfile = open(path.join(outdir, "lines.txt"), 'w')

    main_head, main_table_fmt = misc.gen_table_fmt(
        ("Molecule", "s>", dict(l=10)),
        ("nu", "i"),
        ("l", "i"),
        ("Calculated [Hz]", "e"),
        ("Measured [Hz]", "e"),
        ("Meas. Uncertainty", "e"),
        ("Calc. - Meas.", "e"),
        L=15, P=8, K=2
    )
    transitions_head, transitions_table_fmt = misc.gen_table_fmt(
        ("nu1 -> nu2", "s>"),
        ("l1 -> l2", "s>"),
        ("Calculated [cm^-1]", "e"),
        ("Measured [cm^-1]", "e"),
        ("Meas. Uncertainty", "e"),
        ("Calc. - Meas.", "e"),
        K=2
    )
    extras_head, extras_table_fmt = misc.gen_table_fmt(
        ("Molecule", "s>", dict(L=10)),
        ("nu", "i"),
        ("l", "i"),
        ("Calculated [Hz]", "e"),
        L=15, P=8, K=2
    )

    misc.print_write(outfile, "With given Tiemann parameters: {")
    for v in params.keys():
        misc.print_write(outfile, "    {:5s} = {:15.8e} +/- {:.0e}  [{:s}]".format(
            v, tiemann.PARAMS[v], tiemann.PARAMSu.get(v, 0), get_tiemann_units(v)
        ))
    misc.print_write(outfile, "}")
    misc.print_write(outfile, main_head)
    CHI2 = 0
    ERRS = list()
    for D in srdata:
        X = libSr.MPSolver(D.mu, libSr.Tiemann, D.m.max()*phys.Hz2J, **tiemann.PARAMS)
        for m,u,v,l in D:
            sol = X.solve(method="johnson", l=l, nu=v, E0=X.rescale_hertz(m),
                bounds=(X.Vmin/3, 0), EPSILON=1e-9, maxiters=6500, compute_wf=False)[0]
            e = X.rescale_hertz(sol.E, inv=True)
            CHI2 += ((e - m)/u)**2
            ERRS.append((e - m)/m)
            misc.print_write(outfile, main_table_fmt.format(D.molecule, v, l, e, m, u, e-m))
    misc.print_write(outfile, "CHI^2 = {:.3e}".format(CHI2))
    misc.print_write(outfile, "RMSE = {:.3e}".format(np.sqrt((np.array(ERRS)**2).mean())))

    misc.print_write(outfile, "")

    misc.print_write(outfile, "Transition lines (88Sr):")
    misc.print_write(outfile, transitions_head)
    SOLS = list()
    for D in transition_states:
        X = libSr.MPSolver(D.mu, libSr.Tiemann, -136.7e6*phys.Hz2J, **tiemann.PARAMS)
        for m,u,v,l in D:
            sol = X.solve(method="johnson", l=l, nu=v, E0=X.Vmin/2,
                bounds=(X.Vmin, 0), EPSILON=1e-9, maxiters=6500, compute_wf=False)[0]
            SOLS.append(sol.rescale_energy(X.Ealpha, phys.cm2J))
    CHI2 = 0
    ERRS = list()
    for i in range(1, len(transition_levels)):
        K = SOLS[i].E - SOLS[i-1].E
        M = transition_lines[i-1] - transition_lines[i]
        U = np.sqrt(transition_linesu[i]**2 + transition_linesu[i-1]**2)
        misc.print_write(outfile, transitions_table_fmt.format(
            str(transition_levels[i-1])+" -> "+str(transition_levels[i]),
            str(transition_states[0].l[i-1])+" -> "+str(transition_states[0].l[i]),
            K, M, U, K - M
        ))
        CHI2 += ((K-M)/U)**2
        ERRS.append((K-M)/M)
    misc.print_write(outfile, "CHI^2 = {:.3e}".format(CHI2))
    misc.print_write(outfile, "RMSE = {:.3e}".format(np.sqrt((np.array(ERRS)**2).mean())))

    misc.print_write(outfile, "")

    misc.print_write(outfile, "Extra predictions:")
    misc.print_write(outfile, extras_head)
    for D in extra_states:
        X = libSr.SrSolver(D.mu, libSr.Tiemann, D.m.max()*phys.Hz2J, **tiemann.PARAMS)
        for m,u,v,l in D:
            sol = X.solve(method="johnson", l=l, nu=v, E0=X.rescale_hertz(m),
                bounds=(X.Vmin/3, 0), EPSILON=1e-9, maxiters=6500, compute_wf=False)[0]
            e = X.rescale_hertz(sol.E, inv=True)
            misc.print_write(outfile, extras_table_fmt.format(D.molecule, v, l, e))


    misc.print_write(outfile, "")
    misc.print_write(outfile, 20*"-")
    misc.print_write(outfile, "")


    misc.print_write(outfile, "With fit parameters: {")
    for v in params.keys():
        misc.print_write(outfile, "    {:5s} = {:15.8e} +/- {:.0e}  [{:s}]".format(
            v, params[v], paramsu.get(v, 0), get_tiemann_units(v)
        ))
    misc.print_write(outfile, "}")
    misc.print_write(outfile, main_head)
    CHI2 = 0
    ERRS = list()
    for D in srdata:
        X = libSr.MPSolver(D.mu, libSr.Tiemann, D.m.max()*phys.Hz2J, **params)
        for m,u,v,l in D:
            sol = X.solve(method="johnson", l=l, nu=v, E0=X.rescale_hertz(m),
                bounds=(X.Vmin/3, 0), EPSILON=1e-9, maxiters=6500, compute_wf=False)[0]
            e = X.rescale_hertz(sol.E, inv=True)
            CHI2 += ((e - m)/u)**2
            ERRS.append((e - m)/m)
            misc.print_write(outfile, main_table_fmt.format(D.molecule, v, l, e, m, u, e-m))
    misc.print_write(outfile, "CHI^2 = {:.3e}".format(CHI2))
    misc.print_write(outfile, "RMSE = {:.3e}".format(np.sqrt((np.array(ERRS)**2).mean())))

    misc.print_write(outfile, "")

    misc.print_write(outfile, "Transition lines (88Sr):")
    misc.print_write(outfile, transitions_head)
    SOLS = list()
    for D in transition_states:
        X = libSr.MPSolver(D.mu, libSr.Tiemann, -136.7e6*phys.Hz2J, **params)
        for m,u,v,l in D:
            sol = X.solve(method="johnson", l=l, nu=v, E0=X.Vmin/2,
                bounds=(X.Vmin, 0), EPSILON=1e-9, maxiters=6500, compute_wf=False)[0]
            SOLS.append(sol.rescale_energy(X.Ealpha, phys.cm2J))
    CHI2 = 0
    ERRS = list()
    for i in range(1, len(transition_levels)):
        K = SOLS[i].E - SOLS[i-1].E
        M = transition_lines[i-1] - transition_lines[i]
        U = np.sqrt(transition_linesu[i]**2 + transition_linesu[i-1]**2)
        misc.print_write(outfile, transitions_table_fmt.format(
            str(transition_levels[i-1])+" -> "+str(transition_levels[i]),
            str(transition_states[0].l[i-1])+" -> "+str(transition_states[0].l[i]),
            K, M, U, K - M
        ))
        CHI2 += ((K-M)/U)**2
        ERRS.append((K-M)/M)
    misc.print_write(outfile, "CHI^2 = {:.3e}".format(CHI2))
    misc.print_write(outfile, "RMSE = {:.3e}".format(np.sqrt((np.array(ERRS)**2).mean())))

    misc.print_write(outfile, "")

    misc.print_write(outfile, "Extra predictions:")
    misc.print_write(outfile, extras_head)
    for D in extra_states:
        X = libSr.SrSolver(D.mu, libSr.Tiemann, D.m.max()*phys.Hz2J, **params)
        for m,u,v,l in D:
            sol = X.solve(method="johnson", l=l, nu=v, E0=X.rescale_hertz(m),
                bounds=(X.Vmin/3, 0), EPSILON=1e-9, maxiters=6500, compute_wf=False)[0]
            e = X.rescale_hertz(sol.E, inv=True)
            misc.print_write(outfile, extras_table_fmt.format(D.molecule, v, l, e))

    outfile.close()

else:
    print("invalid command")
    sys.exit(1)

